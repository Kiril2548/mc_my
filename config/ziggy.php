<?php

return [
    'groups' => [
        'admin' => [
            'admin.*',
        ],
        'public' => [
            '*',
        ]
    ],
];
