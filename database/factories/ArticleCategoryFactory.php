<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\ArticleCategory::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->company,
        'status' => $faker->randomKey([1,0])
    ];
});
