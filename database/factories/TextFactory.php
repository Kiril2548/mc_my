<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Text::class, function (Faker $faker) {
    return [
        'page' => $faker->slug,
        'key' => $faker->slug,
        'text' => $faker->text('50000')
    ];
});
