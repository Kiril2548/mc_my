<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Page::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->company,
        'img'   => 'https://picsum.photos/1980/1080/?random',
        'description' => $faker->text('200'),
        'key_words' => json_encode($faker->title),
        'page_text' => $faker->text(50000)
    ];
});
