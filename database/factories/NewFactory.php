<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\News::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->company,
        'img'   => 'https://picsum.photos/1980/1080/?random',
        'text' => $faker->text(10000)
    ];
});
