<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'category_id' => rand(1,5),
        'title' => $faker->company,
        'desc' => $faker->text(1000),
        'price' => $faker->randomFloat(2, 20, 500),
        'count' => rand(1,100),
        'discount_id'   =>  rand(1,100),
    ];
});
