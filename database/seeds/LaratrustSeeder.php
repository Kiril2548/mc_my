<?php

use Illuminate\Database\Seeder;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $U = [
            'name'  => 'test11@test11.test11',
            'email'  => 'test11@test11.test11',
            'password'  => bcrypt('test11@test11.test11'),
            'balance'  => 425,
            'bonus'  => 100,
            'coins'  => 25,
        ];
        $user->fill($U);
        $user->save();

        $this->command->info('Seeding discounts');
        $this->command->getOutput()->progressStart(100);
        for ($i = 1; $i <= 100; $i++){
            \Illuminate\Support\Facades\DB::table('discounts')->insert([
                'value' => $i
            ]);
            $this->command->getOutput()->progressAdvance(1);
        }
        $this->command->getOutput()->progressFinish();

        /*---------------------*/

//        $this->command->info('Seeding product categories');
//        $this->command->getOutput()->progressStart(5);
//        for ($i = 0; $i < 5; $i++ ){
//            $category = factory(App\Models\Category::class)->create();
//            $this->command->getOutput()->progressAdvance(1);
//        }
//        $this->command->getOutput()->progressFinish();

        /*---------------------*/

        $this->command->info('Seeding article categories');
        $this->command->getOutput()->progressStart(5);
        for ($i = 0; $i < 5; $i++ ){
            $category = factory(App\Models\ArticleCategory::class)->create();
            $this->command->getOutput()->progressAdvance(1);
        }
        $this->command->getOutput()->progressFinish();
//
        /*---------------------*/

//        $this->command->info('Seeding products');
//        $this->command->getOutput()->progressStart(25);
//        for ($i = 0; $i < 25; $i++ ){
//            $product = factory(App\Models\Product::class)->create();
//            $product->imgs()->create([
//                'url' => 'https://picsum.photos/1980/1080/?random'
//            ]);
//            $this->command->getOutput()->progressAdvance(1);
//        }
//        $this->command->getOutput()->progressFinish();

        /*---------------------*/

        $this->command->info('Seeding articles');
        $this->command->getOutput()->progressStart(25);
        for ($i = 0; $i < 25; $i++ ){
            $article = factory(App\Models\Article::class)->create();
            $this->command->getOutput()->progressAdvance(1);
        }
        $this->command->getOutput()->progressFinish();

        /*---------------------*/

        $this->command->info('Seeding news');
        $this->command->getOutput()->progressStart(25);
        for ($i = 0; $i < 25; $i++ ){
            $news = factory(App\Models\News::class)->create();
            $this->command->getOutput()->progressAdvance(1);
        }
        $this->command->getOutput()->progressFinish();

        /*---------------------*/

        $this->command->info('Seeding pages');
        $this->command->getOutput()->progressStart(3);
        for ($i = 0; $i < 3; $i++ ){
            $page = factory(App\Models\Page::class)->create();
            $this->command->getOutput()->progressAdvance(1);
        }
        $this->command->getOutput()->progressFinish();

        /*---------------------*/

        $this->command->info('Seeding texts');
        $this->command->getOutput()->progressStart(3);
        for ($i = 0; $i < 3; $i++ ){
            $text = factory(App\Models\Text::class)->create();
            $this->command->getOutput()->progressAdvance(1);
        }
        $this->command->getOutput()->progressFinish();
    }
}
