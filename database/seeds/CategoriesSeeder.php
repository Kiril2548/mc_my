
<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    public function run()
    {
       DB::table('categories')->insert([
           'id' => '1',
           'title' => 'COINS',
           'slug' => 'coins',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-12-20 02:56:26',
           'updated_at' => '2018-12-20 02:56:26',
       ]);

    }
}
        