
<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    public function run()
    {
       DB::table('products')->insert([
           'id' => '1',
           'slug' => '1',
           'category_id' => '1',
           'title' => '1',
           'desc' => '1 COIN',
           'price' => '1.2',
           'count' => '1',
           'status' => '1',
           'created_at' => '2018-12-20 02:57:42',
           'updated_at' => '2018-12-20 02:57:42',
       ]);
       DB::table('products')->insert([
           'id' => '2',
           'slug' => '10',
           'category_id' => '1',
           'title' => '10',
           'desc' => '10 COINS',
           'price' => '12',
           'count' => '10',
           'status' => '1',
           'created_at' => '2018-12-20 02:58:02',
           'updated_at' => '2018-12-20 02:58:02',
       ]);
       DB::table('products')->insert([
           'id' => '3',
           'slug' => '100',
           'category_id' => '1',
           'title' => '100',
           'desc' => '100 COINS',
           'price' => '120',
           'count' => '100',
           'status' => '1',
           'created_at' => '2018-12-20 02:58:21',
           'updated_at' => '2018-12-20 02:58:21',
       ]);
       DB::table('products')->insert([
           'id' => '4',
           'slug' => '1000',
           'category_id' => '1',
           'title' => '1000',
           'desc' => '1000',
           'price' => '1200',
           'count' => '1000',
           'status' => '1',
           'created_at' => '2018-12-20 02:58:38',
           'updated_at' => '2018-12-20 02:58:38',
       ]);
       DB::table('products')->insert([
           'id' => '5',
           'slug' => '10000',
           'category_id' => '1',
           'title' => '10000',
           'desc' => '10000',
           'price' => '12000',
           'count' => '10000',
           'status' => '1',
           'created_at' => '2018-12-20 02:58:48',
           'updated_at' => '2018-12-20 02:58:48',
       ]);
       DB::table('products')->insert([
           'id' => '6',
           'slug' => '100000',
           'category_id' => '1',
           'title' => '100000',
           'desc' => '100000',
           'price' => '120000',
           'count' => '100000',
           'status' => '1',
           'created_at' => '2018-12-20 02:58:58',
           'updated_at' => '2018-12-20 02:58:58',
       ]);
       DB::table('products')->insert([
           'id' => '7',
           'slug' => '1000000',
           'category_id' => '1',
           'title' => '1000000',
           'desc' => '1000000',
           'price' => '1000000',
           'count' => '1000000',
           'status' => '1',
           'created_at' => '2018-12-20 02:59:13',
           'updated_at' => '2018-12-20 03:04:04',
       ]);
       DB::table('products')->insert([
           'id' => '8',
           'slug' => '10000000',
           'category_id' => '1',
           'title' => '10000000',
           'desc' => '10000000',
           'price' => '10000000',
           'count' => '10000000',
           'status' => '1',
           'created_at' => '2018-12-20 03:00:04',
           'updated_at' => '2018-12-20 03:04:06',
       ]);

    }
}
        