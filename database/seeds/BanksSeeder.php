
<?php

use Illuminate\Database\Seeder;

class BanksSeeder extends Seeder
{
    public function run()
    {
       DB::table('banks')->insert([
           'id' => '20',
           'user_id' => '1',
           'from' => 'DOGE',
           'take' => '50000',
           'to' => 'COINS',
           'get' => '13000',
           'rate' => '0.0026',
           'created_at' => '2018-12-21 11:54:23',
           'updated_at' => '2018-12-21 11:54:23',
       ]);
       DB::table('banks')->insert([
           'id' => '21',
           'user_id' => '1',
           'from' => 'DOGE',
           'take' => '5000',
           'to' => 'COINS',
           'get' => '1300',
           'rate' => '0.0026',
           'created_at' => '2018-12-21 11:57:04',
           'updated_at' => '2018-12-21 11:57:04',
       ]);
       DB::table('banks')->insert([
           'id' => '22',
           'user_id' => '1',
           'from' => 'DOGE',
           'take' => '5000',
           'to' => 'COINS',
           'get' => '1300',
           'rate' => '0.0026',
           'created_at' => '2018-12-21 12:04:01',
           'updated_at' => '2018-12-21 12:04:01',
       ]);
       DB::table('banks')->insert([
           'id' => '23',
           'user_id' => '1',
           'from' => 'DOGE',
           'take' => '100',
           'to' => 'COINS',
           'get' => '26',
           'rate' => '0.0026',
           'created_at' => '2018-12-21 12:27:39',
           'updated_at' => '2018-12-21 12:27:39',
       ]);
       DB::table('banks')->insert([
           'id' => '24',
           'user_id' => '1',
           'from' => 'DOGE',
           'take' => '100',
           'to' => 'COINS',
           'get' => '26',
           'rate' => '0.0026',
           'created_at' => '2018-12-21 13:31:20',
           'updated_at' => '2018-12-21 13:31:20',
       ]);
       DB::table('banks')->insert([
           'id' => '25',
           'user_id' => '1',
           'from' => 'DOGE',
           'take' => '100',
           'to' => 'COINS',
           'get' => '26',
           'rate' => '0.0026',
           'created_at' => '2018-12-21 13:31:27',
           'updated_at' => '2018-12-21 13:31:27',
       ]);
       DB::table('banks')->insert([
           'id' => '26',
           'user_id' => '1',
           'from' => 'DOGE',
           'take' => '255',
           'to' => 'COINS',
           'get' => '66.3',
           'rate' => '0.0026',
           'created_at' => '2018-12-21 13:43:41',
           'updated_at' => '2018-12-21 13:43:41',
       ]);
       DB::table('banks')->insert([
           'id' => '27',
           'user_id' => '1',
           'from' => 'BTC',
           'take' => '5',
           'to' => 'COINS',
           'get' => '2036329.25',
           'rate' => '4072.6585',
           'created_at' => '2018-12-21 21:11:06',
           'updated_at' => '2018-12-21 21:11:06',
       ]);
       DB::table('banks')->insert([
           'id' => '28',
           'user_id' => '1',
           'from' => 'XRP',
           'take' => '100',
           'to' => 'COINS',
           'get' => '3711',
           'rate' => '0.3711',
           'created_at' => '2018-12-21 21:11:52',
           'updated_at' => '2018-12-21 21:11:52',
       ]);

    }
}
        