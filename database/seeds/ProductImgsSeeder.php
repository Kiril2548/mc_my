
<?php

use Illuminate\Database\Seeder;

class ProductImgsSeeder extends Seeder
{
    public function run()
    {
       DB::table('product_imgs')->insert([
           'id' => '1',
           'product_id' => '1',
           'url' => 'http://casino.me/photos/1/10.png',
           'created_at' => '2018-12-20 02:57:42',
           'updated_at' => '2018-12-20 02:57:42',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '2',
           'product_id' => '2',
           'url' => 'http://casino.me/photos/1/10.png',
           'created_at' => '2018-12-20 02:58:02',
           'updated_at' => '2018-12-20 02:58:02',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '3',
           'product_id' => '3',
           'url' => 'http://casino.me/photos/1/100.png',
           'created_at' => '2018-12-20 02:58:21',
           'updated_at' => '2018-12-20 02:58:21',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '4',
           'product_id' => '4',
           'url' => 'http://casino.me/photos/1/100.png',
           'created_at' => '2018-12-20 02:58:38',
           'updated_at' => '2018-12-20 02:58:38',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '5',
           'product_id' => '5',
           'url' => 'http://casino.me/photos/1/1000.png',
           'created_at' => '2018-12-20 02:58:48',
           'updated_at' => '2018-12-20 02:58:48',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '6',
           'product_id' => '6',
           'url' => 'http://casino.me/photos/1/1000.png',
           'created_at' => '2018-12-20 02:58:58',
           'updated_at' => '2018-12-20 02:58:58',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '7',
           'product_id' => '7',
           'url' => 'http://casino.me/photos/1/4.png',
           'created_at' => '2018-12-20 02:59:13',
           'updated_at' => '2018-12-20 02:59:13',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '8',
           'product_id' => '8',
           'url' => 'http://casino.me/photos/1/4.png',
           'created_at' => '2018-12-20 03:00:04',
           'updated_at' => '2018-12-20 03:00:04',
       ]);

    }
}
        