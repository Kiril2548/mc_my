
<?php

use Illuminate\Database\Seeder;

class UsersRolesSeeder extends Seeder
{
    public function run()
    {
       DB::table('users_roles')->insert([
           'id' => '1',
           'user_id' => '1',
           'role_id' => '1',
       ]);
       DB::table('users_roles')->insert([
           'id' => '2',
           'user_id' => '1',
           'role_id' => '2',
       ]);
       DB::table('users_roles')->insert([
           'id' => '3',
           'user_id' => '1',
           'role_id' => '3',
       ]);
       DB::table('users_roles')->insert([
           'id' => '4',
           'user_id' => '1',
           'role_id' => '4',
       ]);

    }
}
        