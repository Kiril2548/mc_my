
<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    public function run()
    {
       DB::table('settings')->insert([
           'id' => '1',
           'setting_category' => 'MetaTags',
           'key' => 'site_name',
           'value' => 'GWIN-CASINO',
           'created_at' => '2018-11-06 16:46:55',
           'updated_at' => '2018-12-21 00:15:24',
       ]);
       DB::table('settings')->insert([
           'id' => '2',
           'setting_category' => 'MetaTags',
           'key' => 'site_keyword',
           'value' => '1',
           'created_at' => '2018-11-06 16:47:13',
           'updated_at' => '2018-11-06 16:47:13',
       ]);
       DB::table('settings')->insert([
           'id' => '3',
           'setting_category' => 'MetaTags',
           'key' => 'site_short_des',
           'value' => '1',
           'created_at' => '2018-11-06 16:47:27',
           'updated_at' => '2018-11-06 16:47:27',
       ]);
       DB::table('settings')->insert([
           'id' => '4',
           'setting_category' => 'MetaTags',
           'key' => 'site_desc',
           'value' => '1',
           'created_at' => '2018-11-06 16:47:38',
           'updated_at' => '2018-11-06 16:47:38',
       ]);
       DB::table('settings')->insert([
           'id' => '7',
           'setting_category' => 'SiteSettings',
           'key' => 'currency',
           'value' => 'COINS',
           'created_at' => '2018-11-07 14:10:51',
           'updated_at' => '2018-12-21 00:15:05',
       ]);
       DB::table('settings')->insert([
           'id' => '8',
           'setting_category' => 'SiteSettings',
           'key' => 'currency_tag',
           'value' => 'COINS',
           'created_at' => '2018-11-07 14:10:57',
           'updated_at' => '2018-12-21 00:15:05',
       ]);
       DB::table('settings')->insert([
           'id' => '9',
           'setting_category' => 'Contacts',
           'key' => 'email',
           'value' => 'kirhoc0898@gmail.com',
           'created_at' => '2018-11-07 14:11:22',
           'updated_at' => '2018-11-07 14:11:22',
       ]);
       DB::table('settings')->insert([
           'id' => '10',
           'setting_category' => 'Contacts',
           'key' => 'tel',
           'value' => '+380960100925',
           'created_at' => '2018-11-07 14:11:33',
           'updated_at' => '2018-11-07 14:11:33',
       ]);
       DB::table('settings')->insert([
           'id' => '11',
           'setting_category' => 'Contacts',
           'key' => 'telegram',
           'value' => 'gw1nbleydd',
           'created_at' => '2018-11-07 14:11:43',
           'updated_at' => '2018-11-07 14:11:43',
       ]);
       DB::table('settings')->insert([
           'id' => '12',
           'setting_category' => 'Contacts',
           'key' => 'viber',
           'value' => '+380960100925',
           'created_at' => '2018-11-07 14:12:00',
           'updated_at' => '2018-11-07 14:12:00',
       ]);
       DB::table('settings')->insert([
           'id' => '13',
           'setting_category' => 'SiteSettings',
           'key' => 'ves',
           'value' => 'КГ',
           'created_at' => '2018-11-08 21:36:16',
           'updated_at' => '2018-11-08 21:36:16',
       ]);

    }
}
        