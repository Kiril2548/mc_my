
<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
       DB::table('users')->insert([
           'id' => '1',
           'amount_for_ref' => '0',
           'slug' => '1545350303',
           'avatar' => '/unknow_user.png',
           'name' => 'test11@test11.test11',
           'email' => 'test11@test11.test11',
           'balance' => '325',
           'bonus' => '100',
           'coins' => '2040040.25',
           'chance_to_win' => '0.93',
           'baned' => '0',
           'password' => '$2y$10$gZ4DgoHL/E9/cQAYaqFhpeRN3NlSO9tWdhewCtWouSfcWN0QmPat6',
           'created_at' => '2018-12-20 23:58:42',
           'updated_at' => '2018-12-21 21:11:52',
       ]);

    }
}
        