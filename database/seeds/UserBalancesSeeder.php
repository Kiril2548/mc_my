
<?php

use Illuminate\Database\Seeder;

class UserBalancesSeeder extends Seeder
{
    public function run()
    {
       DB::table('user_balances')->insert([
           'id' => '215',
           'user_id' => '1',
           'rate_id' => '1',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:11:06',
       ]);
       DB::table('user_balances')->insert([
           'id' => '216',
           'user_id' => '1',
           'rate_id' => '2',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:11:52',
       ]);
       DB::table('user_balances')->insert([
           'id' => '217',
           'user_id' => '1',
           'rate_id' => '3',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '218',
           'user_id' => '1',
           'rate_id' => '4',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '219',
           'user_id' => '1',
           'rate_id' => '5',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '220',
           'user_id' => '1',
           'rate_id' => '6',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '221',
           'user_id' => '1',
           'rate_id' => '8',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '222',
           'user_id' => '1',
           'rate_id' => '9',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '223',
           'user_id' => '1',
           'rate_id' => '10',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '224',
           'user_id' => '1',
           'rate_id' => '11',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '225',
           'user_id' => '1',
           'rate_id' => '12',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '226',
           'user_id' => '1',
           'rate_id' => '13',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '227',
           'user_id' => '1',
           'rate_id' => '14',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '228',
           'user_id' => '1',
           'rate_id' => '15',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '229',
           'user_id' => '1',
           'rate_id' => '16',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '230',
           'user_id' => '1',
           'rate_id' => '17',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '231',
           'user_id' => '1',
           'rate_id' => '18',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '232',
           'user_id' => '1',
           'rate_id' => '19',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '233',
           'user_id' => '1',
           'rate_id' => '20',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '234',
           'user_id' => '1',
           'rate_id' => '21',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '235',
           'user_id' => '1',
           'rate_id' => '22',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '236',
           'user_id' => '1',
           'rate_id' => '23',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '237',
           'user_id' => '1',
           'rate_id' => '24',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '238',
           'user_id' => '1',
           'rate_id' => '26',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '239',
           'user_id' => '1',
           'rate_id' => '28',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '240',
           'user_id' => '1',
           'rate_id' => '30',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '241',
           'user_id' => '1',
           'rate_id' => '31',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '242',
           'user_id' => '1',
           'rate_id' => '32',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '243',
           'user_id' => '1',
           'rate_id' => '33',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '244',
           'user_id' => '1',
           'rate_id' => '34',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '245',
           'user_id' => '1',
           'rate_id' => '35',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '246',
           'user_id' => '1',
           'rate_id' => '37',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '247',
           'user_id' => '1',
           'rate_id' => '38',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '248',
           'user_id' => '1',
           'rate_id' => '39',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '249',
           'user_id' => '1',
           'rate_id' => '40',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '250',
           'user_id' => '1',
           'rate_id' => '41',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '251',
           'user_id' => '1',
           'rate_id' => '42',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '252',
           'user_id' => '1',
           'rate_id' => '43',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '253',
           'user_id' => '1',
           'rate_id' => '44',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '254',
           'user_id' => '1',
           'rate_id' => '45',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '255',
           'user_id' => '1',
           'rate_id' => '46',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '256',
           'user_id' => '1',
           'rate_id' => '47',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '257',
           'user_id' => '1',
           'rate_id' => '48',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '258',
           'user_id' => '1',
           'rate_id' => '52',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '259',
           'user_id' => '1',
           'rate_id' => '53',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '260',
           'user_id' => '1',
           'rate_id' => '54',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '261',
           'user_id' => '1',
           'rate_id' => '55',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '262',
           'user_id' => '1',
           'rate_id' => '56',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '263',
           'user_id' => '1',
           'rate_id' => '57',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '264',
           'user_id' => '1',
           'rate_id' => '58',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '265',
           'user_id' => '1',
           'rate_id' => '59',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '266',
           'user_id' => '1',
           'rate_id' => '60',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '267',
           'user_id' => '1',
           'rate_id' => '61',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '268',
           'user_id' => '1',
           'rate_id' => '62',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '269',
           'user_id' => '1',
           'rate_id' => '63',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '270',
           'user_id' => '1',
           'rate_id' => '65',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '271',
           'user_id' => '1',
           'rate_id' => '67',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '272',
           'user_id' => '1',
           'rate_id' => '68',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '273',
           'user_id' => '1',
           'rate_id' => '69',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '274',
           'user_id' => '1',
           'rate_id' => '70',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '275',
           'user_id' => '1',
           'rate_id' => '71',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '276',
           'user_id' => '1',
           'rate_id' => '72',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '277',
           'user_id' => '1',
           'rate_id' => '73',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '278',
           'user_id' => '1',
           'rate_id' => '75',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '279',
           'user_id' => '1',
           'rate_id' => '76',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '280',
           'user_id' => '1',
           'rate_id' => '78',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '281',
           'user_id' => '1',
           'rate_id' => '79',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '282',
           'user_id' => '1',
           'rate_id' => '86',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '283',
           'user_id' => '1',
           'rate_id' => '87',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '284',
           'user_id' => '1',
           'rate_id' => '91',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '285',
           'user_id' => '1',
           'rate_id' => '94',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '286',
           'user_id' => '1',
           'rate_id' => '95',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '287',
           'user_id' => '1',
           'rate_id' => '97',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '288',
           'user_id' => '1',
           'rate_id' => '100',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '289',
           'user_id' => '1',
           'rate_id' => '101',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);
       DB::table('user_balances')->insert([
           'id' => '290',
           'user_id' => '1',
           'rate_id' => '102',
           'amount' => '100',
           'created_at' => '2018-12-21 21:07:31',
           'updated_at' => '2018-12-21 21:07:31',
       ]);

    }
}
        