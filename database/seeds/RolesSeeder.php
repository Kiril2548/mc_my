
<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
       DB::table('roles')->insert([
           'id' => '1',
           'name' => 'user',
           'display_name' => 'User',
       ]);
       DB::table('roles')->insert([
           'id' => '2',
           'name' => 'moderator',
           'display_name' => 'Moderator',
       ]);
       DB::table('roles')->insert([
           'id' => '3',
           'name' => 'admin',
           'display_name' => 'Admin',
       ]);
       DB::table('roles')->insert([
           'id' => '4',
           'name' => 'owner',
           'display_name' => 'Owner',
       ]);

    }
}
        