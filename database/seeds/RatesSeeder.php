
<?php

use Illuminate\Database\Seeder;

class RatesSeeder extends Seeder
{
    public function run()
    {
       DB::table('rates')->insert([
           'id' => '1',
           'currency' => 'Bitcoin',
           'crypto_tag' => 'bitcoin',
           'crypto_symbol' => 'BTC',
           'rate' => '4072.6585',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '2',
           'currency' => 'XRP',
           'crypto_tag' => 'ripple',
           'crypto_symbol' => 'XRP',
           'rate' => '0.3711',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '3',
           'currency' => 'Ethereum',
           'crypto_tag' => 'ethereum',
           'crypto_symbol' => 'ETH',
           'rate' => '116.3529',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '4',
           'currency' => 'Bitcoin Cash',
           'crypto_tag' => 'bitcoin-cash',
           'crypto_symbol' => 'BCH',
           'rate' => '217.2631',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '5',
           'currency' => 'EOS',
           'crypto_tag' => 'eos',
           'crypto_symbol' => 'EOS',
           'rate' => '2.7124',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '6',
           'currency' => 'Stellar',
           'crypto_tag' => 'stellar',
           'crypto_symbol' => 'XLM',
           'rate' => '0.1249',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:03',
       ]);
       DB::table('rates')->insert([
           'id' => '7',
           'currency' => 'Bitcoin SV',
           'crypto_tag' => 'bitcoin-sv',
           'crypto_symbol' => 'BSV',
           'rate' => '123.3148',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 21:03:07',
       ]);
       DB::table('rates')->insert([
           'id' => '8',
           'currency' => 'Litecoin',
           'crypto_tag' => 'litecoin',
           'crypto_symbol' => 'LTC',
           'rate' => '32.2546',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '9',
           'currency' => 'Tether',
           'crypto_tag' => 'tether',
           'crypto_symbol' => 'USDT',
           'rate' => '1.0207',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:05',
       ]);
       DB::table('rates')->insert([
           'id' => '10',
           'currency' => 'TRON',
           'crypto_tag' => 'tron',
           'crypto_symbol' => 'TRX',
           'rate' => '0.0195',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:05',
       ]);
       DB::table('rates')->insert([
           'id' => '11',
           'currency' => 'Cardano',
           'crypto_tag' => 'cardano',
           'crypto_symbol' => 'ADA',
           'rate' => '0.0424',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:07',
       ]);
       DB::table('rates')->insert([
           'id' => '12',
           'currency' => 'IOTA',
           'crypto_tag' => 'iota',
           'crypto_symbol' => 'MIOTA',
           'rate' => '0.3384',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:08',
       ]);
       DB::table('rates')->insert([
           'id' => '13',
           'currency' => 'Monero',
           'crypto_tag' => 'monero',
           'crypto_symbol' => 'XMR',
           'rate' => '53.5257',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '14',
           'currency' => 'Dash',
           'crypto_tag' => 'dash',
           'crypto_symbol' => 'DASH',
           'rate' => '94.4251',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '15',
           'currency' => 'Binance Coin',
           'crypto_tag' => 'binance-coin',
           'crypto_symbol' => 'BNB',
           'rate' => '5.7059',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:09',
       ]);
       DB::table('rates')->insert([
           'id' => '16',
           'currency' => 'NEM',
           'crypto_tag' => 'nem',
           'crypto_symbol' => 'XEM',
           'rate' => '0.0734',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:09',
       ]);
       DB::table('rates')->insert([
           'id' => '17',
           'currency' => 'Ethereum Classic',
           'crypto_tag' => 'ethereum-classic',
           'crypto_symbol' => 'ETC',
           'rate' => '4.7506',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '18',
           'currency' => 'NEO',
           'crypto_tag' => 'neo',
           'crypto_symbol' => 'NEO',
           'rate' => '7.104',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '19',
           'currency' => 'Waves',
           'crypto_tag' => 'waves',
           'crypto_symbol' => 'WAVES',
           'rate' => '3.7536',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:10',
       ]);
       DB::table('rates')->insert([
           'id' => '20',
           'currency' => 'Zcash',
           'crypto_tag' => 'zcash',
           'crypto_symbol' => 'ZEC',
           'rate' => '64.3399',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:10',
       ]);
       DB::table('rates')->insert([
           'id' => '21',
           'currency' => 'Dogecoin',
           'crypto_tag' => 'dogecoin',
           'crypto_symbol' => 'DOGE',
           'rate' => '0.0026',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 11:39:46',
       ]);
       DB::table('rates')->insert([
           'id' => '22',
           'currency' => 'Maker',
           'crypto_tag' => 'maker',
           'crypto_symbol' => 'MKR',
           'rate' => '428.8231',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:12',
       ]);
       DB::table('rates')->insert([
           'id' => '23',
           'currency' => 'Tezos',
           'crypto_tag' => 'tezos',
           'crypto_symbol' => 'XTZ',
           'rate' => '0.4744',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:12',
       ]);
       DB::table('rates')->insert([
           'id' => '24',
           'currency' => 'Bitcoin Gold',
           'crypto_tag' => 'bitcoin-gold',
           'crypto_symbol' => 'BTG',
           'rate' => '15.9646',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 13:49:13',
       ]);
       DB::table('rates')->insert([
           'id' => '25',
           'currency' => 'VeChain',
           'crypto_tag' => 'vechain',
           'crypto_symbol' => 'VET',
           'rate' => '0.0045',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:50',
           'updated_at' => '2018-12-21 21:03:21',
       ]);
       DB::table('rates')->insert([
           'id' => '26',
           'currency' => 'OmiseGO',
           'crypto_tag' => 'omisego',
           'crypto_symbol' => 'OMG',
           'rate' => '1.5664',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:14',
       ]);
       DB::table('rates')->insert([
           'id' => '27',
           'currency' => 'USD Coin',
           'crypto_tag' => 'usd-coin',
           'crypto_symbol' => 'USDC',
           'rate' => '1.0146',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:03:28',
       ]);
       DB::table('rates')->insert([
           'id' => '28',
           'currency' => 'Qtum',
           'crypto_tag' => 'qtum',
           'crypto_symbol' => 'QTUM',
           'rate' => '2.3599',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:15',
       ]);
       DB::table('rates')->insert([
           'id' => '29',
           'currency' => 'TrueUSD',
           'crypto_tag' => 'trueusd',
           'crypto_symbol' => 'TUSD',
           'rate' => '1.015',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:03:34',
       ]);
       DB::table('rates')->insert([
           'id' => '30',
           'currency' => '0x',
           'crypto_tag' => '0x',
           'crypto_symbol' => 'ZRX',
           'rate' => '0.3435',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:16',
       ]);
       DB::table('rates')->insert([
           'id' => '31',
           'currency' => 'Ontology',
           'crypto_tag' => 'ontology',
           'crypto_symbol' => 'ONT',
           'rate' => '0.6898',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:20',
       ]);
       DB::table('rates')->insert([
           'id' => '32',
           'currency' => 'Basic Attention Token',
           'crypto_tag' => 'basic-attention-token',
           'crypto_symbol' => 'BAT',
           'rate' => '0.1454',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:20',
       ]);
       DB::table('rates')->insert([
           'id' => '33',
           'currency' => 'Lisk',
           'crypto_tag' => 'lisk',
           'crypto_symbol' => 'LSK',
           'rate' => '1.5442',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:20',
       ]);
       DB::table('rates')->insert([
           'id' => '34',
           'currency' => 'Decred',
           'crypto_tag' => 'decred',
           'crypto_symbol' => 'DCR',
           'rate' => '18.8519',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:19',
       ]);
       DB::table('rates')->insert([
           'id' => '35',
           'currency' => 'Bitcoin Diamond',
           'crypto_tag' => 'bitcoin-diamond',
           'crypto_symbol' => 'BCD',
           'rate' => '1.0612',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:19',
       ]);
       DB::table('rates')->insert([
           'id' => '36',
           'currency' => 'Paxos Standard Token',
           'crypto_tag' => 'paxos-standard-token',
           'crypto_symbol' => 'PAX',
           'rate' => '1.0141',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:03:43',
       ]);
       DB::table('rates')->insert([
           'id' => '37',
           'currency' => 'Stratis',
           'crypto_tag' => 'stratis',
           'crypto_symbol' => 'STRAT',
           'rate' => '1.3465',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:18',
       ]);
       DB::table('rates')->insert([
           'id' => '38',
           'currency' => 'Zilliqa',
           'crypto_tag' => 'zilliqa',
           'crypto_symbol' => 'ZIL',
           'rate' => '0.0181',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:18',
       ]);
       DB::table('rates')->insert([
           'id' => '39',
           'currency' => 'Nano',
           'crypto_tag' => 'nano',
           'crypto_symbol' => 'NANO',
           'rate' => '1.0364',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:17',
       ]);
       DB::table('rates')->insert([
           'id' => '40',
           'currency' => 'Bytecoin',
           'crypto_tag' => 'bytecoin-bcn',
           'crypto_symbol' => 'BCN',
           'rate' => '0.0007',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:17',
       ]);
       DB::table('rates')->insert([
           'id' => '41',
           'currency' => 'DigiByte',
           'crypto_tag' => 'digibyte',
           'crypto_symbol' => 'DGB',
           'rate' => '0.0115',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:26',
       ]);
       DB::table('rates')->insert([
           'id' => '42',
           'currency' => 'Verge',
           'crypto_tag' => 'verge',
           'crypto_symbol' => 'XVG',
           'rate' => '0.008',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:25',
       ]);
       DB::table('rates')->insert([
           'id' => '43',
           'currency' => 'ICON',
           'crypto_tag' => 'icon',
           'crypto_symbol' => 'ICX',
           'rate' => '0.2545',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:25',
       ]);
       DB::table('rates')->insert([
           'id' => '44',
           'currency' => 'Chainlink',
           'crypto_tag' => 'chainlink',
           'crypto_symbol' => 'LINK',
           'rate' => '0.3193',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:25',
       ]);
       DB::table('rates')->insert([
           'id' => '45',
           'currency' => 'BitShares',
           'crypto_tag' => 'bitshares',
           'crypto_symbol' => 'BTS',
           'rate' => '0.043',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:24',
       ]);
       DB::table('rates')->insert([
           'id' => '46',
           'currency' => 'Siacoin',
           'crypto_tag' => 'siacoin',
           'crypto_symbol' => 'SC',
           'rate' => '0.0028',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:24',
       ]);
       DB::table('rates')->insert([
           'id' => '47',
           'currency' => 'Revain',
           'crypto_tag' => 'revain',
           'crypto_symbol' => 'R',
           'rate' => '0.206',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:23',
       ]);
       DB::table('rates')->insert([
           'id' => '48',
           'currency' => 'Aeternity',
           'crypto_tag' => 'aeternity',
           'crypto_symbol' => 'AE',
           'rate' => '0.478',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:22',
       ]);
       DB::table('rates')->insert([
           'id' => '49',
           'currency' => 'Aurora',
           'crypto_tag' => 'aurora',
           'crypto_symbol' => 'AOA',
           'rate' => '0.0134',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:03:52',
       ]);
       DB::table('rates')->insert([
           'id' => '50',
           'currency' => 'Gemini Dollar',
           'crypto_tag' => 'gemini-dollar',
           'crypto_symbol' => 'GUSD',
           'rate' => '1.0129',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:03:58',
       ]);
       DB::table('rates')->insert([
           'id' => '51',
           'currency' => 'Pundi X',
           'crypto_tag' => 'pundi-x',
           'crypto_symbol' => 'NPXS',
           'rate' => '0.0006',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:04',
       ]);
       DB::table('rates')->insert([
           'id' => '52',
           'currency' => 'Factom',
           'crypto_tag' => 'factom',
           'crypto_symbol' => 'FCT',
           'rate' => '9.872',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:31',
       ]);
       DB::table('rates')->insert([
           'id' => '53',
           'currency' => 'Steem',
           'crypto_tag' => 'steem',
           'crypto_symbol' => 'STEEM',
           'rate' => '0.2817',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:30',
       ]);
       DB::table('rates')->insert([
           'id' => '54',
           'currency' => 'Populous',
           'crypto_tag' => 'populous',
           'crypto_symbol' => 'PPT',
           'rate' => '1.5439',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:30',
       ]);
       DB::table('rates')->insert([
           'id' => '55',
           'currency' => 'Komodo',
           'crypto_tag' => 'komodo',
           'crypto_symbol' => 'KMD',
           'rate' => '0.7645',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:30',
       ]);
       DB::table('rates')->insert([
           'id' => '56',
           'currency' => 'Bytom',
           'crypto_tag' => 'bytom',
           'crypto_symbol' => 'BTM',
           'rate' => '0.0861',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:29',
       ]);
       DB::table('rates')->insert([
           'id' => '57',
           'currency' => 'Augur',
           'crypto_tag' => 'augur',
           'crypto_symbol' => 'REP',
           'rate' => '7.3465',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:29',
       ]);
       DB::table('rates')->insert([
           'id' => '58',
           'currency' => 'Electroneum',
           'crypto_tag' => 'electroneum',
           'crypto_symbol' => 'ETN',
           'rate' => '0.0081',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:29',
       ]);
       DB::table('rates')->insert([
           'id' => '59',
           'currency' => 'Holo',
           'crypto_tag' => 'holo',
           'crypto_symbol' => 'HOT',
           'rate' => '0.0005',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:28',
       ]);
       DB::table('rates')->insert([
           'id' => '60',
           'currency' => 'MaidSafeCoin',
           'crypto_tag' => 'maidsafecoin',
           'crypto_symbol' => 'MAID',
           'rate' => '0.1433',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:27',
       ]);
       DB::table('rates')->insert([
           'id' => '61',
           'currency' => 'Golem',
           'crypto_tag' => 'golem-network-tokens',
           'crypto_symbol' => 'GNT',
           'rate' => '0.0701',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:36',
       ]);
       DB::table('rates')->insert([
           'id' => '62',
           'currency' => 'Cryptonex',
           'crypto_tag' => 'cryptonex',
           'crypto_symbol' => 'CNX',
           'rate' => '1.1851',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:36',
       ]);
       DB::table('rates')->insert([
           'id' => '63',
           'currency' => 'Status',
           'crypto_tag' => 'status',
           'crypto_symbol' => 'SNT',
           'rate' => '0.0184',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:36',
       ]);
       DB::table('rates')->insert([
           'id' => '64',
           'currency' => 'Huobi Token',
           'crypto_tag' => 'huobi-token',
           'crypto_symbol' => 'HT',
           'rate' => '1.1641',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:13',
       ]);
       DB::table('rates')->insert([
           'id' => '65',
           'currency' => 'IOST',
           'crypto_tag' => 'iostoken',
           'crypto_symbol' => 'IOST',
           'rate' => '0.0049',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:35',
       ]);
       DB::table('rates')->insert([
           'id' => '66',
           'currency' => 'Dai',
           'crypto_tag' => 'dai',
           'crypto_symbol' => 'DAI',
           'rate' => '1.0132',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:19',
       ]);
       DB::table('rates')->insert([
           'id' => '67',
           'currency' => 'KuCoin Shares',
           'crypto_tag' => 'kucoin-shares',
           'crypto_symbol' => 'KCS',
           'rate' => '0.6492',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:34',
       ]);
       DB::table('rates')->insert([
           'id' => '68',
           'currency' => 'QASH',
           'crypto_tag' => 'qash',
           'crypto_symbol' => 'QASH',
           'rate' => '0.1567',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:34',
       ]);
       DB::table('rates')->insert([
           'id' => '69',
           'currency' => 'Decentraland',
           'crypto_tag' => 'decentraland',
           'crypto_symbol' => 'MANA',
           'rate' => '0.0555',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:33',
       ]);
       DB::table('rates')->insert([
           'id' => '70',
           'currency' => 'Ardor',
           'crypto_tag' => 'ardor',
           'crypto_symbol' => 'ARDR',
           'rate' => '0.0531',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:33',
       ]);
       DB::table('rates')->insert([
           'id' => '71',
           'currency' => 'Bitcoin Private',
           'crypto_tag' => 'bitcoin-private',
           'crypto_symbol' => 'BTCP',
           'rate' => '2.5481',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:41',
       ]);
       DB::table('rates')->insert([
           'id' => '72',
           'currency' => 'TenX',
           'crypto_tag' => 'tenx',
           'crypto_symbol' => 'PAY',
           'rate' => '0.4724',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:40',
       ]);
       DB::table('rates')->insert([
           'id' => '73',
           'currency' => 'Waltonchain',
           'crypto_tag' => 'waltonchain',
           'crypto_symbol' => 'WTC',
           'rate' => '1.2071',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:40',
       ]);
       DB::table('rates')->insert([
           'id' => '74',
           'currency' => 'Insight Chain',
           'crypto_tag' => 'insight-chain',
           'crypto_symbol' => 'INB',
           'rate' => '0.2953',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:28',
       ]);
       DB::table('rates')->insert([
           'id' => '75',
           'currency' => 'MonaCoin',
           'crypto_tag' => 'monacoin',
           'crypto_symbol' => 'MONA',
           'rate' => '0.7076',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:39',
       ]);
       DB::table('rates')->insert([
           'id' => '76',
           'currency' => 'Aion',
           'crypto_tag' => 'aion',
           'crypto_symbol' => 'AION',
           'rate' => '0.176',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:39',
       ]);
       DB::table('rates')->insert([
           'id' => '77',
           'currency' => 'Nexo',
           'crypto_tag' => 'nexo',
           'crypto_symbol' => 'NEXO',
           'rate' => '0.0837',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:34',
       ]);
       DB::table('rates')->insert([
           'id' => '78',
           'currency' => 'Dentacoin',
           'crypto_tag' => 'dentacoin',
           'crypto_symbol' => 'DCN',
           'rate' => '0.0001',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:38',
       ]);
       DB::table('rates')->insert([
           'id' => '79',
           'currency' => 'Polymath',
           'crypto_tag' => 'polymath-network',
           'crypto_symbol' => 'POLY',
           'rate' => '0.1476',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:38',
       ]);
       DB::table('rates')->insert([
           'id' => '80',
           'currency' => 'Ark',
           'crypto_tag' => 'ark',
           'crypto_symbol' => 'ARK',
           'rate' => '0.3953',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:40',
       ]);
       DB::table('rates')->insert([
           'id' => '81',
           'currency' => 'Elastos',
           'crypto_tag' => 'elastos',
           'crypto_symbol' => 'ELA',
           'rate' => '2.9354',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:45',
       ]);
       DB::table('rates')->insert([
           'id' => '82',
           'currency' => 'Digitex Futures',
           'crypto_tag' => 'digitex-futures',
           'crypto_symbol' => 'DGTX',
           'rate' => '0.0574',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:51',
       ]);
       DB::table('rates')->insert([
           'id' => '83',
           'currency' => 'Wanchain',
           'crypto_tag' => 'wanchain',
           'crypto_symbol' => 'WAN',
           'rate' => '0.3884',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:04:56',
       ]);
       DB::table('rates')->insert([
           'id' => '84',
           'currency' => 'Linkey',
           'crypto_tag' => 'linkey',
           'crypto_symbol' => 'LKY',
           'rate' => '0.8609',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:00',
       ]);
       DB::table('rates')->insert([
           'id' => '85',
           'currency' => 'DEX',
           'crypto_tag' => 'dex',
           'crypto_symbol' => 'DEX',
           'rate' => '0.1885',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:04',
       ]);
       DB::table('rates')->insert([
           'id' => '86',
           'currency' => 'WAX',
           'crypto_tag' => 'wax',
           'crypto_symbol' => 'WAX',
           'rate' => '0.0395',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:44',
       ]);
       DB::table('rates')->insert([
           'id' => '87',
           'currency' => 'ReddCoin',
           'crypto_tag' => 'reddcoin',
           'crypto_symbol' => 'RDD',
           'rate' => '0.0014',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:44',
       ]);
       DB::table('rates')->insert([
           'id' => '88',
           'currency' => 'MobileGo',
           'crypto_tag' => 'mobilego',
           'crypto_symbol' => 'MGO',
           'rate' => '0.4083',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:10',
       ]);
       DB::table('rates')->insert([
           'id' => '89',
           'currency' => 'HyperCash',
           'crypto_tag' => 'hypercash',
           'crypto_symbol' => 'HC',
           'rate' => '0.9327',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:18',
       ]);
       DB::table('rates')->insert([
           'id' => '90',
           'currency' => 'QuarkChain',
           'crypto_tag' => 'quarkchain',
           'crypto_symbol' => 'QKC',
           'rate' => '0.0453',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:22',
       ]);
       DB::table('rates')->insert([
           'id' => '91',
           'currency' => 'PIVX',
           'crypto_tag' => 'pivx',
           'crypto_symbol' => 'PIVX',
           'rate' => '0.6435',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:51',
       ]);
       DB::table('rates')->insert([
           'id' => '92',
           'currency' => 'Ravencoin',
           'crypto_tag' => 'ravencoin',
           'crypto_symbol' => 'RVN',
           'rate' => '0.0163',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:31',
       ]);
       DB::table('rates')->insert([
           'id' => '93',
           'currency' => 'Mixin',
           'crypto_tag' => 'mixin',
           'crypto_symbol' => 'XIN',
           'rate' => '85.4721',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:37',
       ]);
       DB::table('rates')->insert([
           'id' => '94',
           'currency' => 'Bancor',
           'crypto_tag' => 'bancor',
           'crypto_symbol' => 'BNT',
           'rate' => '0.5897',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:06:34',
       ]);
       DB::table('rates')->insert([
           'id' => '95',
           'currency' => 'Metaverse ETP',
           'crypto_tag' => 'metaverse',
           'crypto_symbol' => 'ETP',
           'rate' => '0.6948',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:49',
       ]);
       DB::table('rates')->insert([
           'id' => '96',
           'currency' => 'Crypto.com Chain',
           'crypto_tag' => 'crypto-com-chain',
           'crypto_symbol' => 'CRO',
           'rate' => '0.0367',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:52',
       ]);
       DB::table('rates')->insert([
           'id' => '97',
           'currency' => 'aelf',
           'crypto_tag' => 'aelf',
           'crypto_symbol' => 'ELF',
           'rate' => '0.1248',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:49',
       ]);
       DB::table('rates')->insert([
           'id' => '98',
           'currency' => 'ProximaX',
           'crypto_tag' => 'proximax',
           'crypto_symbol' => 'XPX',
           'rate' => '0.0058',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:05:57',
       ]);
       DB::table('rates')->insert([
           'id' => '99',
           'currency' => 'Mithril',
           'crypto_tag' => 'mithril',
           'crypto_symbol' => 'MITH',
           'rate' => '0.0717',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 21:07:11',
       ]);
       DB::table('rates')->insert([
           'id' => '100',
           'currency' => 'THETA',
           'crypto_tag' => 'theta',
           'crypto_symbol' => 'THETA',
           'rate' => '0.0506',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:51',
           'updated_at' => '2018-12-21 13:49:47',
       ]);
       DB::table('rates')->insert([
           'id' => '101',
           'currency' => 'Power Ledger',
           'crypto_tag' => 'power-ledger',
           'crypto_symbol' => 'POWR',
           'rate' => '0.095',
           'status' => '1',
           'created_at' => '2018-12-21 00:06:04',
           'updated_at' => '2018-12-21 13:49:54',
       ]);
       DB::table('rates')->insert([
           'id' => '102',
           'currency' => 'Enjin Coin',
           'crypto_tag' => 'enjin-coin',
           'crypto_symbol' => 'ENJ',
           'rate' => '0.0484',
           'status' => '1',
           'created_at' => '2018-12-21 11:09:28',
           'updated_at' => '2018-12-21 13:49:55',
       ]);
       DB::table('rates')->insert([
           'id' => '103',
           'currency' => 'STASIS EURS',
           'crypto_tag' => 'stasis-eurs',
           'crypto_symbol' => 'EURS',
           'rate' => '1.1329',
           'status' => '0',
           'created_at' => '2018-12-21 11:09:28',
           'updated_at' => '2018-12-21 21:07:06',
       ]);

    }
}
        