
<?php

use Illuminate\Database\Seeder;

class ArticleCategoriesSeeder extends Seeder
{
    public function run()
    {
       DB::table('article_categories')->insert([
           'id' => '1',
           'title' => 'Fahey, Treutel and Kuhic',
           'slug' => 'et-quam-enim-nisi-impedit',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:42',
           'updated_at' => '2018-12-20 23:58:42',
       ]);
       DB::table('article_categories')->insert([
           'id' => '2',
           'title' => 'Kutch PLC',
           'slug' => 'voluptatem-ullam-eligendi-ea-omnis',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:42',
           'updated_at' => '2018-12-20 23:58:42',
       ]);
       DB::table('article_categories')->insert([
           'id' => '3',
           'title' => 'Cassin LLC',
           'slug' => 'qui-vel-iure-laudantium-nihil-nemo-pariatur',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:42',
           'updated_at' => '2018-12-20 23:58:42',
       ]);
       DB::table('article_categories')->insert([
           'id' => '4',
           'title' => 'Pollich, Haag and Borer',
           'slug' => 'ea-eos-et-dolores-eius',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-12-20 23:58:42',
           'updated_at' => '2018-12-20 23:58:42',
       ]);
       DB::table('article_categories')->insert([
           'id' => '5',
           'title' => 'Bernier, Koepp and Mosciski',
           'slug' => 'incidunt-expedita-expedita-qui',
           'place' => '1',
           'status' => '0',
           'created_at' => '2018-12-20 23:58:42',
           'updated_at' => '2018-12-20 23:58:42',
       ]);

    }
}
        