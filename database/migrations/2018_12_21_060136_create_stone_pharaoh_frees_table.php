<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStonePharaohFreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stone_pharaoh_frees', function (Blueprint $table) {
            $table->increments('id');

            $table->string('type');
            $table->string('name');

            $table->integer('steps');
            $table->string('mode');

            $table->string('cell_row_select')->nullable();
            $table->string('cell_column_select')->nullable();

            $table->string('quantity_row');
            $table->string('quantity_column');

            $table->string('stone_value');

            $table->string('processing');

            $table->boolean('auto_spin')->default(false);

            $table->double('bet', 32, 8)->default(0.00000000);

            $table->double('win_cash', 32, 8)->default(0.00000000);

            $table->double('balance_before', 32, 8)->default(0.00000000);
            $table->double('balance_after', 32, 8)->default(0.00000000);

            $table->json('combines');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stone_pharaoh_frees');
    }
}
