<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('ref_id')->nullable();
            $table->double('amount_for_ref', 8, 2)->default(0.00);

            $table->string('slug')->default(time()+rand(0,5));

            $table->string('avatar')->default('/unknow_user.png');

            $table->string('name');
            $table->string('email')->unique();

            $table->double('balance', 32,2)->default(0.00000000); //Можно вывесли
            $table->double('bonus', 32,2)->default(0.00000000);
            $table->double('coins', 32,2)->default(0.00000000);

            $table->double('chance_to_win', 8,2)->default(1);

            $table->boolean('baned')->default(0);

            $table->timestamp('email_verified_at')->nullable();

            $table->string('password');

            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
