<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotGameLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slot_game_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');

            $table->string('type');
            $table->string('game');

            $table->integer('steps');
            $table->string('mode');

            $table->boolean('win')->nullable();

            $table->boolean('auto_spin')->default(false);

            $table->double('price', 32, 8)->default(0.00000000);
            $table->double('range', 32, 8)->default(0.00000000);

            $table->double('win_cash', 32, 8)->default(0.00000000);
            $table->double('balance_before', 32, 8)->default(0.00000000);
            $table->double('balance_after', 32, 8)->default(0.00000000);

            $table->json('combine');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slot_game_logs');
    }
}
