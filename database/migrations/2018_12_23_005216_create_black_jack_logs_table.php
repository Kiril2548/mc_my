<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlackJackLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('black_jack_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');

            $table->string('type')->default('black jack');
            $table->string('name')->default('black jack');

            $table->string('mode')->default('pay');

            $table->boolean('status')->nullable();

            $table->double('bet', 32 , 8)->default(0.00000000);
            $table->double('rate',8,2)->default(0);

            $table->double('win_cash', 32 , 8)->default(0.00000000);
            $table->double('balance_before', 32 , 8)->default(0.00000000);
            $table->double('balance_after', 32 , 8)->default(0.00000000);

            $table->json('dealer_card');
            $table->json('user_card');
            $table->json('deck_last');
            $table->json('deck');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('black_jack_logs');
    }
}
