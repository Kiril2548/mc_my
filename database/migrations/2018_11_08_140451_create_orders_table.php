<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('order_id')->nullable();

            $table->unsignedInteger('category_id')->nullable();

            $table->unsignedInteger('user_id')->nullable();
            $table->string('who')->default('user');

            $table->unsignedInteger('product_id')->nullable();


            $table->string('count')->nullable();


            $table->string('status')->default('В ожидании');

            $table->double('amount', 8, 5)->default(0);

            $table->string('error_des')->nullable();
            $table->string('type')->default('default');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
