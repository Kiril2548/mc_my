<div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
            <div class="sidebar-brand">
                <a href="{{ route('main') }}"><img src="{{ asset('favicon.ico') }}" width="35" height="35"
                                                   style="margin-right: 5px;"> AdminPanel</a>
                <div id="close-sidebar">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="sidebar-header">
                <div class="user-pic">
                    <img class="img-responsive img-rounded" src="{{ Auth::user()->avatar }}" alt="User picture">
                </div>
                <div class="user-info">
                        <span class="user-name">{{ Auth::user()->name }}
                            <strong>_</strong>
                        </span>
                    <span
                        class="user-role">{{ Auth::user()->roles[count((Auth::user()->roles))-1]->display_name }}</span>
                    <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                </div>
            </div>
            <!-- sidebar-header  -->
        {{--<div class="sidebar-search">--}}
        {{--<div>--}}
        {{--<div class="input-group">--}}
        {{--<input type="text" class="form-control search-menu" placeholder="Search...">--}}
        {{--<div class="input-group-append">--}}
        {{--<span class="input-group-text">--}}
        {{--<i class="fa fa-search" aria-hidden="true"></i>--}}
        {{--</span>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <!-- sidebar-search  -->
            <div class="sidebar-menu">
                <ul>
                    <li class="header-menu">
                        <span>Default</span>
                    </li>
                    @if($admin_settings['e_commerce_d'] == 1 || $admin_settings['blog_d'] == 1)
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-tachometer-alt"></i>
                                <span>Админка</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    @if($admin_settings['e_commerce_d'] == 1)
                                        <li>
                                            <a href="{{ route('admin.main') }}">E-commerce</a>
                                        </li>
                                    @endif

                                    @if($admin_settings['blog_d'] == 1)
                                        <li>
                                            <a href="{{ route('admin.main') }}">Blog</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-users"></i>
                            <span>Пользователи</span>
                        </a>
                    </li>

                    @if($admin_settings['session'] == 1)
                        <li>
                            <a href="{{ route('admin.sessions.index') }}">
                                <i class="fa fa-globe"></i>
                                <span>Сессии</span>
                            </a>
                        </li>
                    @endif


                    @if(
                        $admin_settings['category'] == 1
                        || $admin_settings['products'] == 1
                        || $admin_settings['orders'] == 1
                        || $admin_settings['payments'] == 1
                        || $admin_settings['carts'] == 1
                    )
                        <li class="header-menu">
                            <span>E-commerce</span>
                        </li>

                        @if($admin_settings['category'] == 1)
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-clipboard-list"></i>
                                    <span>Категории (продукты)</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('admin.categories.index') }}">Все категории</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.categories.place') }}">Позицыи Категорий</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endif

                        @if($admin_settings['products'] == 1)
                            <li>
                                <a href="{{ route('admin.products.index') }}">
                                    <i class="fa fa-briefcase"></i>
                                    <span>Продукты</span>
                                </a>
                            </li>
                        @endif

                        <li>
                            <a href="{{ route('admin.rates.index') }}">
                                <i class="fa fa-coins"></i>
                                <span>Crypto currency rate</span>
                            </a>
                        </li>

                        @if($admin_settings['orders'] == 1)
                            <li>
                                <a href="#">
                                    <i class="fa fa-archive"></i>
                                    <span>Заказы</span>
                                </a>
                            </li>
                        @endif

                        @if($admin_settings['payments'] == 1)
                            <li>
                                <a href="#">
                                    <i class="fa fa-credit-card"></i>
                                    <span>Транзакции</span>
                                </a>
                            </li>
                        @endif

                        @if($admin_settings['carts'] == 1)
                            <li>
                                <a href="#">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span>Карзина</span>
                                </a>
                            </li>
                        @endif
                    @endif

                    @if(
                        $admin_settings['pages'] == 1
                        || $admin_settings['news'] == 1
                        || $admin_settings['articles'] == 1
                        || $admin_settings['texts'] == 1
                    )
                        <li class="header-menu">
                            <span>Content</span>
                        </li>

                        @if($admin_settings['pages'] == 1)
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-file-alt"></i>
                                    <span>Страницы</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('admin.menus.index') }}">Меню</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.pages.index') }}">Страницы</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endif

                        @if($admin_settings['news'] == 1)
                            <li>
                                <a href="{{ route('admin.news.index') }}">
                                    <i class="fa fa-newspaper"></i>
                                    <span>Новости</span>
                                </a>
                            </li>
                        @endif

                        @if($admin_settings['article_categories'] == 1)
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-clipboard-list"></i>
                                    <span>Категории (статьи)</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('admin.article-categories.index') }}">Все категории</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.article-categories.place') }}">Позицыи
                                                Категорий</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endif

                        @if($admin_settings['articles'] == 1)
                            <li>
                                <a href="{{ route('admin.articles.index') }}">
                                    <i class="fa fa-bookmark"></i>
                                    <span>Статьи</span>
                                </a>
                            </li>
                        @endif

                        @if($admin_settings['texts'] == 1)
                            <li>
                                <a href="{{ route('admin.texts.index') }}">
                                    <i class="fa fa-font"></i>
                                    <span>Текста</span>
                                </a>
                            </li>
                        @endif
                    @endif

                    @if(Auth::user()->hasRole('admin'))
                        @if(
                            $admin_settings['api_settings'] == 1
                            || $admin_settings['settings'] == 1
                            || $admin_settings['roles'] == 1
                            || $admin_settings['permissions'] == 1
                        )
                            <li class="header-menu">
                                <span>Settings</span>
                            </li>

                            @if($admin_settings['settings'] == 1)
                                <li>
                                    <a href="{{ route('admin.settings.index') }}">
                                        <i class="fa fa-cog"></i>
                                        <span>Настройки</span>
                                    </a>
                                </li>
                            @endif

                            @if($admin_settings['api_settings'] == 1)
                                <li>
                                    <a href="#">
                                        <i class="fa fa-lock"></i>
                                        <span>Апи настрайки</span>
                                    </a>
                                </li>
                            @endif

                            @if($admin_settings['roles'] == 1)
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user"></i>
                                        <span>Роли</span>
                                    </a>
                                </li>
                            @endif

                            @if($admin_settings['permissions'] == 1)
                                <li>
                                    <a href="#">
                                        <i class="fa fa-terminal"></i>
                                        <span>Права</span>
                                    </a>
                                </li>
                            @endif
                        @endif
                    @endif

                    @if(Auth::user()->hasRole('owner'))
                        <li class="header-menu">
                            <span>Misc</span>
                        </li>
                        <li>
                            <a href="{{ route('admin.documentation') }}">
                                <i class="fa fa-question"></i>
                                <span>Documentation</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('admin.admin_settings.index') }}">
                                <i class="fa fa-cogs"></i>
                                <span>Настройки админки</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- sidebar-menu  -->
        </div>
        <!-- sidebar-content  -->

        {{--<div class="sidebar-footer">--}}
        {{--<a href="#">--}}
        {{--<i class="fa fa-bell"></i>--}}
        {{--<span class="badge badge-pill badge-warning notification">3</span>--}}
        {{--</a>--}}
        {{--<a href="#">--}}
        {{--<i class="fa fa-envelope"></i>--}}
        {{--<span class="badge badge-pill badge-success notification">7</span>--}}
        {{--</a>--}}
        {{--<a href="#">--}}
        {{--<i class="fa fa-cog"></i>--}}
        {{--<span class="badge-sonar"></span>--}}
        {{--</a>--}}
        {{--<a href="#">--}}
        {{--<i class="fa fa-power-off"></i>--}}
        {{--</a>--}}
        {{--</div>--}}
    </nav>
    <!-- sidebar-wrapper  -->
</div>
<!-- page-wrapper -->
