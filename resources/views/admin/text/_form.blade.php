<b-field label="Страница"
         :type="errors.page ? 'is-danger' : ''"
         :message="errors.page ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="text.page" maxlength="50" placeholder="main_page"></b-input>
</b-field>

<b-field label="Ключ"
         :type="errors.key ? 'is-danger' : ''"
         :message="errors.key ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="text.key" maxlength="50" placeholder="header"></b-input>
</b-field>

<b-field label="Текст"
         :type="errors.text ? 'is-danger' : ''"
         :message="errors.text ? 'Поле обязательно к заполнению' : ''">
    <b-input type="textarea" v-model="text.text" maxlength="50000" placeholder="Текст блока страницы"></b-input>
</b-field>

<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
