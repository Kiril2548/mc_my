@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.texts.index') }}">Текста</a></li>
                        <li><a href="#">Редактирование Текста: {{ $text->key }}</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;">@include('admin.text._form')</div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        var lfm = function (options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=1280,height=600');
            window.SetUrl = cb;
        }
    </script>
    <script>
        new Vue({
            el: "#content",
            data: {
                errors: {},
                text: @json($text),
            },
            methods: {
                onSave() {
                    let self = this;
                    axios.put(route('admin.texts.update', self.text.id), self.text)
                        .then(function (response) {
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Текст успешно сохранена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
