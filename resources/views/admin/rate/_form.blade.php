<b-field label="Тег"
         :type="errors.crypto_symbol ? 'is-danger' : ''"
         :message="errors.crypto_symbol ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="rate.crypto_symbol" maxlength="50" placeholder="BTC"></b-input>
</b-field>

<b-field label="Название"
         :type="errors.currency ? 'is-danger' : ''"
         :message="errors.currency ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="rate.currency" maxlength="50" placeholder="Bitcoin"></b-input>
</b-field>

<b-field label="Абривиатура"
         :type="errors.crypto_tag ? 'is-danger' : ''"
         :message="errors.crypto_tag ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="rate.crypto_tag" maxlength="50" placeholder="bitcoin"></b-input>
</b-field>

<b-field label="USD в 1"
         :type="errors.rate ? 'is-danger' : ''"
         :message="errors.rate ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="rate.rate" maxlength="32" placeholder="0.0000"></b-input>
</b-field>

<b-field
    label="Статус">
    <b-select v-model="category.status" placeholder="ACTIVE" expanded>
        <option value="1">ACTIVE</option>
        <option value="0">DELETE</option>
    </b-select>
</b-field>

<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
