@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.rates.index') }}">Rates</a></li>
                    </ul>
                </nav>
            </div>

            <div class="card col-md-12" style="padding: 5px;">
                <header class="card-header" style="padding: 0; padding-bottom: 5px; padding-top: 5px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <b-field
                                    label="Позицый на страницу">
                                    <b-select v-model="per_page" expanded>
                                        <option v-for="item in pre_page_a" :value="item">@{{ item }}</option>
                                    </b-select>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                <b-field
                                    label="Поиск">
                                    <div class="field has-addons">
                                        <div class="control" style="width: 100%;">
                                            <input class="input" type="text" v-model="filter" placeholder="Поиск">
                                        </div>
                                        <div class="control">
                                            <a class="button is-primary is-outlined" @click="onSearch(filter)">
                                                Найти
                                            </a>
                                        </div>
                                    </div>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                <button class="button is-primary is-smal" style="float: right; margin-bottom: 10px; margin-top: 15px;"
                                        @click="isCardModalActive = true">
                                    Добавить Валюту
                                </button>
                                <b-modal :active.sync="isCardModalActive" :width="640" scroll="keep">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="content">
                                                @include('admin.rate._form')
                                            </div>
                                        </div>
                                    </div>
                                </b-modal>
                            </div>
                        </div>
                    </div>
                </header>
                <b-table
                    :data="data"
                    paginated
                    hoverable
                    mobile-cards
                    :filter="filter"
                    :per-page="per_page">
                    <template slot-scope="props">

                        <b-table-column field="id" label="ID" width="40" sortable numeric>
                            @{{ props.row.id }}
                        </b-table-column>

                        <b-table-column field="crypto_symbol" label="CRYPTO" sortable>
                            @{{ props.row.crypto_symbol }}
                        </b-table-column>

                        <b-table-column field="crypto_cyrrency" label="CRYPTO" sortable>
                            @{{ props.row.currency }} (@{{ props.row.crypto_tag }})
                        </b-table-column>

                        <b-table-column field="rate" label="CRYPTO" sortable>
                            USD <b>@{{ props.row.rate }}</b>
                        </b-table-column>

                        <b-table-column field="status" label="Статус" sortable centered>
                            <span class="tag is-success" v-if="props.row.status == 1">
                                ACTIVE
                            </span>
                            <span class="tag is-danger" v-if="props.row.status == 0">
                                DELETE
                            </span>
                        </b-table-column>

                        <b-table-column field="action" label="" sortable numeric>
                            <div class="btn-group">
                                <a :href="routeEdit+ '/' + props.row.id + '/edit'"
                                   class="btn btn-outline-primary btn-sm">EDIT</a>
                                <button @click="onDelete(props.row.id)" class="btn btn-outline-danger btn-sm"
                                        v-if="props.row.status == 1">DELETE
                                </button>
                                <button @click="onDelete(props.row.id)" class="btn btn-outline-success btn-sm"
                                        v-if="props.row.status == 0">ACTIVATE
                                </button>
                            </div>
                        </b-table-column>
                    </template>
                    <template slot="empty">
                        <section class="section">
                            <div class="content has-text-grey has-text-centered">
                                <p>
                                    <b-icon
                                        icon="emoticon-sad"
                                        size="is-large">
                                    </b-icon>
                                </p>
                                <p>Ничего нет.</p>
                            </div>
                        </section>
                    </template>
                </b-table>

            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        new Vue({
            el: "#content",
            data: {
                errors: {},
                routeEdit: route('admin.rates.index'),
                filter: '',
                per_page: 10,
                pre_page_a: [5, 10, 20],
                category: {
                    category_id: '',
                    status: 1
                },
                isCardModalActive: false,
                data: @json($rates),
                rate: {}
            },
            methods: {
                onDelete(index) {
                    let self = this;
                    axios.delete(route('admin.rates.destroy', index))
                        .then(function (response) {
                            self.data = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Статус Rate успешно изменен`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSave() {
                    let self = this;
                    axios.post(route('admin.rates.store'), self.rate)
                        .then(function (response) {
                            self.data = response.data;
                            self.rate = {};
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Rate успешно сохранена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSearch(search) {
                    let self = this;
                    axios.post(route('admin.rates.search'), {
                        search: search
                    })
                        .then(function (response) {
                            self.data = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Поиск прошел успешно`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время поиска произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
