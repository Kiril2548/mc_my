<b-field label="Категория *">
    <b-input v-model="setting.setting_category" style="background-color: #fff;
    border-color: #dbdbdb;
    color: #363636;
    box-shadow: inset 0 1px 2px hsla(0,0%,4%,.1);
    max-width: 100%;
    width: 100%;" list="category"></b-input>
    <datalist id="category">
        <option v-for="s_c in settings_category" :value="s_c">
    </datalist>
</b-field>

<b-field label="Ключ *">
    <b-input v-model="setting.key"></b-input>
</b-field>

<b-field label="Значение *">
    <b-input v-model="setting.value"></b-input>
</b-field>

<b-field label="Дополнение">
    <b-input v-model="setting.more"></b-input>
</b-field>



{{--************************************************--}}
<div style="text-align: right;">
    <a class="button is-success" style="width: 75px;
    float: right;" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
