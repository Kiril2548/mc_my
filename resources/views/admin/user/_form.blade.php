<b-field label="Имя"
         :type="errors.name ? 'is-danger' : ''"
         :message="errors.name ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="user.name" maxlength="50" placeholder="Вася"></b-input>
</b-field>

<b-field label="E-mail"
         :type="errors.email ? 'is-danger' : ''"
         :message="errors.email ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="user.email" maxlength="50" placeholder="vasy@mail.ru"></b-input>
</b-field>

<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
