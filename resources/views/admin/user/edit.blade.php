@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.users.index') }}">Пользователи</a></li>
                        <li><a href="#">Редактирование пользователя: <b>{{ $user->name }}</b></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;">@include('admin.user._form')</div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        new Vue({
            el: "#content",
            data: {
                errors: {},
                user: @json($user),
            },
            methods: {
                onSave() {
                    let self = this;
                    axios.put(route('admin.users.update', self.user.id), self.user)
                        .then(function (response) {
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Пользователь успешно обновлен`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.errors= {};
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
