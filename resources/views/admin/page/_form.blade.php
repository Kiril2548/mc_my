<b-field label="Заголовок"
         :type="errors.title ? 'is-danger' : ''"
         :message="errors.title ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="page.title" maxlength="50" placeholder="О нас"></b-input>
</b-field>

<b-field label="SEO Описание"
         :type="errors.description ? 'is-danger' : ''"
         :message="errors.description ? 'Поле обязательно к заполнению' : ''">
    <b-input type="textarea" v-model="page.description" maxlength="250" placeholder="Информация о нашем сервисе"></b-input>
</b-field>

<b-field label="SEO Ключевые слова"
         :type="errors.key_words ? 'is-danger' : ''"
         :message="errors.key_words ? 'Добавьте хотябы 1 тег' : ''">
    <b-taginput
        v-model="page.key_words"
        ellipsis
        icon="label"
        placeholder="Добавте тег">
    </b-taginput>
</b-field>

<b-field label="Текст страницы"
         :type="errors.page_text ? 'is-danger' : ''"
         :message="errors.page_text ? 'Поле обязательно к заполнению' : ''">
    <b-input type="textarea" v-model="page.page_text" maxlength="50000" placeholder="Контект страницы"></b-input>

</b-field>


<div class="container-fluid" style="padding: 0;">
    <div class="row">
        <div class="col-md-12">
            <b-field
                label="Фото"
            >
                <button v-if="page.img == ''" class="button" @click="lfmm">Добавить фото</button>
            </b-field>
            <div class="container-fluid" v-if="page.img != ''" style="margin-top: 5px;">
                <div class="row">
                    <span style="">
                        <img :src="page.img"
                             style="height: 100px; width: 100px; object-fit: cover; border-radius: 10px;"
                             alt="">
                        <br>
                        <button  @click="delImgFromArray(key)" style="margin-top: 5px; margin-left: 10px;"
                                class="button is-danger is-inverted">Удалить</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
