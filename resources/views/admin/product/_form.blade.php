<b-field label="Название"
         :type="errors['product.title'] ? 'is-danger' : ''"
         :message="errors['product.title'] ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="product.title" maxlength="50" placeholder="Продукт"></b-input>
</b-field>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6" style="padding-left: 0;">
            <b-field
                label="Категория"
                :type="errors['product.category_id'] ? 'is-danger' : ''"
                :message="errors['product.category_id'] ? 'Категория должна быть выбрана' : ''">
                <b-select v-model="product.category_id" placeholder="Выберите категорию"
                          expanded>
                    <option v-for="category in categories" :value="category.id">@{{ category.title }}</option>
                </b-select>
            </b-field>
        </div>
        <div class="col-md-6" style="padding-right: 0;">
            <b-field
                label="Скидка"
                :type="errors['product.discounts'] ? 'is-danger' : ''"
                :message="errors['product.discounts'] ? 'Выбрана не существющая скидка' : ''">
                <b-select v-model="product.discount_id" placeholder="Выберите скидку"
                          expanded>
                    <option :value="null"></option>
                    <option v-for="discount in discounts" :value="discount.id">@{{ discount.value }}%</option>
                </b-select>
            </b-field>
        </div>
    </div>
</div>
<b-field label="Описание*"
         :type="errors['product.desc'] ? 'is-danger' : ''"
         :message="errors['product.desc'] ? 'Поле обязательно к заполнению' : ''">
    <b-input maxlength="5000" placeholder="Любой текст" v-model="product.desc" type="textarea"></b-input>
</b-field>
<div class="container-fluid" style="padding: 0;">
    <div class="row">
        <div class="col-md-4">
            <b-field label="Цена*"
                     :type="errors['product.price'] ? 'is-danger' : ''"
                     :message="errors['product.price'] ? 'Цена обязательно к заполнению и должна иметь вид [ 100.05 ]' : ''">
                <b-input placeholder="100.00"
                         type="text"
                         v-model="product.price">
                </b-input>
            </b-field>
        </div>
        <div class="col-md-4">
            <b-field label="Количество"
                     :type="errors['product.count'] ? 'is-danger' : ''"
                     :message="errors['product.count'] ? 'Должна иметь вид [ 25 ]' : ''">
                <b-input placeholder="100"
                         type="text"
                         v-model="product.count">
                </b-input>
            </b-field>
        </div>
        <div class="col-md-4">
            <b-field
                label="Статус"
                :type="errors['product.status'] ? 'is-danger' : ''"
                :message="errors['product.status'] ? errors['product.status'] : ''">
                <b-select v-model="product.status" placeholder="ACTIVE" expanded>
                    <option value="1">ACTIVE</option>
                    <option value="0">DELETE</option>
                </b-select>
            </b-field>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="padding-left: 0;">
            <b-field
                label="Параметры товара*">
                <b-field>
                    <b-input type="hidden" disabled placeholder="Ключ" v-model="param.key"></b-input>
                    <b-input type="text" placeholder="Значение" v-model="param.value"></b-input>
                    <p class="control">
                        <button class="button is-success" @click="addParam">Добавить</button>
                    </p>
                </b-field>
            </b-field>
        </div>
    </div>
</div>
<div class="container-fluid" v-if="params.length >= 1">
    <div class="row">
        <div class="col-md-12" v-for="(item, key) in params">
            <p><b>@{{ item.value }}</b> <a href="#" style="color: red" @click="delParam(key)">X</a></p>
        </div>
    </div>
</div>

<div class="container-fluid" style="padding: 0;">
    <div class="row">
        <div class="col-md-12">
            <b-field
                label="Фото"
                :type="errors.imgs ? 'is-danger' : ''"
                :message="errors.imgs ? 'Нужно добавить хотя бы 1 фото' : ''">
                <button class="button" @click="lfmm">Добавить фото</button>
            </b-field>
            <div class="container-fluid" style="margin-top: 5px;">
                <div class="row">
                    <span v-for="(item, key) in imgs" style="">
                        <img :src="item.url"
                             style="height: 100px; width: 100px; object-fit: cover; border-radius: 10px;"
                             alt="">
                        <br>
                        <button @click="delImgFromArray(key)" style="margin-top: 5px; margin-left: 10px;"
                                class="button is-danger is-inverted">Удалить</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


{{--************************************************--}}
<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
