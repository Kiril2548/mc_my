<b-field label="Заголовок"
         :type="errors.title ? 'is-danger' : ''"
         :message="errors.title ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="news.title" maxlength="50" placeholder="У нас новый функционал"></b-input>
</b-field>

<b-field label="Текст"
         :type="errors.text ? 'is-danger' : ''"
         :message="errors.text ? 'Поле обязательно к заполнению' : ''">
    <b-input type="textarea" v-model="news.text" maxlength="50000" placeholder="Информация о том что изменилось"></b-input>
</b-field>

<b-field label="Дана Начала">
    <b-input
        type="date"
        v-model="news.date_start"
        placeholder="Нажмите для выбота даты"
        icon="calendar-today">
    </b-input>
</b-field>

<b-field label="Дата Окончания">
    <b-input
        type="date"
        v-model="news.date_end"
        placeholder="Нажмите для выбота даты"
        icon="calendar-today">
    </b-input>
</b-field>


<div class="container-fluid" style="padding: 0;">
    <div class="row">
        <div class="col-md-12">
            <b-field
                label="Фото"
                :type="errors.img ? 'is-danger' : ''"
                :message="errors.img ? 'Фото должно быть добавлненно' : ''"
            >
                <button v-if="news.img == ''" class="button" @click="lfmm">Добавить фото</button>
            </b-field>
            <div class="container-fluid" v-if="news.img != ''" style="margin-top: 5px;">
                <div class="row">
                    <span style="">
                        <img :src="news.img"
                             style="height: 100px; width: 100px; object-fit: cover; border-radius: 10px;"
                             alt="">
                        <br>
                        <button  @click="delImgFromArray(key)" style="margin-top: 5px; margin-left: 10px;"
                                 class="button is-danger is-inverted">Удалить</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
