@extends('frontend.layouts.app')

@section('content')

    @if($page->img != "" || $page->img != null)
        <div class="container-fluid img-card">
            <div class="row">
                <div class="col-md-12 text-center"
                     style="height: 400px; padding: 75px; background-image: url({{$page->img}}); background-size: cover;">
                    <h1 class="text-center"
                        style="
                            font-size: 70px;
                            font-weight: 800;
                            background-color: rgba(21,30,18,0.7);
                            padding: 15px;
                            border-radius: 15px;
                        "
                    >{{ $page->title }}</h1>
                </div>
            </div>
        </div>
        <div class="container card card-info p-4" style="margin-top: -100px;">
            <div class="row">
                <div class="col-md-12 text-justify">
                    <p>{{ $page->page_text }}</p>
                </div>
            </div>
        </div>

    @else
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-12 text-justify">
                    <h1 class="text-center"
                        style="
                            font-size: 70px;
                            font-weight: 800;
                            padding: 15px;
                        "
                    >{{ $page->title }}</h1>

                    <p>{{ $page->page_text }}</p>
                </div>
            </div>
        </div>
    @endif

@endsection

