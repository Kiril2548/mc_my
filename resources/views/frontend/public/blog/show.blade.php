@extends('frontend.layouts.app')

@section('content')


    <div class="container mb-2 mt-2">
        <div class="row">
            <div class="col-md-9">
                <div class="card img-card">
                    <div class="img"
                         style="
                             min-height: 400px;
                             background-size: cover;
                             background-position: center;
                             background-repeat: no-repeat;
                             background-image: url({{ $article->img }});
                             "></div>
                </div>
                <div class="card card-info p-4 text-justify" style="margin-top: -150px; width: 94%; margin-left: 3%;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 text-left"><h3>{{ $article->title }}</h3></div>
                            <div class="col-md-6 text-right">
                                <span>
                                    <span class="mr-3"><i class="fas fa-eye"></i> {{ $article->views }}</span>
                                    <span class="mr-3"><i
                                            class="fas fa-comments"></i> {{ count($article->comments) }}</span>
                                    <span><i class="fas fa-clock"></i> {{ $article->created_at }}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <p>{{ $article->text }}</p>
                </div>
                <div class="card mt-3" style="width: 94%; margin-left: 3%;">
                    @auth
                        <form action="{{ route('comments.store', $article->slug) }}" method="post" class="p-3">
                            @csrf
                            <div class="input-group mb-3">
                            <textarea required name="comment" class="form-control" placeholder="Добавить комментария"
                                      rows="2"
                                      aria-label="With textarea"></textarea>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i
                                            class="fab fa-telegram-plane"></i></button>
                                </div>
                            </div>
                        </form>
                    @endif

                    @if(count($comments) >= 1)
                        <div class="container">
                            <div class="row p-3">
                                @foreach($comments as $comment)
                                    <div class="media comment-box">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="img-responsive user-photo"
                                                     src="{{ $comment->user->avatar }}">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="container-fluid media-heading">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <h5>{{ $comment->user->name }}</h5>
                                                    </div>
                                                    <div class="col-md-4 text-right">
                                                        <span><i class="fas fa-eye"></i> {{ $comment->views }}</span>
                                                        <span class="ml-2"><i
                                                                class="fas fa-thumbs-up"></i> {{ $comment->likes }}</span>
                                                        <span class="ml-2">
                                                            @if(Auth::check())

                                                                @if($comment->user_id == Auth::user()->id)
                                                                    <div class="btn-group" role="group">
                                                                    <button type="button"
                                                                            class="btn btn-outline-secondary"
                                                                            data-toggle="modal"
                                                                            data-target="#comment_{{ $comment->id }}">
                                                                        <i class="fas fa-edit"></i>
                                                                    </button>
                                                                    <button type="button"
                                                                            class="btn btn-outline-secondary"
                                                                            onclick="document.getElementById('del_comment_{{ $comment->id }}').submit()">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </button>
                                                                    <form id="del_comment_{{ $comment->id }}"
                                                                          action="{{ route('comments.delete', $comment->id) }}"
                                                                          method="post">
                                                                        @csrf
                                                                    </form>
                                                                </div>
                                                                    <!-- Modal -->
                                                                    <div class="modal fade"
                                                                         id="comment_{{ $comment->id }}"
                                                                         tabindex="-1" role="dialog"
                                                                         aria-labelledby="exampleModalCenterTitle"
                                                                         aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-centered"
                                                                         role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"
                                                                                    id="exampleModalCenterTitle">Редактирование комментария</h5>
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span
                                                                                        aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form
                                                                                    action="{{ route('comments.update', $comment->id) }}"
                                                                                    method="post" class="p-3">
                                                                                    @csrf
                                                                                    <div class="input-group mb-3">
                                                                                    <textarea name="comment"
                                                                                              class="form-control"
                                                                                              placeholder="Текст комментария"
                                                                                              rows="10"
                                                                                              required
                                                                                              aria-label="With textarea">
                                                                                        {{ $comment->comment }}
                                                                                    </textarea>
                                                                                        <div class="input-group-append">
                                                                                            <button
                                                                                                class="btn btn-outline-secondary"
                                                                                                type="submit"
                                                                                                id="button-addon2"><i
                                                                                                    class="fab fa-telegram-plane"></i></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                  </div>
                                                                </div>

                                                                @else
                                                                    <button type="submit"
                                                                            onclick="document.getElementById('like_comment_{{ $comment->id }}').submit()"
                                                                            class="btn btn-sm btn-outline-secondary">
                                                                        <i class="fas fa-thumbs-up"></i>
                                                                    </button>
                                                                    <form id="like_comment_{{ $comment->id }}"
                                                                          action="{{ route('comments.like', $comment->id) }}"
                                                                          method="post">
                                                                        @csrf
                                                                </form>
                                                                @endif
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <p>
                                                {{ $comment->comment }}
                                                <br>
                                                <br>
                                                <sup class="fa-pull-right"><i
                                                        class="fas fa-clock"></i> {{ $comment->created_at }}</sup>
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <ul class="list-group">
                    @foreach($articles as $key => $item)
                        <li class="list-group-item">
                            <a href="{{ route('articles.show', $item->slug) }}" style="
                                display: block;
                                text-align: right;
                                height: 100%;
                                color: #666;
                                text-decoration: none;
                                padding: 10px;
                            ">{{ $item->title }}</a>
                        </li>

                        @if($key == 10)
                            @break
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

@endsection
