@extends('frontend.layouts.app')

@section('content')

    <div class="container mt-3">
        <div class="row">
            @foreach($articles as $article)
                <div class="col-md-12 mb-4">
                    <div class="card">
                        <div class="img"
                             style="min-height: 500px; background-size: cover; background-image: url({{ $article->img }})">
                            <div style="
                                position: absolute;
                                bottom: 0;
                                width: 100%;
                                background-color: rgba(21,30,18,0.9);
                                min-height: 150px;
                                padding: 15px;

                            ">
                                <h3 class="font-weight-bold"><a
                                        href="{{ route('articles.show', $article->slug) }}">{{ $article->title }}</a>
                                </h3>
                                <p>{!! substr($article->text, 0, 500) !!}...<a
                                        style="font-weight: 600; text-decoration: none;"
                                        href="{{ route('articles.show', $article->slug) }}">Перейти <i
                                            class="fas fa-arrow-circle-right"></i></a></p>
                                <div class="d-flex justify-content-between pl-3 pr-3">
                                    <span>
                                        <i class="fas fa-eye"></i> {{ $article->views }}
                                        <i class="ml-3 fas fa-comments"></i> {{ count($article->comments) }}
                                    </span>
                                    <span><i class="fas fa-clock"></i> {{ $article->created_at }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="w-100 d-flex justify-content-center">
                {{ $articles->links() }}
            </div>
        </div>
    </div>

@endsection

