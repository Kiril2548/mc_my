@extends('frontend.layouts.app')

@section('content')

    <div class="container mt-3">
        <div class="row">
            @foreach($news as $new)
                <div class="col-md-12 mb-4">
                    <div class="card">
                        <div class="img p-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-3" style="
                                        background-image: url({{ $new->img }});
                                        background-size: cover;
                                        background-position: center;
                                        "></div>
                                    <div class="col-md-9">
                                        <h3 class="font-weight-bold">{{ $new->title }}</h3>
                                        <p>{!! substr($new->text, 0, 500) !!}</p>
                                        <div class="d-flex justify-content-end pl-3 pr-3">
                                            <span><i class="fas fa-clock"></i> {{ $new->created_at }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="w-100 d-flex justify-content-center">
                {{ $news->links() }}
            </div>
        </div>
    </div>

@endsection

