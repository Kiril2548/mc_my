@extends('frontend.layouts.app')

@section('content')


    <div class="container mb-2 mt-2">
        <div class="row">
            <div class="col-md-9">
                <div class="card img-card">
                    <div class="img"
                         style="
                             min-height: 400px;
                             background-size: cover;
                             background-position: center;
                             background-repeat: no-repeat;
                             background-image: url({{ $news->img }});
                             "></div>
                </div>
                <div class="card card-info p-4 text-justify" style="margin-top: -150px; width: 94%; margin-left: 3%;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 text-left"><h3>{{ $news->title }}</h3></div>
                            <div class="col-md-6 text-right">
                                <span>
                                    <span class="mr-3"><i class="fas fa-eye"></i> {{ $news->views }}</span>
                                    <span><i class="fas fa-clock"></i> {{ $news->created_at }}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <p>{{ $news->text }}</p>
                </div>
            </div>
            <div class="col-md-3">
                <ul class="list-group">
                    @foreach($news_all as $key => $item)
                        <li class="list-group-item">
                            <a href="{{ route('news.show', $item->slug) }}" style="
                                display: block;
                                text-align: right;
                                height: 100%;
                                color: #666;
                                text-decoration: none;
                                padding: 10px;
                            ">{{ $item->title }}</a>
                        </li>

                        @if($key == 10)
                            @break
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

@endsection
