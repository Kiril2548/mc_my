<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/ico" href="{{ asset('favicon.ico') }}"/>

    <title>{{ config('app.name', 'MineStartProject') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="{{ asset("theme/css/nucleo-icons.css") }}" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link type="text/css" href="{{ asset('theme/css/blk-design-system.min.css') }}" rel="stylesheet">
    <link href="{{ asset("theme/demo/demo.css") }}" rel="stylesheet"/>

    @routes

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    {{--<script>--}}
        {{--jQuery(document).ready(function($) {--}}
            {{--$(window).load(function() {--}}
                {{--setTimeout(function() {--}}
                    {{--$('#circle').fadeOut('slow', function() {});--}}
                {{--}, 500);--}}

                {{--setTimeout(function() {--}}
                    {{--$('#circle-div').fadeOut('slow', function() {});--}}
                {{--}, 700);--}}

            {{--});--}}
        {{--});--}}
    {{--</script>--}}

</head>
<body>

{{--<div id="circle-div">--}}
    {{--<div id="circle">--}}
        {{--<div class="loader">--}}
            {{--<div class="loader">--}}
                {{--<div class="loader">--}}
                    {{--<div class="loader">--}}
                        {{--<div class="loader">--}}
                            {{--<div class="loader">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<style>
    ::-webkit-scrollbar{
        width:6px;
    }
    ::-webkit-scrollbar-thumb{
        border-width:1px 1px 1px 2px;
        border-color: #777;
        background-color: #813b6a;
    }
    ::-webkit-scrollbar-thumb:hover{
        border-width: 1px 1px 1px 2px;
        border-color: #555;
        background-color: #b43f9d;
    }
    ::-webkit-scrollbar-track{
        border-width:0;
    }
    ::-webkit-scrollbar-track:hover{
        border-left: solid 1px #1c1e66;
        background-color: #1c1e66;
    }
</style>
@stack('style')

@include('frontend.partials._style')



<div id="app">
    @include('frontend.partials._nav')

    <main class="app">
        @include('frontend.partials._modelBuyCoin')
        @yield('content')
    </main>

    @include('frontend.partials._footer')
</div>
@include('frontend.partials._up')


<!-- AXIOS -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<!-- Core -->
<script src="{{ asset("theme/js/core/jquery.min.js") }}" type="text/javascript"></script>
<script src="{{ asset("theme/js/core/popper.min.js") }}" type="text/javascript"></script>
<script src="{{ asset("theme/js/core/bootstrap.min.js") }}" type="text/javascript"></script>


<!-- Theme JS -->
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{ asset("theme/js/plugins/bootstrap-switch.js") }}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ asset("theme/js/plugins/nouislider.min.js") }}" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{ asset("theme/js/plugins/moment.min.js") }}"></script>
<script src="{{ asset("theme/js/plugins/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script src="{{ asset("theme/demo/demo.js") }}"></script>
<script src="{{ asset("theme/js/blk-design-system.min.js?v=1.0.0") }}" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script>
    axios.defaults.headers.common['X-CSRF-TOKEN'] = @json(csrf_token());

    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 250) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {

            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
</script>
@stack('scripts')

</body>
</html>
