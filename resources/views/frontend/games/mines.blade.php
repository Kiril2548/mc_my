@extends('frontend.layouts.app')
<style>
    @keyframes scale_ {
        0% {
            transform: scale(1);
        }
        50% {
            transform: scale(1.1);
        }
        100% {
            transform: scale(1);
        }
    }

    @keyframes scale_mine {
        0% {
            transform: scale(1);
            box-shadow: 0px 0px 20px rgba(255,255,255,0.3);
        }
        50% {
            transform: scale(1.1);
            box-shadow: 0px 0px 20px rgba(255,255,255,0.3);
        }
        100% {
            transform: scale(1);
            box-shadow: 0px 0px 20px rgba(255,255,255,0.3);
        }
    }

    @keyframes swing {
        15% {
            -webkit-transform: translateX(1px);
            transform: translateX(1px);
        }
        30% {
            -webkit-transform: translateX(-1px);
            transform: translateX(-1px);
        }
        50% {
            -webkit-transform: translateX(0.5px);
            transform: translateX(0.5px);
        }
        65% {
            -webkit-transform: translateX(-0.5px);
            transform: translateX(-0.5px);
        }
        80% {
            -webkit-transform: translateX(0.3px);
            transform: translateX(0.3px);
        }
        100% {
            -webkit-transform: translateX(0);
            transform: translateX(0);
        }
    }

    * {
        transition: all 0.3s ease;
        user-select: none;
    }

    .field {
        min-width: 265px;
        max-width: 265px;
    }

    .row_cells {
        margin-bottom: 2px;
    }

    .cell_item_active {
        width: 50px;
        height: 50px;
        background-color: rgba(255, 255, 255, 0.2)!important;
    }

    .cell_item {
        width: 50px;
        height: 50px;
        background-color: rgba(255, 255, 255, 0.1);
    }

    .cell_item_active:hover {
        cursor: pointer;
        animation: swing 1s ease;
        animation-iteration-count: 1;
        background-color: rgba(255, 255, 255, 0.4);
    }

    .scale_ {
        animation: scale_ 0.5s ease;
        animation-iteration-count: 1;
    }

    .true {
        background-color: rgba(255, 255, 255, 0.4);
        background-image: url("{{ asset('games_img/true.svg') }}");
        background-size: cover;
        animation: scale_mine 0.3s ease;
        animation-iteration-count: 1;
    }

    .false {
        background-color: rgba(255, 255, 255, 0.4);
        background-image: url("{{ asset('games_img/false.svg') }}");
        background-size: cover;
        animation: scale_mine 0.3s ease;
        animation-iteration-count: 1;
    }

    #game_num, #user_balance {
        font-weight: 800;
    }

    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 8px 7px !important;
    }

    @media only screen and (max-width: 767px) {
        #history_of_games{
            display: none!important;
        }
        #_main_cell{
            padding: 0 !important;
        }
    }
</style>

@push('style')
@endpush

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md text-center">
                <h1>{{ config('app.name', 'MineStartProject') }}</h1>
                <h4>Игра # <span id="game_num"></span></h4>
                <h4>Баланс: <span id="user_balance">@json(Auth::user()->coins)</span> coins</h4>
            </div>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4 d-flex justify-content-center">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>MINES:</h4>
                                </div>
                                <div class="col-md-9">
                                    <div class="text-center">
                                        <b id="mines_count">5</b>
                                    </div>
                                    <div id="sliderRegular" class="slider"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-4 d-flex justify-content-center">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>BET:</h4>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control form-control-sm" value="1" id="bet_amount">
                                    <button class="btn btn-sm btn-outline-dark" onclick="bet_change_to('1/2')">1/2</button>
                                    <button class="btn btn-sm btn-outline-dark" onclick="bet_change_to('x2')">x2</button>
                                    <button class="btn btn-sm btn-outline-dark" onclick="bet_change_to('max')">MAX</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-danger d-none text-center error_text" role="alert"></div>
                        <div class="container-fluid d-flex justify-content-center" id="_main_cell" style="padding-left: 25%;">
                            <div class="row field">
                                <div class="container-fluid">
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(1, '#selectEase_1')" id="selectEase_1"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(2, '#selectEase_2')" id="selectEase_2"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(3, '#selectEase_3')" id="selectEase_3"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(4, '#selectEase_4')" id="selectEase_4"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(5, '#selectEase_5')" id="selectEase_5"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(6, '#selectEase_6')" id="selectEase_6"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(7, '#selectEase_7')" id="selectEase_7"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(8, '#selectEase_8')" id="selectEase_8"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(9, '#selectEase_9')" id="selectEase_9"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(10, '#selectEase_10')" id="selectEase_10"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(11, '#selectEase_11')" id="selectEase_11"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(12, '#selectEase_12')" id="selectEase_12"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(13, '#selectEase_13')" id="selectEase_13"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(14, '#selectEase_14')" id="selectEase_14"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(15, '#selectEase_15')" id="selectEase_15"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(16, '#selectEase_16')" id="selectEase_16"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(17, '#selectEase_17')" id="selectEase_17"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(18, '#selectEase_18')" id="selectEase_18"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(19, '#selectEase_19')" id="selectEase_19"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(20, '#selectEase_20')" id="selectEase_20"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(21, '#selectEase_21')" id="selectEase_21"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(22, '#selectEase_22')" id="selectEase_22"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(23, '#selectEase_23')" id="selectEase_23"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(24, '#selectEase_24')" id="selectEase_24"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(25, '#selectEase_25')" id="selectEase_25"
                                             class="cell_item"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="history_of_games" style="margin-left: 25px; width: 250px;">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead>
                                        <th>Игра</th>
                                        <th class="text-right">Результат</th>
                                        </thead>
                                        <tbody id="last_games">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid" style="width: 440px;">
                            <div class="row justify-content-between">
                                <div class="col-md-12 d-flex justify-content-center">
                                    Next Step Get: <b id="next_range" class="ml-2 mr-2">0</b>coins
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 d-flex justify-content-center bnt_place">
                                    <button type="button" class="btn btn-primary add_coin_btn d-none"
                                            data-toggle="modal" data-target=".buyCoins">GET COINS
                                    </button>
                                    <button class="btn btn-warning" id="start_btn">START</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var slider = document.getElementById('sliderRegular');
        var sliderAmount = document.getElementById('sliderRegularAmount');
        var game = 0;
        var mines = 0;
        var bet = 0;
        var games = @json($games);
        var btn = "START";

        var balance = @json(Auth::user()->coins);

        var tbody = "";
        $(document).ready(function () {

            noUiSlider.create(slider, {
                start: 5,
                connect: [true, false],
                range: {
                    min: 1,
                    max: 24
                }
            });

            checkBalance(balance);
            $('#game_num').text(game);
            $('#start_btn').click(function () {

                if (btn == "GET") {
                    axios.post(route('games.mines.get', {
                        game: game
                    }))
                        .then(function (response) {

                            balance = response.data.user_balance;
                            checkBalance(balance);
                            $("#user_balance").text(balance);
                            games = response.data.games;
                            printTableLastGames(games);
                            game = 0;

                            $('.cell_item').removeClass('cell_item_active');
                            toogleText($(this).text(), 0);

                        })
                        .catch(function (error) {

                        });
                } else {
                    if ($(this).hasClass('btn-warning')) {
                        $('.field').toggleClass('scale_');
                        setTimeout(function () {
                            $('.field').toggleClass('scale_');
                        }, 500)
                    }

                    $('.cell_item').toggleClass('cell_item_active');
                    toogleText($(this).text(), 0);

                    $('.false').removeClass('false');
                    $('.true').removeClass('true');
                }
            });

            setInterval(function () {
                checkMinesValue();
                checkBetValue();
                bet_change_to();
            }, 100);

        });

        function selectEase(key, id) {
            if ($(id).hasClass('cell_item_active')) {
                axios.post(route('games.mines.check', {
                    game: game,
                    key: key,
                    mines: mines,
                    bet: bet
                }))
                    .then(function (response) {
                        if (response.data.cell_item == 0) {
                            games = response.data.games;
                            printTableLastGames(games);
                            game = 0;

                            $('.true').toggleClass('cell_item_active');
                            $('.cell_item').removeClass('true');
                            for (var i = 0; i <= (response.data.rows * response.data.rows) - 1; i++) {
                                if (response.data.combination[i] == 1) {
                                    $('#selectEase_' + (i + 1)).addClass('true');
                                } else {
                                    $('#selectEase_' + (i + 1)).addClass('false');
                                }
                            }

                            $('.cell_item').toggleClass('cell_item_active');
                            toogleText($(this).text(), 0);

                            balance = response.data.user_balance;
                            checkBalance(balance);
                            $('#game_num').text(game);
                            $('#next_range').text(response.data.range_next);
                        } else {
                            balance = response.data.user_balance;
                            $(id).removeClass('cell_item_active').addClass('true');
                            game = response.data.game;
                            toogleText($(this).text(), response.data.step, response.data.range);
                            $('#game_num').text(game);
                            $('#next_range').text(response.data.range_next);
                        }
                        $("#user_balance").text(balance);

                    })
                    .catch(function (error) {
                        var error_l = error.response.data.error ? error.response.data.error: "Перезапустите страницу";
                        alert(error_l);
                    });
            }
        }

        function toogleText(text, step, amount = 0) {
            if (step == 0) {
                if (btn == "START") {
                    $('#start_btn').html('STOP');
                    $('#start_btn').addClass('btn-danger').removeClass('btn-warning').removeClass('btn-info');
                    btn = "STOP";
                } else if (btn == "STOP" || btn == "GET") {
                    $('#start_btn').html('START');
                    $('#start_btn').addClass('btn-warning').removeClass('btn-danger').removeClass('btn-info');
                    btn = "START";
                }
            } else {
                $('#start_btn').html('GET: <b> ' + amount + '</b> coins');
                $('#start_btn').addClass('btn-info').removeClass('btn-warning').removeClass('btn-danger');
                btn = "GET";
            }
        }

        function checkBalance(balance) {
            if (balance <= 0) {
                if (($('.add_coin_btn').hasClass('d-none'))) {
                    $('.add_coin_btn').removeClass('d-none');
                }
            } else {
                if (!($('.add_coin_btn').hasClass('d-none'))) {
                    $('.add_coin_btn').toggleClass('d-none');
                }
            }
        }

        function printTableLastGames(games) {
            $('#last_games').fadeToggle(300);
            tbody = "";
            for (var i = 0; i < games.length; i++) {
                if (games[i].win_cash >= 0) {
                    tbody += '<tr>' +
                        '<td># <b>' + games[i].id + '</b></td>' +
                        '<td class="text-right" style="color: #23c123 !important;"><b>' + games[i].win_cash + '</b> coins</td>' +
                        '</tr>';
                } else {
                    tbody += '<tr>' +
                        '<td># <b>' + games[i].id + '</b></td>' +
                        '<td class="text-right" style="color: #8b3537 !important;"><b>' + games[i].win_cash + '</b> coins</td>' +
                        '</tr>';
                }
            }


            $('#last_games').html(tbody);
            $('#last_games').fadeToggle(300);
        }

        function checkMinesValue() {
            mines = Math.round($('#sliderRegular>.noUi-base>.noUi-origin>.noUi-handle').attr('aria-valuetext'));
            $('#mines_count').text(mines);
        }

        function checkBetValue() {
            bet = Math.round($('#bet_amount').val());
        }

        function bet_change_to(val) {
            if(val == '1/2') {
                bet = bet/2
            } else if (val == 'x2') {
                bet = bet*2
            } else if (val == 'max') {
                bet = balance;
            } else {
                $('#bet_amount').val(bet)
            }
            if(bet < 1){
                bet = 1;
            } else if (bet > balance) {
                if(balance >= 1)
                    bet = balance;
            }
            Math.round($('#bet_amount').val(bet));
        }
        
        function getCorectUserBalance() {
            
        }
    </script>

    @if($games)
        <script>
            $(document).ready(function () {
                printTableLastGames(games);
            })
        </script>
    @endif
@endpush

