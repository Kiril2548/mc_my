@extends('frontend.layouts.app')

@push('style')
    <style>
        @keyframes scale_mine {
            0% {
                transform: rotateY(180deg);
                box-shadow: 0px 0px 20px rgba(255, 255, 255, 0.3);
            }
            50% {
                transform: rotateY(90deg);
                box-shadow: 0px 0px 20px rgba(255, 255, 255, 0.3);
            }
            100% {
                transform: rotateY(0deg);
                box-shadow: 0px 0px 20px rgba(255, 255, 255, 0.3);
            }
        }

        * {
            transition: all 0.1s ease;
            user-select: none;
        }

        .field {
            min-width: 265px;
            max-width: 265px;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 8px 7px !important;
        }

        .scale_img {
            animation: scale_mine 0.3s ease;
            animation-iteration-count: 1;
        }

        .scale_img:hover {
            transform: scale(1.7);
            z-index: 1000;
        }

        .none_view {
            display: none !important;
        }

        @media only screen and (max-width: 767px) {
            #history_of_games {
                display: none !important;
            }

            #_main_cell {
                padding: 0 !important;
            }
        }
    </style>
@endpush

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md text-center">
                <h1>{{ config('app.name', 'MineStartProject') }}</h1>
                <h4>Игра # <span id="game_num"></span></h4>
                <h4>Баланс: <b id="user_balance">@json(Auth::user()->coins)</b> COINS</h4>
                <h4>Ставка: <b id="bet_view">1</b> coin</h4>
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4 d-flex justify-content-center">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>BET:</h4>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control form-control-sm" value="1" id="bet_amount">
                                    <button class="btn btn-sm btn-outline-dark" onclick="bet_change_to('1/2')">1/2
                                    </button>
                                    <button class="btn btn-sm btn-outline-dark" onclick="bet_change_to('x2')">x2
                                    </button>
                                    <button class="btn btn-sm btn-outline-dark" onclick="bet_change_to('max')">MAX
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                        <div class="container field_bj none_view">
                            <div class="container d-flex justify-content-center">
                                <div class="row ">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <h4>TOTAL DEALER: <b id="dealer_value">0</b></h4>
                                            <div class="col-md-12 d-flex justify-content-around mb-2" id="deckOfDealer">

                                            </div>
                                            <div class="col-md-12 d-flex justify-content-center" style="min-height: 70px;">
                                                <div class="alert alert-success visible" role="alert">
                                                    YOU WON
                                                </div>
                                                <div class="alert alert-danger d-none" role="alert">
                                                    YOU LOSE
                                                </div>
                                            </div>
                                            <div class="col-md-12 d-flex justify-content-around mt-2" id="deckOfUser">

                                            </div>
                                            <h4>TOTAL USER: <b id="user_value">0</b></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container d-flex field_bj_btn justify-content-center mt-4">
                                <div class="row">
                                    <button class="btn btn-success" id="getCard" onclick="getCard()">MORE</button>
                                    <button class="btn btn-danger" id="stopGame" onclick="stop()">STOP</button>
                                </div>
                            </div>
                        </div>
                        <div class="container stop_game text-center">
                            <button class="btn btn-warning" id="startGame" onclick="startGame()">РАЗДАТЬ КАРТЫ</button>
                        </div>
                    </div>
                    <div class="col-md-3 d-md-block d-sm-none">
                        <table class="table">
                            <thead>
                            <th>Игра</th>
                            <th class="text-right">Результат</th>
                            </thead>
                            <tbody id="last_games">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var
            game = 0,
            games = @json($games),
            userCards = [],
            userCardsValue = 0,
            dealerCards = [],
            dealerCardsValue = 0,
            bet = 0,
            balance = @json(Auth::user()->coins)
        ;

        $(document).ready(function () {
            printTableLastGames(games);
            setInterval(function () {
                checkBetValue();
                bet_change_to();
                userB();
            }, 100);
        });

        function printUserCards() {
            $('#user_value').text(userCardsValue);
            $cards = "";
            for (var i in userCards) {
                for (var j in userCards[i]) {
                    $cards += "<div class='scale_img'><img src='/games_img/cards/" + j + ".png' style='width: 80px; height: 120px;'></div>";
                }
            }
            $('#deckOfUser').html($cards);
        }

        function printDealerCards() {
            $('#user_value').text(userCardsValue);
            $cards = "";
            if (dealerCards.length == 1) {
                for (var j in dealerCards[0]) {
                    $('#dealer_value').text(dealerCards[0][j]);
                    $cards += "<div class='scale_img'><img src='/games_img/cards/" + j + ".png' style='width: 80px; height: 120px;'></div>";
                    $cards += "<div class='scale_img'><img src='/games_img/cards/Z.png' style='width: 80px; height: 120px;'></div>";
                }
            } else {
                $('#dealer_value').text(dealerCardsValue);
                for (var i in dealerCards) {
                    for (var j in dealerCards[i]) {
                        $cards += "<div class='scale_img'><img src='/games_img/cards/" + j + ".png' style='width: 80px; height: 120px;'></div>";
                    }
                }
            }

            $('#deckOfDealer').html($cards);
        }

        function startGame() {
            $('.alert').addClass('d-none');
            axios.post(route('games.blackjack.start', {
                game: game,
                bet: bet
            }))
                .then(function (response) {
                    console.log(response);
                    game = response.data.game;
                    userCards = response.data.user_card;
                    userCardsValue = response.data.user_card_value;
                    dealerCards = response.data.dealer_card;
                    balance = response.data.user_balance;

                    printUserCards();
                    printDealerCards();
                    $("#game_num").text(game);
                    $(".field_bj").removeClass('none_view');
                    $(".field_bj_btn").removeClass('none_view');
                    $(".stop_game").addClass('none_view');
                })
                .catch(function (error) {
                    var error_l = error.response.data.error ? error.response.data.error : "Перезапустите страницу";
                    alert(error_l);
                });
        }

        function getCard() {
            axios.post(route('games.blackjack.get_card', {
                game: game
            }))
                .then(function (response) {
                    if (response.data.status == 'lose') {
                        $('.alert-danger').removeClass('d-none');
                        userCards = response.data.user_card;
                        userCardsValue = response.data.user_card_value;
                        game = 0;
                        balance = response.data.user_balance;
                        games = response.data.games;

                        printTableLastGames(games);
                        $(".field_bj_btn").addClass('none_view');
                        $(".stop_game").removeClass('none_view');

                        printUserCards();
                    } else {
                        userCards = response.data.user_card;
                        userCardsValue = response.data.user_card_value;

                        printUserCards();
                    }
                })
                .catch(function (error) {
                    var error_l = error.response.data.error ? error.response.data.error : "Перезапустите страницу";
                    alert(error_l);
                });
        }

        function stop() {
            axios.post(route('games.blackjack.stop', {
                game: game
            }))
                .then(function (response) {
                    game = response.data.game;
                    userCards = response.data.user_card;
                    userCardsValue = response.data.user_card_value;
                    dealerCardsValue = response.data.dealer_value;
                    dealerCards = response.data.dealer_card;
                    balance = response.data.balance_user;
                    games = response.data.games;
                    if (response.data.status == 1){
                        $('.alert-success').removeClass('d-none');
                    } else {
                        $('.alert-danger').removeClass('d-none');
                    }
                    printTableLastGames(games);
                    printUserCards();
                    printDealerCards();
                    $("#game_num").text(game);
                    $(".field_bj_btn").addClass('none_view');
                    $(".stop_game").removeClass('none_view');
                })
                .catch(function (error) {
                    var error_l = error.response.data.error ? error.response.data.error : "Перезапустите страницу";
                    alert(error_l);
                });
        }

        function checkBetValue() {
            bet = $('#bet_amount').val();
            $('#bet_view').text(bet);
        }

        function userB() {
            $('#user_balance').text(balance);
        }

        function bet_change_to(val) {
            if (val == '1/2') {
                bet = bet / 2
            } else if (val == 'x2') {
                bet = bet * 2
            } else if (val == 'max') {
                bet = balance;
            } else {
                $('#bet_amount').val(bet)
            }
            if (bet < 1) {
                bet = 1;
            } else if (bet > balance) {
                if (balance >= 1)
                    bet = balance;
            }
            $('#bet_amount').val(bet);
        }

        function printTableLastGames(games) {
            $('#last_games').fadeToggle(300);
            tbody = "";
            for (var i = 0; i < games.length; i++) {
                if (games[i].win_cash >= 0) {
                    tbody += '<tr>' +
                        '<td># <b>' + games[i].id + '</b></td>' +
                        '<td class="text-right" style="color: #23c123 !important;"><b>' + games[i].win_cash + '</b> coins</td>' +
                        '</tr>';
                } else {
                    tbody += '<tr>' +
                        '<td># <b>' + games[i].id + '</b></td>' +
                        '<td class="text-right" style="color: #8b3537 !important;"><b>' + games[i].win_cash + '</b> coins</td>' +
                        '</tr>';
                }
            }


            $('#last_games').html(tbody);
            $('#last_games').fadeToggle(300);
        }
    </script>
@endpush

