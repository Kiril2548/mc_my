@extends('frontend.layouts.app')
<style>
    @keyframes scale_ {
        0% {
            transform: scale(1);
        }
        50% {
            transform: scale(1.1);
        }
        100% {
            transform: scale(1);
        }
    }

    @keyframes scale_mine {
        0% {
            transform: scale(1);
            box-shadow: 0px 0px 20px rgba(255,255,255,0.3);
        }
        50% {
            transform: scale(1.1);
            box-shadow: 0px 0px 20px rgba(255,255,255,0.3);
        }
        100% {
            transform: scale(1);
            box-shadow: 0px 0px 20px rgba(255,255,255,0.3);
        }
    }

    @keyframes swing {
        15% {
            -webkit-transform: translateX(1px);
            transform: translateX(1px);
        }
        30% {
            -webkit-transform: translateX(-1px);
            transform: translateX(-1px);
        }
        50% {
            -webkit-transform: translateX(0.5px);
            transform: translateX(0.5px);
        }
        65% {
            -webkit-transform: translateX(-0.5px);
            transform: translateX(-0.5px);
        }
        80% {
            -webkit-transform: translateX(0.3px);
            transform: translateX(0.3px);
        }
        100% {
            -webkit-transform: translateX(0);
            transform: translateX(0);
        }
    }

    * {
        transition: all 0.3s ease;
        user-select: none;
    }

    .field {
        min-width: 265px;
        max-width: 265px;
    }

    .row_cells {
        margin-bottom: 2px;
    }

    .cell_item_active {
        width: 86px;
        height: 50px;
        background-color: rgba(255, 255, 255, 0.2)!important;
    }

    .cell_item {
        width: 86px;
        height: 50px;
        background-color: rgba(255, 255, 255, 0.1);
    }

    .cell_item_active:hover {
        cursor: pointer;
        animation: swing 1s ease;
        animation-iteration-count: 1;
        background-color: rgba(255, 255, 255, 0.4);
    }

    .scale_ {
        animation: scale_ 0.5s ease;
        animation-iteration-count: 1;
    }

    .true {
        background-color: rgba(255, 255, 255, 0.4);
        background-image: url("{{ asset('games_img/true.svg') }}");
        background-size: cover;
        animation: scale_mine 0.3s ease;
        animation-iteration-count: 1;
    }

    .false {
        background-color: rgba(255, 255, 255, 0.4);
        background-image: url("{{ asset('games_img/false.svg') }}");
        background-size: cover;
        animation: scale_mine 0.3s ease;
        animation-iteration-count: 1;
    }

    #game_num, #user_balance {
        font-weight: 800;
    }

    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 8px 7px !important;
    }
</style>

@push('style')
@endpush

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md text-center">
                <h1>{{ config('app.name', 'MineStartProject') }}</h1>
                <h4>Игра # <span id="game_num"></span></h4>
                <h4>Баланс: <span id="user_balance">@json(Auth::user()->coins)</span> coins</h4>
            </div>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4 d-flex justify-content-center">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Число открытий:</h4>
                                </div>
                                <div class="col-md-9">
                                    <div class="text-center">
                                        <b class="stone_count">0</b>
                                    </div>
                                    <div id="sliderRegular" class="slider"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Выигрыш:</h4>
                                </div>
                                <div class="col-md-9">
                                    <div class="text-center">
                                        <b class="win_cash">0</b>
                                    </div>
                                    <div id="sliderRegular" class="slider"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-4 d-flex justify-content-center">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>BET:</h4>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control form-control-sm" value="1" id="bet_amount" onchange="setChange()">
                                    <button class="btn btn-sm btn-outline-dark" onclick="betPlus()">1/2</button>
                                    <button class="btn btn-sm btn-outline-dark" onclick="betMinus()">x2</button>
                                    <button class="btn btn-sm btn-outline-dark" onclick="betMax()">MAX</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-danger d-none text-center error_text" role="alert"></div>
                        <div class="container-fluid d-flex justify-content-center" style="padding-left: 25%;">
                            <div class="row field">
                                <div class="container-fluid">
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(0, 0)" id="i00"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(0, 1)" id="i01"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(0, 2)" id="i02"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(1, 0)" id="i10"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(1, 1)" id="i11"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(1, 2)" id="i12"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(2, 0)" id="i20"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(2, 1)" id="i21"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(2, 2)" id="i22"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(3, 0)" id="i30"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(3, 1)" id="i31"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(3, 2)" id="i32"
                                             class="cell_item"></div>
                                    </div>
                                    <div class="row row_cells justify-content-between">
                                        <div onclick="selectEase(4, 0)" id="i40"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(4, 1)" id="i41"
                                             class="cell_item"></div>
                                        <div onclick="selectEase(4, 2)" id="i42"
                                             class="cell_item"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 25px; width: 250px;">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead>
                                        <th>Игра</th>
                                        <th class="text-right">Результат</th>
                                        </thead>
                                        <tbody id="last_games">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid" style="width: 440px;">
                            <div class="row justify-content-between">
                                <div class="col-md-12 d-flex justify-content-center">
                                    Next Step Get: <b id="next_range" class="ml-2 mr-2">0</b>coins
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 d-flex justify-content-center bnt_place">
                                    <button type="button" class="btn btn-primary add_coin_btn d-none"
                                            data-toggle="modal" data-target=".buyCoins">GET COINS
                                    </button>
                                    <button class="btn btn-warning" id="start_btn">START</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var pharaoh = {};
        pharaoh.minBet = <?= \App\Models\Games\Stones\StonePharaoh::MIN_BET ?>;
        pharaoh.bet     = <?= \App\Models\Games\Stones\StonePharaoh::MIN_BET ?>;
        pharaoh.user_coins     = <?= Auth::user()->coins ?>;

        var btn = "START";
        function selectEase(row, column) {
            var item = '#'+'i'+row+''+column+'';

            if($(item).hasClass('cell_item_active')){
                axios.post(route('games.stones.pharaoh.game'), {
                    id: pharaoh.id,
                    cellRow: column,
                    cellColumn: row
                })
                    .then(function (response) {
                        $(item).removeClass('cell_item_active');
                        $(item).text(response.data.play.prize.toFixed(2));
                        $('#user_balance').text(response.data.user_cash.toFixed(2));
                        pharaoh.user_coins = response.data.user_cash;
                        if(response.data.end){
                            setTimeout(function () {
                                // alert('Игра окончена Вы выграли '+response.data.win_cash+', перезагрузи страницу плиз');
                                // location.reload();
                                $('.cell_item').removeClass('cell_item_active');
                                $('.cell_item').text('?');
                                $('.win_cash').text(response.data.win_cash.toFixed(2));
                                toogleText('STOP', 0);
                                $('#start_btn').removeAttr('disabled');
                            }, 200);
                        }
                    })
                    .catch(function (error) {
                        var error_l = error.response.data.error ? error.response.data.error: "Перезапустите страницу";
                        alert(error_l);
                    });
            }
        }

        function toogleText(text, step, amount = 0) {
            if (step == 0) {
                if (btn == "START") {
                    $('#start_btn').html('STOP');
                    $('#start_btn').addClass('btn-danger').removeClass('btn-warning').removeClass('btn-info');
                    btn = "STOP";
                } else if (btn == "STOP" || btn == "GET") {
                    $('#start_btn').html('START');
                    $('#start_btn').addClass('btn-warning').removeClass('btn-danger').removeClass('btn-info');
                    btn = "START";
                }
            } else {
                $('#start_btn').html('GET: <b> ' + amount + '</b> coins');
                $('#start_btn').addClass('btn-info').removeClass('btn-warning').removeClass('btn-danger');
                btn = "GET";
            }
        }

        $('#start_btn').click(function () {
            if((pharaoh.user_coins - pharaoh.bet) < 0){
                alert('Не достаточно денег');
                return;
            }
            axios.post(route('games.stones.pharaoh.game'),  {
                bet: pharaoh.bet,
            })
                .then(function (response) {
                    if(response.data.id == null){
                        alert('Не хватает денег');
                        return;
                    }
                    pharaoh.user_coins -= pharaoh.bet;
                    $('#user_balance').text(pharaoh.user_coins.toFixed(2));
                    pharaoh.id = response.data.id;
                    $('.stone_count').text(response.data.stoneValue);
                    $('.cell_item').addClass('cell_item_active');
                    toogleText($(this).text(), 0);
                    $('#start_btn').attr('disabled','disabled');
                });
        });

        function setChange() {
            pharaoh.bet = parseInt($('#bet_amount').val());
        }

        function betPlus() {
            if((pharaoh.bet/2) >= 1){
                pharaoh.bet /= 2;
                $('#bet_amount').val(pharaoh.bet);
            }
        }

        function betMinus() {
            pharaoh.bet *= 2;
            $('#bet_amount').val(pharaoh.bet);
            if(pharaoh.bet > pharaoh.user_coins){
                pharaoh.bet = pharaoh.user_coins;
                $('#bet_amount').val(pharaoh.bet);
            }
        }

        function betMax() {
            pharaoh.bet = pharaoh.user_coins;
            $('#bet_amount').val(pharaoh.bet);
        }

    </script>

@endpush

