<div class="modal fade buyCoins" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle" style="color: #333; font-weight: 800;">BUY THE
                    COINS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="alert alert-danger d-none w-100" id="coins_add_false" role="alert">
                            Вам нужно пополнить счет что бы купить COINS
                        </div>

                        <div class="alert alert-success d-none w-100" id="coins_add_true" role="alert">
                            На ваш счет добавленно <b id="addCoins">5</b> COINS
                        </div>
                    </div>
                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-md-3 text-center">
                                <img src="{{ $product->imgs[0]->url }}" alt="">
                                <h5 style="color: #333; font-weight: 800;">{{ $product->title }} <br> {{ $product->category->title }}</h5>
                                <button onclick="buyCoin({{ $product->title }})" class="btn btn-success btn-sm">BUY</button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Route::getCurrentRoute()->getName() == 'mines')
    <script>
        function buyCoin(amount) {
            axios.post(route('buy.coins', {
                amount: amount,
            }))
                .then(function (response) {
                    if(response.data.status == true){
                        $('#coins_add_true').removeClass('d-none');
                        $("#addCoins").text(response.data.add_coins);
                        $("#user_balance").text(response.data.user_balance);

                        balance = response.data.user_balance;
                        checkBalance(balance);

                        setTimeout(function () {
                            $('#coins_add_true').addClass('d-none');
                        }, 5000);
                    } else {
                        $('#coins_add_false').removeClass('d-none');
                        setTimeout(function () {
                            $('#coins_add_false').addClass('d-none');
                        }, 5000);
                    }
                })
                .catch(function (error) {

                });
        }
    </script>
@elseif(Route::getCurrentRoute()->getName() == 'main')
    <script>
        function buyCoin(amount) {
            axios.post(route('buy.coins', {
                amount: amount,
            }))
                .then(function (response) {
                    if(response.data.status == true){
                        $('#coins_add_true').removeClass('d-none');
                        $("#addCoins").text(response.data.add_coins);
                        $("#user_balance").text(response.data.user_balance);

                        cats.user_coins = response.data.user_balance;
                        setCoins(cats.user_coins);

                        setTimeout(function () {
                            $('#coins_add_true').addClass('d-none');
                        }, 5000);
                    } else {
                        $('#coins_add_false').removeClass('d-none');
                        setTimeout(function () {
                            $('#coins_add_false').addClass('d-none');
                        }, 5000);
                    }
                })
                .catch(function (error) {

                });
        }
    </script>
@else
    <script>
        function buyCoin(amount) {
            axios.post(route('buy.coins', {
                amount: amount,
            }))
                .then(function (response) {

                })
                .catch(function (error) {

                });
        }
    </script>
@endif