<nav id="nav" class="navbar navbar-expand-md bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('favicon.ico') }}" width="35" height="35"
                 style="margin-right: 5px;"> {{ config('app.name', 'MineStartProject') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('stone.pharaoh') }}">{{ __('STONE') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('main') }}">{{ __('SLOTS') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('mines') }}">{{ __('MINES') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('blackjack') }}">{{ __('BLACK JACK') }}</a>
                </li>

                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ __('POKER') }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('poker.holdem') }}">
                            {{ __('HOLDEM') }}
                        </a>

                        <a class="dropdown-item" href="#">
                            {{ __('OMAHA') }}
                        </a>

                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">|</a>
                </li>
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('LOGIN') }}</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('register'))
                            <a class="nav-link" href="{{ route('register') }}">{{ __('REGISTER') }}</a>
                        @endif
                    </li>
                @else
                    <li class="nav-item">
                        <button onclick="window.location = route('bank')" type="button" class="nav-link btn btn-primary btn-round p-2">
                            <i class="tim-icons icon-bank"></i> БАНК
                        </button>
                        <button type="button" class="nav-link btn btn-secondary btn-round p-2" id="show_u_b">
                            <i class="tim-icons icon-wallet-43"></i> <b>@json(round(Auth::user()->balance, 4))</b>USD
                        </button>
                    </li>

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            @if(Auth::user()->hasRole('admin'))
                                <a class="dropdown-item" href="{{ route('admin.main') }}">
                                    {{ __('Admin') }}
                                </a>
                                <hr>
                            @endif

                            <a class="dropdown-item" href="{{ route('user.show', Auth::user()->id) }}">
                                {{ __('Profile') }}
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
