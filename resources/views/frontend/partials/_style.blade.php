<style>
    select option{
        background-color: #171941 !important;
        color: #D650D6 !important;
    }

    .dataTables_info{
        color: #ffffff;
    }

    @media only screen and (max-width: 767px) {
        div.dataTables_paginate ul.pagination .page-item:first-of-type, div.dataTables_paginate ul.pagination .page-item:last-of-type, div.dataTables_paginate ul.pagination .page-item:nth-of-type(2), div.dataTables_paginate ul.pagination .page-item:nth-of-type(8) {
            display: list-item!important;
        }
    }

    /*#circle-div{*/
        /*position: fixed;*/
        /*background-color: #151E12;*/
        /*width: 100%;*/
        /*height: 100vh;*/
        /*z-index: 10000;*/
    /*}*/

    /*#circle {*/
        /*position: absolute;*/
        /*top: 50%;*/
        /*left: 50%;*/
        /*transform: translate(-50%,-50%);*/
        /*width: 150px;*/
        /*height: 150px;*/
    /*}*/

    /*.loader {*/
        /*width: calc(100% - 0px);*/
        /*height: calc(100% - 0px);*/
        /*border: 8px solid #6BA73B;*/
        /*border-top: 8px solid #1C2519;*/
        /*border-radius: 50%;*/
        /*animation: rotate 5s linear infinite;*/
    /*}*/

    /*@keyframes rotate {*/
        /*100% {transform: rotate(360deg);}*/
    /*}*/

    /**{*/
        /*transition: all 0.2s ease;*/
    /*}*/

</style>
