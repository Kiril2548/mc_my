@extends('frontend.layouts.app')

@section('content')
    <section class="section mt-5 section-lg">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-plain">
                        <div class="card-header">
                            <h3 class="profile-title text-center">
                                КУПИТЬ COINS
                            </h3>
                        </div>
                        <div class="card-body">
                            <select class="form-control" name="currency" id="currencies">

                            </select>

                            <br>

                            <input class="form-control" type="text" id="amount_change" value="" placeholder="10.4556">

                            <br>

                            <h3>Вы получите <b id="coins_amount">0</b> COINS</h3>
                            <div class="d-flex justify-content-end">
                                <button class="btn btn-warning btn-sm" id="buy_coins">КУПИТЬ</button>
                            </div>
                        </div>
                    </div>
                    <div class="card card-plain">
                        <div class="card-header">
                            <h3 class="profile-title text-center">
                                Вывести
                            </h3>
                        </div>
                        <div class="card-body">
                            <select class="form-control" name="currency" id="currencies2">

                            </select>

                            <br>

                            <input class="form-control" type="text" id="" value="" placeholder="Укажите сколько COINS хотите продать">

                            <br>

                            <h3>Вы получите <b id="">0</b> <span id="correct_currency_out"></span></h3>

                            <div class="d-flex justify-content-end">
                                <button class="btn btn-info btn-sm">ЗАКАЗАТЬ ВЫПЛАТУ</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="overflow-x: auto;">
                        <div class="card-body text-center">
                            <h3>ВАШ БАЛАНС</h3>
                            <div style="max-height: 550px; overflow-x: hidden;">
                                <table class="table" id="balance_table">
                                    <thead>
                                    <tr>
                                        <th>ВАЛЮТА</th>
                                        <th>КОЛИЧЕСТВО</th>
                                    </tr>
                                    </thead>
                                    <tbody id="balance_table_body">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection


@push('scripts')
    <script>
        var
            balances = @json($balances),
            max = 0,
            coins = 0,
            coins_on_user_balance = @json(Auth::user()->coins),
            index = 0,
            from_ = '',
            to_ = 'COINS',
            amount_ = 0;
        ;

        $(document).ready(function () {
            printCurency();
            printBalance();
            $('#balance_table').DataTable({
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                },
                "order": [],
                "paging": false
            });

            setTimeout(function () {
                $('.col-sm-12').removeClass('col-md-6');
            }, 500);

            setInterval(function () {
                checkAmountCoins();
            }, 100);

            $('#currencies').change(function () {
                index = $(this).val() - 1;
            });
        });

        function checkAmountCoins() {
            var coins_a = ($('#amount_change').val() * balances[index].rates.rate) * 100;
            $('#coins_amount').text(coins_a.toFixed(4));

            if ($('#amount_change').val() > balances[index].amount) {
                $('#amount_change').val(balances[index].amount);
            }
            if ($('#amount_change').val() < 0) {
                $('#amount_change').val(0);
            }
            amount_ = $('#amount_change').val();
            from_ = balances[index].rates.id;
        }

        function printCurency() {
            var car = "";
            var carFrom = "";

            car += '<option disabled value="0" selected>Выберите что будете обменивать</option>'
            carFrom += '<option disabled value="0" selected>Выберите куда будете выводить</option>'
            for (var i = 0; i < balances.length; i++) {
                car += '<option value="' + (i + 1) + '">' + balances[i].rates.crypto_symbol + ' - ' + (balances[i].amount) + '</option>'
                carFrom += '<option value="' + (i + 1) + '">' + balances[i].rates.crypto_symbol + ' - ' + (balances[i].amount) + '</option>'
            }
            $('#currencies').html(car);
            $('#currencies2').html(carFrom);
        }

        function printBalance() {
            var bal = "";
            bal += '<tr>' +
                '<td class="text-left">COINS:</td>' +
                '<td class="text-left">' +
                '    <b>' + coins_on_user_balance.toFixed(4) + '</b> <img src="/crypto_img/coin.svg" width="35px" alt="">' +
                '</td>' +
                '</tr>';
            for (var i = 0; i < balances.length; i++) {
                bal += '<tr>' +
                    '<td class="text-left">' + balances[i].rates.crypto_symbol + ':</td>' +
                    '<td class="text-left">' +
                    '    <b>' + (balances[i].amount).toFixed(4) + '</b> ' +
                    '<img src="/crypto_img/' + balances[i].rates.crypto_symbol + '.svg" width="25px" alt="">' +
                    '</td>' +
                    '</tr>';
            }
            $('#balance_table_body').html(bal);
        }

        $('#buy_coins').click(function () {
            axios.post(route('bank.exchange', {
                from_: from_,
                to_: to_,
                amount_: amount_
            }))
                .then(function (response) {

                    balances = response.data.balances;
                    coins_on_user_balance = response.data.coins;

                    printCurency();
                    printBalance();

                })
                .catch(function (error) {

                });
        });


    </script>
@endpush
