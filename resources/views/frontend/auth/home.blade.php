@extends('frontend.layouts.app')

@section('content')
<div class="container mt-4" id="app">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h3>Statistic</h3></div>

                <div class="card-body">
                    <h4>MINES</h4>
                    <table class="table" id="mines_table">
                        <thead>
                        <tr>
                            <th>GAME #</th>
                            <th>STEPS</th>
                            <th>MINES</th>
                            <th>ROWS</th>
                            <th>STATUS</th>
                            <th>RESULT</th>
                            <th>BEFORE</th>
                            <th>AFTER</th>
                            <th>PLAY_AT</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mine_games as $game)
                            <tr>
                                <td>{{ $game->id }}</td>
                                <td>{{ $game->steps }}</td>
                                <td>{{ $game->mines }}</td>
                                <td>{{ $game->rows }}x{{ $game->rows }}</td>
                                @if($game->win)
                                    <td><span class="badge badge-success">WIN</span></td>
                                @else
                                    <td><span class="badge badge-danger">LOSE</span></td>
                                @endif

                                @if($game->win_cash >= 0)
                                    <td><span style="color: #23c123 !important;">+<b>{{ $game->win_cash }}</b> COINS</span></td>
                                @else
                                    <td><span style="color: #8b3537 !important;"><b>{{ $game->win_cash }}</b> COINS</span></td>
                                @endif
                                <td><b>{{ $game->balance_before }}</b> COINS</td>
                                <td><b>{{ $game->balance_after }}</b> COINS</td>
                                <td>{{ $game->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <hr>

                    <h4>SLOTS</h4>
                    <table class="table" id="slots_table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>GAME</th>
                            <th>STATUS</th>
                            <th>PRICE</th>
                            <th>RESULT</th>
                            <th>BEFORE</th>
                            <th>AFTER</th>
                            <th>PLAY_AT</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($slot_games as $game)
                            <tr>
                                <td>{{ $game->id }}</td>
                                <td>{{ $game->game }}</td>
                                @if($game->win)
                                    <td><span class="badge badge-success">WIN</span></td>
                                @else
                                    <td><span class="badge badge-danger">LOSE</span></td>
                                @endif
                                <td><b>{{ $game->price }}</b> COINS</td>
                                <td><b>{{ $game->win_cash }}</b> COINS</td>
                                <td><b>{{ $game->balance_before }}</b> COINS</td>
                                <td><b>{{ $game->balance_after }}</b> COINS</td>
                                <td>{{ $game->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $('#mines_table').DataTable({
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                },
                "order": [],
                "pagingType": "numbers",
                "lengthMenu": [ 5, 10, 25, 50, 75, 100 ]
            });

            $('#slots_table').DataTable({
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                },
                "order": [],
                "pagingType": "numbers",
                "lengthMenu": [ 5, 10, 25, 50, 75, 100 ]
            });
        });
    </script>
@endpush
