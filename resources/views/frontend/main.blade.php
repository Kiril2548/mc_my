@extends('frontend.layouts.app')

@push('style')
    <style>
        *{
            transition: all 0.3s ease;
        }
        .main_win{
            display: none;
        }
        .main_win_s{
            display: block;
            width: 100% !important;
            height: 100vh !important;
        }
        .win{
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%);
            z-index: 500;
            border: 1px solid yellow;
            background-color: rgba(185, 185, 0, 0.47);
            color: #000;
            text-align: center;
            padding: 250px;
            font-size: 50px;
            font-weight: 800;
        }
        .win-Alert{
            top: 10000px;
        }
        .item-casino{
            height: 80px;
            width: 100px;
            background-color: #fff;
            color: #000;
            margin-right: 5px;
        }
        .item-win-casino{
            height: 80px;
            width: 100px;
            background-color: #fff;
            color: #000;
        }

        .svgItem{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -580px -320px;
            content: '';
            position: absolute;
            top: -200px;
            left: 0;
            width: 80px;
            height: 80px;
            /*border-radius: 13px;*/
        }

        .svgCherry, .item1{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -600px -325px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .svg7, .item0{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -600px -185px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .svgStrawberry, .item2{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -710px -445px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .item3{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -600px -445px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .item4{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -600px -575px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .svgWatermelon, .item5{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -600px -705px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .svgKakos, .item6{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -600px -835px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .item7{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -710px -835px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
       .item8{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -710px -705px;
            text-align: center;
            width: 80px;
            height: 80px;
        }
        .item9{
            background-image: url({{asset('/img/games/slots/games_v=1.svg')}});
            background-repeat: no-repeat;
            background-size: 1920px 1080px;
            background-position: -710px -575px;
            text-align: center;
            width: 80px;
            height: 80px;
        }

    </style>
@endpush

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md text-center">
                <h1>{{ config('app.name', 'MineStartProject') }}</h1>
            </div>

            <div class="container">

                    {{--<div class="win win-Alert">--}}
                        {{--1000$--}}
                    {{--</div>--}}

                <div class="row justify-content-center">
                    <ul class="nav nav-pills nav-pills-primary nav-pills-icons" role="tablist">
                        <!--
                            color-classes: "nav-pills-primary", "nav-pills-info", "nav-pills-success", "nav-pills-warning","nav-pills-danger"
                        -->
                        <li class="nav-item">
                            <a class="nav-link active" href="#dashboard-1" role="tab" data-toggle="tab">
                                <i class="tim-icons icon-laptop"></i>
                                CAT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#schedule-1" role="tab" data-toggle="tab">
                                <i class="tim-icons icon-settings-gear-63"></i>
                                EGYPT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tasks-1" role="tab" data-toggle="tab">
                                <i class="tim-icons icon-calendar-60"></i>
                                DOGS
                            </a>
                        </li>
                    </ul>
                    @if(isset(Auth::user()->coins))
                        <div class="col-md-12 d-flex justify-content-center">
                            <div class="btn btn-warning" >У вас <span class="user_coins">{{Auth::user()->coins}}</span></div>

                            <div class="d-flex justify-content-center prize">

                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="tab-content tab-space">
                            <div class="tab-pane active" id="dashboard-1">
                                <div class="container-fluid">
                                    <div class="row">

                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-4 text-center" style="min-width: 350px">
                                            <div class="d-flex automate">
                                                <div class="baraban first_baraban" style="overflow-y: hidden; max-height: 249px; min-height: 249px;">
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item0"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item1"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item2"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item3"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item4"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item5"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item6"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item7"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item8"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item9"></svg>
                                                    </div>
                                                </div>
                                                <div class="baraban first_baraban1" style="overflow-y: hidden; max-height: 249px;">
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item0"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item1"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item2"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item3"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item4"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item5"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item6"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item7"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item8"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item9"></svg>
                                                    </div>
                                                </div>
                                                <div class="baraban first_baraban2" style="overflow-y: hidden; max-height: 249px;">
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item0"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item1"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item2"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item3"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item4"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item5"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item6"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item7"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item8"></svg>
                                                    </div>
                                                    <div class="item-casino text-center mb-1">
                                                        <svg class="item9"></svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 game_log" id="" style="overflow-y: hidden; max-height: 250px;">
                                            @if(!$latestGames->isEmpty())
                                                @foreach($latestGames as $game)
                                                    <div class="mb-1 content-right" style="height: 50px; border-bottom: 1px solid white; color: white;">
                                                        @if($game->win_cash)
                                                            <span>+ {{$game->win_cash}}</span>
                                                        @else
                                                            <span>- {{$game->price}}</span>
                                                        @endif
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 d-flex justify-content-center">
                                            <div class="">
                                                <button class="btn btn-warning bet_cats_plus" onclick="betCatsPlus()">
                                                    x 2
                                                </button>
                                                <br>
                                                <button class="btn btn-warning bet_cats_minus" onclick="betCatsMinus()">
                                                    x 1 / 2
                                                </button>
                                            </div>
                                            <div class="" style="width: 200px">
                                                <input type="text" class="bet_cats form-control form-control-lg" value="1" onchange="setBetCats()" style="border:0; height:90px; font-size: 3rem; font-weight: 800; text-align: center;">
                                            </div>
                                            <button class="btn btn-warning cats_auto_spin" onclick="switchAutoSpin()">
                                                auto spine
                                            </button>
                                            <button class="btn btn-warning start_cats" onclick="start()">START</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="schedule-1">
                                EGYPT
                            </div>
                            <div class="tab-pane" id="tasks-1">
                                DOGS
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="links">
                {{--<a href="{{ route('main')  }}">MAIN PAGE</a>--}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{asset('js/games/slots/cats.js')}}"></script>
    <script src="{{asset('js/games/coins.js')}}"></script>

    <script>
        init({
            'bet_coin' : 1,
            'user_coins' : <?= (isset(Auth::user()->coins) ? Auth::user()->coins : 0) ?>,
        });
    </script>

@endpush

