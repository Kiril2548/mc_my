var cats = {};

function init(catsModel) {
    cats = catsModel;
    cats.isGame = false;
}

function start() {
    if(!cats.isGame){
        var self = this;
        cats.isGame = true;

        if((cats.user_coins-cats.bet_coin) < 0){
            alert('Не хватает монет, пошёл нахуй');
            return false;
        }

        switchDisabledAtStart();
        minusCoins(cats.bet_coin);
        cats.user_coins -= cats.bet_coin;

        axios.post(route('games.slots.cats.store'), {'bet_coin' : cats.bet_coin}).then(function (response) {

            generateItem(response.data);
            cats.user_coins += response.data.game.prize;

        }).catch(function (error) {}).then(function () {});
    }

    return false;
}

function generateItem(dataAll) {

    var data = dataAll.game;

    var endRand1 = getRandomArbitrary(50, 190);
    var endRand2 = getRandomArbitrary(50, 190);
    var endRand3 = getRandomArbitrary(50, 190);

    var pushSelect1 = false;
    var pushSelect2 = false;
    var pushSelect3 = false;

    var numberSelect = $(".first_baraban .select").index();
    var numberSelect1 = $(".first_baraban1 .select1").index();
    var numberSelect2 = $(".first_baraban2 .select2").index();

    $(".first_baraban >").each(function(i, elem) {
        if((numberSelect - 20) > i){
            $(this).remove();
        }
        if ($(this).hasClass("select")) {
            $(this).removeClass('select');
            return false;
        }
    });
    $(".first_baraban1 >").each(function(i, elem) {
        if((numberSelect1 - 20) > i){
            $(this).remove();
        }
        if ($(this).hasClass("select1")) {
            $(this).removeClass('select1');
            return false;
        }
    });
    $(".first_baraban2 >").each(function(i, elem) {
        if((numberSelect2 - 20) > i){
            $(this).remove();
        }
        if ($(this).hasClass("select2")) {
            $(this).removeClass('select2');
            return false;
        }
    });

    for(var i = 0, numberItem = 0; i < 200; i++, numberItem++){

        var checkViewSelect = false;

        numberItem = (numberItem > 9) ? 0 : numberItem;

        if(i > 49){
            if(i > endRand1 && (data.combine[0] == numberItem) && (!pushSelect1)){
                $(".first_baraban").append('<div class="item-win-casino text-center mb-1 select"><svg class="item'+numberItem+'"></svg></div>');
                checkViewSelect = true;
                pushSelect1 = true;
            }
            if(i > endRand2 && (data.combine[1] == numberItem) && (!pushSelect2)){
                $(".first_baraban1").append('<div class="item-win-casino text-center mb-1 select1"><svg class="item'+numberItem+'"></svg></div>');
                checkViewSelect = true;
                pushSelect2 = true;
            }
            if(i > endRand3 && (data.combine[2] == numberItem) && (!pushSelect3)){
                $(".first_baraban2").append('<div class="item-win-casino text-center mb-1 select2"><svg class="item'+numberItem+'"></svg></div>');
                checkViewSelect = true;
                pushSelect3 = true;
            }
        }

        if(!checkViewSelect){
            $(".first_baraban").append('<div class="item-casino text-center mb-1"><svg class="item'+numberItem+'"></svg></div>');
            $(".first_baraban1").append('<div class="item-casino text-center mb-1"><svg class="item'+numberItem+'"></svg></div>');
            $(".first_baraban2").append('<div class="item-casino text-center mb-1"><svg class="item'+numberItem+'"></svg></div>');
        }

        checkViewSelect = false;
    }

    startSpine(dataAll.latestGame);
}

function startSpine(latestGame) {
    var scrollTo1 = $(".first_baraban").scrollTop() + (($('.select').offset()).top) - 445;
    var scrollTo2 = $(".first_baraban1").scrollTop() + (($('.select1').offset()).top)  - 445;
    var scrollTo3 = $(".first_baraban2").scrollTop() + (($('.select2').offset()).top)  - 445;

    var rand1 = getRandomArbitrary(200, 4001);
    var rand2 = getRandomArbitrary(rand1, rand1+4000);
    var rand3 = getRandomArbitrary(rand2, rand1+4000);

    $('.first_baraban').animate({
        scrollTop : scrollTo1
    }, rand1);
    $('.first_baraban1').animate({
        scrollTop : scrollTo2
    }, rand2);
    $('.first_baraban2').animate({
        scrollTop : scrollTo3
    }, rand3);

    var bigRand = (rand1 > rand2) ? ((rand1 > rand3) ? rand1 : (rand3)) : ((rand2 > rand3) ? rand2 : rand3);

    setTimeout(function () {
        switchDisabledAtStart();
        showPrize(latestGame.prize);
        plusCoins(latestGame.prize);
        addInLog(latestGame);

        $('.first_baraban').stop();
        $('.first_baraban1').stop();
        $('.first_baraban2').stop();

        cats.isGame = false;

        if(cats.cats_auto_spin){
            start();
        }
    }, bigRand);

}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function switchDisabledAtStart() {
    if($('.start_cats').is(':disabled')){
        $('.start_cats').removeAttr('disabled');
    }else{
        $('.start_cats').attr('disabled','disabled');
    }
}

function showPrize(prize) {
    $('.prize').append('<div class="btn btn-warning">Вы выграли '+prize+' койнов</div>');
    setTimeout(function () {
        $('.prize').empty();
    }, 3000);
}

function addInLog(latestGame) {
    var plusOrMinus = (latestGame.prize) ? '+' : '-';
    var game = '<div class="mb-1" style="height: 50px; border-bottom: 1px solid white; color: white;">' + plusOrMinus + ' ' + ((latestGame.prize) ? latestGame.prize : latestGame.price) + '</div>';
    $('.game_log').prepend(game);
}

function switchAutoSpin() {
    if(cats.cats_auto_spin){
        $('.cats_auto_spin').text('Auto spine off');
        cats.cats_auto_spin = false;
    }else{
        $('.cats_auto_spin').text('Auto spine on');
        start();
        cats.cats_auto_spin = true;
        setDisableStart(cats.cats_auto_spin);
    }
}

function setDisableStart(bool) {
    if(bool){
        $('.start_cats').attr('disabled','disabled');
    }else{
        $('.start_cats').removeAttr('disabled');
    }
}

function betCatsMinus() {
    if((cats.bet_coin/2) >= 1){
        cats.bet_coin /= 2;
        $('.bet_cats').val(cats.bet_coin);
    }
}

function betCatsPlus() {
    cats.bet_coin *= 2;
    $('.bet_cats').val(cats.bet_coin);
}

function setBetCats() {
    cats.bet_coin = parseInt($('.bet_cats').val());
}