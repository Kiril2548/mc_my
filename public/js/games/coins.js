function setCoins(quantity) {
    $('.user_coins').text(quantity);
}

function plusCoins(plusQuant){
    var result = parseInt($('.user_coins').text()) + plusQuant;
    setCoins(result);
}

function minusCoins(minusQuant) {
    var result = parseInt($('.user_coins').text()) - minusQuant;
    setCoins(result);
}