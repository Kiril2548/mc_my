<?php

namespace App\Providers;

use App\Models\AdminSetting;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use App\Models\UserBalance;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (\Schema::hasTable('settings')) {
            $settings_all = Setting::all();
            $settings = [];
            foreach ($settings_all as $setting) {
                $settings[$setting->key] = $setting->value;
            }
            view()->share('settings', $settings);
        }

        if (\Schema::hasTable('products')) {
            $products = Product::where('status', 1)->with('category', 'imgs')->get();
            view()->share('products', $products);
        }
//
        if (\Schema::hasTable('pages')) {
            $pages = Page::all();
            view()->share('pages', $pages);
        }
//
        if (\Schema::hasTable('admin_settings')) {
            $admin_settings_all = AdminSetting::all();
            $admin_settings = [];
            foreach ($admin_settings_all as $setting) {
                if ($setting->value == 1)
                    $setting->value = true;
                else
                    $setting->value = false;
                $admin_settings[$setting->key] = $setting->value;
            }
            view()->share('admin_settings', $admin_settings);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
