<?php

namespace App\Http\Controllers;

use App\Models\Games\Poker\HoldemPoker;
use Illuminate\Http\Request;

class PokerController extends Controller
{
    public function start()
    {
        $poker_game = new HoldemPoker();
        $a = $poker_game->start_game();
        $user_card = $poker_game->getUserCards();
        $dealer_card = $poker_game->getDealerCards();
        $table = $poker_game->getTableCards();
        $poker_game->combination_of_dealer($dealer_card, $table);
        $poker_game->combination_of_user($user_card, $table);
        $poker_game->who_win();

        $a['user_combo_name'] = $poker_game->current_user_combo_name;
        $a['user_combo'] = $poker_game->current_user_combo;

        $a['dealer_combo_name'] = $poker_game->current_dealer_combo_name;
        $a['dealer_combo'] = $poker_game->current_dealer_combo;

        $a['winner'] = $poker_game->winner;

        dd($a);
    }
}
