<?php

namespace App\Http\Controllers;

use App\Models\Games\BlackJack;
use App\Models\Games\BlackJackLog;
use App\Models\TBot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BlackJackController extends Controller
{

    public function blackjack(Request $request)
    {
        $games = BlackJackLog::whereUserId(Auth::id())->latest()->take(5)->get([
            'id', 'win_cash', 'user_card'
        ]);

        return view('frontend.games.black_jack', compact('games'));
    }


    
}
