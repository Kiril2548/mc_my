<?php

namespace App\Http\Controllers;

use App\Models\Games\MineGameLog;
use App\Models\Games\SlotGameLog;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(User $user, Request $request)
    {
        $mine_games = MineGameLog::whereUserId($user->id)
            ->where('mode', 'pay')->orderBy('id', 'desc')->get();

        $slot_games = SlotGameLog::whereUserId($user->id)
            ->where('mode', 'pay')->orderBy('id', 'desc')->get();

        return view('frontend.auth.home', compact('mine_games', 'slot_games'));
    }
}
