<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Rate;
use App\Models\UserBalance;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{
    public function bank(Request $request)
    {

        $user = Auth::user();

        $count_balances = UserBalance::whereUserId($user->id)->count();
        if ($count_balances == 0){
            $rates = Rate::where('status', true)->get();
            foreach ($rates as $rate){
                $user_balance = new UserBalance();
                $user_balance->user_id = Auth::id();
                $user_balance->rate_id = $rate->id;
                $user_balance->save();
            }

        }
        $balances = UserBalance::whereUserId($user->id)->orderBy('amount', 'desc')->with('rates')->get();

        return view('frontend.auth.bank', compact('balances'));
    }

    public function exchange(Request $request)
    {
        $user = Auth::user();

        $bank = new Bank();
        $coins = $bank->exchangeCurrency($user, $request->amount_, $request->from_, $request->to_ ?? 'COINS');
        $balances = UserBalance::whereUserId($user->id)->orderBy('amount', 'desc')->with('rates')->get();
        return response()->json([
            'balances'  => $balances,
            'coins'     => $coins
        ]);
    }
}
