<?php

namespace App\Http\Controllers;

use App\Models\Games\SlotGameLog;
use App\Models\Games\Slots\Cat;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public $_gameSlotsCats = null;
    public $_gamesLog      = null;

    public function __construct(Cat $cat, SlotGameLog $gamesLog)
    {
        $this->_gameSlotsCats = $cat;
        $this->_gamesLog      = $gamesLog;
    }

    public function index(Request $request)
    {
        $latestGames = $this->_gamesLog->whereType($this->_gameSlotsCats->getType())
                                        ->whereGame($this->_gameSlotsCats->getName())
                                        ->latest()
                                        ->take(10)
                                        ->get(['price', 'win_cash', 'combine']);

        return view('frontend.main', [
            'cats' => $this->_gameSlotsCats,
            'latestGames' => $latestGames,
            'combineWin' => $this->_gameSlotsCats->_payouts,
        ]);
    }
}
