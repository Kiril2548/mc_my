<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewController extends Controller
{
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->get();

        return view('admin.new.index', compact('news'));
    }

    public function edit(News $news)
    {
        return view('admin.new.edit', compact('news'));
    }
}
