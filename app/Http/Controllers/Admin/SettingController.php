<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();

        $settings_category = [];
        foreach ($settings as $setting){
            if (!in_array($setting->setting_category, $settings_category)){
                array_push($settings_category, $setting->setting_category);
            }
        }

        return view('admin.setting.index', compact('settings', 'settings_category'));
    }
}
