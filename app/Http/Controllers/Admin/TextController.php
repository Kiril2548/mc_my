<?php

namespace App\Http\Controllers\Admin;

use App\Models\Text;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TextController extends Controller
{
    public function index()
    {
        $texts = Text::all();

        return view('admin.text.index', compact('texts'));
    }

    public function edit(Text $text)
    {
        return view('admin.text.edit', compact('text'));
    }
}
