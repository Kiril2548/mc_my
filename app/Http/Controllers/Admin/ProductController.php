<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Discounts;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::where('status', 1)->with('category', 'imgs', 'params', 'discount')->get();

        $discounts = Discounts::all();

        $categories = Category::where('status', 1)->get();

        if(count(Category::all()) == 0){
            return redirect()->route('admin.categories.index');
        } else {
            return view('admin.product.index', compact('products','categories', 'discounts'));
        }
    }

    public function edit(Product $product)
    {
        $product = $product->load('category', 'imgs', 'params', 'discount');

        $discounts = Discounts::all();

        $imgs = $product->imgs;
        $params = $product->params;

        $categories = Category::all();

        return view('admin.product.edit', compact('product', 'category', 'params', 'imgs', 'discounts', 'categories'));
    }
}
