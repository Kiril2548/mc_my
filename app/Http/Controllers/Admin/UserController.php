<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        $users = User::with('roles', 'balances')->get();

        return view('admin.user.index', compact('users', 'roles'));
    }

    public function edit(User $user)
    {
        $user = $user->load('roles');
        $roles = Role::all();

        return view('admin.user.edit', compact('user', 'roles'));
    }
}
