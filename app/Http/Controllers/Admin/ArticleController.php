<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\ArticleCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{

    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->get();

        $categories = ArticleCategory::all();

        return view('admin.article.index', compact('articles', 'categories'));
    }

    public function edit(Article $article)
    {
        $categories = ArticleCategory::all();

        return view('admin.article.edit', compact('article', 'categories'));
    }

}
