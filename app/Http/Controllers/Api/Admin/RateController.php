<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Rate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RateController extends Controller
{
    public function store(Request $request)
    {

        $rate = new Rate();
        $rate->fill($request->all());
        $rate->save();

        return Rate::all();
    }

    public function update(Rate $rate, Request $request)
    {
        $rate->fill($request->all());
        $rate->save();

        return $rate;
    }

    public function destroy(Rate $rate)
    {
        if ($rate->status == 1){
            $rate->status = 0;
        } else {
            $rate->status = 1;
        }
        $rate->save();
        return Rate::all();
    }

    public function search(Request $request)
    {
        return Rate::where('currency', 'like', '%' . $request->search . '%')
            ->orWhere('crypto_tag', 'like', '%' . $request->search . '%')
            ->orWhere('crypto_symbol', 'like', '%' . $request->search . '%')
            ->orWhere('rate', 'like', '%' . $request->search . '%')
            ->get();
    }
}
