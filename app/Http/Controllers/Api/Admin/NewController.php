<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\NewRequest;
use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewController extends Controller
{
    public function store(NewRequest $request)
    {

        $new = new News();
        $new->fill($request->all());
        $new->slug = $request->title;
        $new->save();

        return News::all();
    }

    public function update(News $news, NewRequest $request)
    {
        $news->fill($request->all());
        $news->slug = $request->title;
        $news->save();

        return $news;
    }

    public function search(Request $request)
    {
        return News::where('title', 'like', '%' . $request->search . '%')
            ->orWhere('text', 'like', '%' . $request->search . '%')
            ->get();
    }

    public function destroy(News $news)
    {
        $news->delete();
        return News::all();
    }
}
