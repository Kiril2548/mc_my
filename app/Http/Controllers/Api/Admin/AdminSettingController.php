<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\AdminSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminSettingController extends Controller
{
    public function update(Request $request)
    {

        $settings = $request->all();

        foreach ($settings as $key => $setting){
            $set = AdminSetting::where('key', $key)->first();
            $set->value = $setting;
            $set->save();
        }

        return AdminSetting::all();
    }
}
