<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function store(Request $request)
    {

        $setting = new Setting();
        $setting->fill($request->all());
        $setting->save();

        $settings = Setting::all();

        $settings_category = [];
        foreach ($settings as $setting){
            if (!in_array($setting->setting_category, $settings_category)){
                array_push($settings_category, $setting->setting_category);
            }
        }


        return [
            'settings_category' => $settings_category,
            'settings'          => $settings
        ];
    }

    public function update(Request $request)
    {

        $settings =$request->all();
        foreach ($settings as $setting){
            $set = Setting::where('key', $setting['key'])->first();
            $set->value = $setting['value'];
            $set->save();
        }


        $settings = Setting::all();

        $settings_category = [];
        foreach ($settings as $setting){
            if (!in_array($setting->setting_category, $settings_category)){
                array_push($settings_category, $setting->setting_category);
            }
        }


        return [
            'settings_category' => $settings_category,
            'settings'          => $settings
        ];
    }
}
