<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{

    public function store(ArticleRequest $request)
    {

        $new = new Article();
        $new->fill($request->all());
        $new->slug = $request->title;
        $new->save();

        return Article::all();
    }

    public function update(Article $article, ArticleRequest $request)
    {
        $article->fill($request->all());
        $article->slug = $request->title;
        $article->save();

        return $article;
    }

    public function search(Request $request)
    {
        return Article::where('title', 'like', '%' . $request->search . '%')
            ->orWhere('text', 'like', '%' . $request->search . '%')
            ->get();
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return Article::all();
    }

}
