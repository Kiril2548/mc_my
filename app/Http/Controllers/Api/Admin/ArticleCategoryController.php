<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\ArticleCategoryRequest;
use App\Models\ArticleCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleCategoryController extends Controller
{

    public function store(ArticleCategoryRequest $request)
    {

        $category = new ArticleCategory();
        $category->slug = $request->title;
        $category->fill($request->all());
        $category->save();

        return ArticleCategory::with('category')->orderBy('place', 'asc')->get();
    }

    public function update(ArticleCategory $articleCategory, ArticleCategoryRequest $request)
    {
        $articleCategory->fill($request->all());
        $articleCategory->slug = $articleCategory->title;
        $articleCategory->save();

        return $articleCategory;
    }

    public function destroy(ArticleCategory $articleCategory)
    {


        if ($articleCategory->status == 1){
            $articleCategory->status = 0;
        } else {
            $articleCategory->status = 1;
        }
        $articleCategory->save();
        return ArticleCategory::with('category')->orderBy('place', 'asc')->get();
    }

    public function search(Request $request)
    {
        return ArticleCategory::where('title', 'like', '%' . $request->search . '%')->with('category')->get();
    }

    public function place(ArticleCategory $articleCategory, Request $request)
    {
        $articleCategory->place = $request->palce;
        $articleCategory->save();

        return ArticleCategory::orderBy('place', 'asc')->with('category')->where('status', 1)->get();
    }

}
