<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\PageStore;
use App\Http\Requests\PageUpdate;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function store(PageStore $request)
    {

        $page = new Page();

        $page->title = $request->title;
        $page->slug = $request->title;
        $page->description = $request->description;
        $page->page_text = $request->page_text;
        $page->img = $request->img ?? "";
        $page->key_words = isset($request->key_words) ? json_encode($request->key_words) : null;

        $page->save();

        return Page::all();
    }

    public function update(Page $page, PageUpdate $request)
    {



        $page->title = $request->title;
        $page->slug = $request->title;
        $page->description = $request->description;
        $page->page_text = $request->page_text;
        $page->img = $request->img ?? "";
        $page->key_words = json_encode($request->key_words);

        $page->save();

        return $page;
    }

    public function search(Request $request)
    {
        return Page::where('title', 'like', '%' . $request->search . '%')
            ->orWhere('description', 'like', '%' . $request->search . '%')
            ->orWhere('page_text', 'like', '%' . $request->search . '%')
            ->get();
    }

    public function destroy(Page $page)
    {
        $page->delete();
        return Page::all();
    }
}
