<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\ProductStore;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function store(ProductStore $request)
    {

        $product = new Product();
        $product->fill($request->product);
        $product->slug = $product->title;
        $product->price += ($product->price*0.2);
        $product->save();


        $product->params()->createMany($request->params);
        $product->imgs()->createMany($request->imgs);


        return Product::where('status', 1)->with('category', 'imgs', 'params', 'discount')->get();
    }

    public function update(Product $product, ProductStore $request)
    {
        $product->fill($request->product);
        $product->slug = $product->title;
        $product->save();

        $product->imgs()->delete();
        $product->params()->delete();

        $product->params()->createMany($request->params);
        $product->imgs()->createMany($request->imgs);
        $product = $product->load('category', 'imgs', 'params', 'discount');

        return $product;
    }

    public function search(Request $request)
    {
        return Product::where('status', 1)->where(function ($query) use ($request){
            $query->where('title', 'like', '%' . $request->search . '%')
                ->orWhere('desc', 'like', '%' . $request->search . '%')
                ->orWhere('slug', 'like', '%' . $request->search . '%')
                ->orWhere('price', 'like', '%' . $request->search . '%')
                ->orWhere('count', 'like', '%' . $request->search . '%')
                ->orWhere('created_at', 'like', '%' . $request->search . '%')
                ->orWhere('updated_at', 'like', '%' . $request->search . '%');
        })
            ->with('category', 'imgs', 'params', 'discount')->get();
    }

    public function destroy(Product $product)
    {
        if ($product->status == 1){
            $product->status = 0;
        } else {
            $product->status = 1;
        }
        $product->save();
        return Product::where('status', 1)->with('category', 'imgs', 'params', 'discount')->get();
    }
}
