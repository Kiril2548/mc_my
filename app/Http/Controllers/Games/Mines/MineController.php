<?php

namespace App\Http\Controllers\Games\Mines;

use App\Models\Games\MineGameLog;
use App\Models\Games\Mines\BaseMine;
use App\Models\TBot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MineController extends Controller
{
    protected $cof_plus = 0.01;
    protected $cof_minus = 0.03;

    public function mine(Request $request)
    {
        $user = Auth::user();

        $mine = new BaseMine();

        $mine->setRows(5);
        $mine->setMines($request->mines ?? (($mine->getRows()*$mine->getRows())-1));
        $mine->setPrice($request->bet ?? 1);

        $game = MineGameLog::whereId($request->game)->first();
        try{
            self::checkMode($user, $mine);

            if($request->game != 0){
                $cell = json_decode($game->game_data);
                $chance = $mine->countChance($game->steps, $game->rows, $user->chance_to_win);
                $value_of_this_key = $mine->lose_or_win($chance);
                $cell = $mine->lose_cell($cell, $request->key-1, $value_of_this_key);

                if($cell[$request->key-1] == 1){
                    $game->steps += 1;
                } else {
                    $user->chance_to_win += $this->cof_plus;
                    $game->steps += 1;
                    $game->win = false;
                    $game->win_cash = 0-$mine->getPrice();
                    $game->balance_before = $user->coins;
                    $game->balance_after = $user->coins - $mine->getPrice();
                }
                $game->save();
                $user->save();

                $games = MineGameLog::whereUserId(Auth::id())->latest()->take(5)->get([
                    'id', 'win_cash'
                ]);
                if($cell[$request->key-1] == 1) {
                    return response()->json([
                        'user_balance' => $user->coins,
                        'rows'  => $game->rows,
                        'price' => $game->price,
                        'step' => $game->steps,
                        'range' => round($mine->getPrizeOnThisStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'range_next' => round($mine->getPrizeOnNextStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'cell_item' => $cell[$request->key - 1],
                        'game' => $game->id,
                        'games' => $games,
                    ], 201);
                } else {
                    return response()->json([
                        'user_balance' => $user->coins,
                        'rows'  => $game->rows,
                        'price' => $game->price,
                        'step' => $game->steps,
                        'range' => round($mine->getPrizeOnThisStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'range_next' => round($mine->getPrizeOnNextStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'cell_item' => $cell[$request->key - 1],
                        'game' => $game->id,
                        'combination' => $cell,
                        'games' => $games,
                    ], 201);
                }

            } else {
                if ($user->coins <= 1){
                    return response()->json(['error' => 'Нужно пополнить запас coins ПИДОР'], 422);
                }
                if($user->coins < $mine->getPrice()){
                    return response()->json(['error' => 'Да не топи ты так понизь ставку!'], 422);
                }

                $cell = $mine->createCell();
                $chance = $mine->countChance(1, $mine->getRows(), $user->chance_to_win);
                $value_of_this_key = $mine->lose_or_win($chance);
                $cell = $mine->lose_cell($cell, $request->key-1, $value_of_this_key);

                $game = new MineGameLog();

                if($cell[$request->key-1] == 1){
                    $data = [
                        'user_id'         => $user->id,
                        'type'            => 'mines',
                        'game'            => 'EASE_MINES',
                        'mode'            => $mine->getMode(),
                        'mines'           => $mine->getMines(),
                        'rows'            => $mine->getRows(),
                        'steps'           => 1,
                        'auto_spin'       => false,
                        'price'           => $mine->getPrice(),
                        'range'           => $mine->getCof(),
                        'win_cash'        => 0,
                        'balance_before'  => $user->coins,
                        'balance_after'   => 0,
                        'game_data'       => json_encode($cell),
                    ];
                } else {
                    $user->chance_to_win += $this->cof_plus;
                    $data = [
                        'user_id'           => $user->id,
                        'type'              => 'mines',
                        'game'              => 'EASE_MINES',
                        'mode'              => 'pay',
                        'mines'             => $mine->getMines(),
                        'rows'              => $mine->getRows(),
                        'steps'             => 1,
                        'auto_spin'         => false,
                        'price'             => $mine->getPrice(),
                        'range'             => $mine->getCof(),
                        'win'               => false,
                        'win_cash'          => 0-$mine->getPrice(),
                        'balance_before'    => $user->coins,
                        'balance_after'     => $user->coins - $mine->getPrice(),
                        'game_data'         => json_encode($cell),
                    ];
                }
                $user->coins -= $mine->getPrice();
                $user->save();
                $game->fill($data);
                $game->save();

                $games = MineGameLog::whereUserId(Auth::id())->latest()->take(5)->get([
                    'id', 'win_cash'
                ]);
                if($cell[$request->key-1] == 1) {
                    return response()->json([
                        'user_balance' => $user->coins,
                        'rows'  => $game->rows,
                        'price' => $game->price,
                        'step' => $game->steps,
                        'range' => round($mine->getPrizeOnThisStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'range_next' => round($mine->getPrizeOnNextStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'cell_item' => $cell[$request->key - 1],
                        'game' => $game->id,
                        'games' => $games,
                    ], 201);
                } else {
                    return response()->json([
                        'user_balance' => $user->coins,
                        'rows'  => $game->rows,
                        'price' => $game->price,
                        'step' => $game->steps,
                        'range' => round($mine->getPrizeOnThisStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'range_next' => round($mine->getPrizeOnNextStep($game->rows, $game->mines, $game->price, $game->steps), 2),
                        'cell_item' => $cell[$request->key - 1],
                        'game' => $game->id,
                        'combination' => $cell,
                        'games' => $games,
                    ], 201);
                }
            }

        } catch (\Exception $e){
            $tBot = new TBot();
            $tBot->sendErrorMes($e);
            $games = MineGameLog::whereUserId(Auth::id())->latest()->take(5)->get([
                'id', 'win_cash'
            ]);
            return response()->json([
                'user_balance' => $user->coins,
                'price' =>  $game->price,
                'step' =>  $game->steps,
                'range' => 0,
                'range_next' => 0,
                'cell_item' => 0,
                'game'      => 0,
                'games'        => $games,
            ], 201);
        }
    }

    public function getCash(Request $request)
    {
        $mine = new BaseMine();
        $user = Auth::user();
        $game = MineGameLog::whereId($request->game)->first();

        $user->chance_to_win -= $this->cof_minus;

        try{
            $game->win_cash = round($mine->getPrizeOnThisStep($game->rows, $game->mines, $game->price, $game->steps), 2);

            $user->coins += $game->win_cash;
            $user->save();

            $game->balance_after = $user->coins;
            $game->win = true;
            $game->save();

            $games = MineGameLog::whereUserId(Auth::id())->latest()->take(5)->get([
                'id', 'win_cash'
            ]);
            return response()->json([
                'user_balance' => $user->coins,
                'games'        => $games,
            ], 201);
        } catch (\Exception $e){
            $tBot = new TBot();
            $tBot->sendErrorMes($e);

            $games = MineGameLog::whereUserId(Auth::id())->latest()->take(5)->get([
                'id', 'win_cash'
            ]);
            return response()->json([
                'user_balance' => $user->coins,
                'games'        => $games,
            ], 201);
        }
    }

    static function checkMode($user, $mine, $price = 0){
        if($user->coins <= 0){
            $mine->setMode('free');
        } else {
            $mine->setMode('pay');
        }
    }
}
