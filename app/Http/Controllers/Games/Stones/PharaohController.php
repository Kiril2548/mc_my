<?php

namespace App\Http\Controllers\Games\Stones;

use App\Models\Games\Stones\StonePharaoh;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PharaohController extends Controller
{
    public $_game = null;

    public function __construct()
    {
        $this->_game = new StonePharaoh();
    }

    public function index(Request $request)
    {
        return view('frontend.games.stones.pharaoh');
    }

    public function game(Request $request)
    {
        $response = null;

        $game = $this->_game->whereId($request->id)->whereUserId(Auth::id())->first();

        if(is_null($game)){
            $this->_game->__initGame($request->bet, $request->cellRow, $request->cellColumn)->start();
            $response = response()->json($this->_game->getResultPlay(), 201);
        }else{
            $game->__initGame(
                $game->bet,
                $request->cellRow,
                $request->cellColumn,
                $game->stone_value,
                $game->steps,
                json_decode($game->combines, true),
                $game->quantity_row,
                $game->quantity_column
            )->start();

            $response = response()->json($game->getResultPlay(), 200);
        }

        return $response;
    }
}
