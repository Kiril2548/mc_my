<?php

namespace App\Http\Controllers\Games\Slots;

use App\Models\Games\SlotGameLog;
use App\Models\Games\Slots\Cat;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CatsController extends Controller
{
    public $_game = null;
    public $_gameLog = null;

    public function __construct()
    {
        $this->_game = new Cat();
        $this->_gameLog = new SlotGameLog();
    }

    public function store(Request $request)
    {
        $response = null;

        $beforeCoins = Auth::user()->coins;

        $bet_coin = (empty($request->bet_coin)) ? 1 : $request->bet_coin;
        $this->_game->setBetCoin($bet_coin);

        if(Auth::user()->coins >= $this->_game->getBetCoin()){

            $this->_game->setMode(Cat::MODE_PAY);
            $this->_game->start();
            $this->_game->payForSpin();
            $this->_game->getPrize();

            $dataLog = [
                'user_id' => Auth::id(),
                'type'      => $this->_game->getType(),
                'game'      => $this->_game->getName(),
                'mode'      => $this->_game->getMode(),
                'steps'     => 1,
                'auto_spin' => false,
                'price'     => $this->_game->getBetCoin(),
                'range'     => $this->_game->_prize,
                'win'       => ($this->_game->_prize != 0) ? true : false,
                'win_cash'  => ($this->_game->_prize * $this->_game->getBetCoin()),
                'balance_before' => $beforeCoins,
                'balance_after'  => Auth::user()->coins,
                'combine' => json_encode($this->_game->_rand_combination),
            ];

            $this->_gameLog->fill($dataLog);
            $this->_gameLog->save();

            $response = response()->json([
                'game' => [
                    'combine' => $this->_game->_rand_combination,
                    'prize'   => $this->_game->_prize,
                    'id_won'  => $this->_game->_id_won,
                ],
                'latestGame' => [
                    'price'   => $this->_game->getBetCoin(),
                    'prize' => ($this->_game->_prize * $this->_game->getBetCoin()),
                ]
            ], 201);

        }else{
            $response = response()->json([
                'error' => 'Не достаточно денег',
            ], 401);
        }

        return $response;
    }
}
