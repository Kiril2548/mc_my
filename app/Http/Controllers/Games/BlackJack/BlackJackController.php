<?php

namespace App\Http\Controllers\Games\BlackJack;

use App\Models\Games\BlackJack;
use App\Models\Games\BlackJackLog;
use App\Models\TBot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BlackJackController extends Controller
{


    public function start(Request $request)
    {
        $b_j_log = new BlackJackLog();
        $b_j = new BlackJack();
        try {
            $user = Auth::user();
            if ($request->game == 0) {
                if ($user->coins <= 0.1) {
                    return response()->json(['error' => 'Нужно пополнить запас coins ПИДОР'], 422);
                }

                $b_j = $b_j->init_game($request->bet);
                $data = [
                    'user_id' => $user->id,
                    'bet' => $request->bet,
                    'balance_before' => $user->coins,
                    'dealer_card' => json_encode($b_j['dealer_cards']),
                    'user_card' => json_encode($b_j['user_cards']),
                    'deck_last' => json_encode($b_j['deck']),
                    'deck' => json_encode($b_j['deck_of_game']),
                ];
                $b_j_log->fill($data);
                $b_j_log->save();
                $user->coins -= $request->bet;
                $user->save();

                return response()->json([
                    'game' => $b_j_log->id,
                    'bet' => $request->bet,
                    'user_balance' => $user->coins,
                    'user_card' => $b_j['user_cards'],
                    'user_card_value' => $b_j['user_cards_value'],
                    'dealer_card' => [$b_j['dealer_cards'][0]],
                ]);
            }
        } catch (\Exception $e) {
            $tb = new TBot();
            $tb->sendErrorMes($e);
        }
    }

    public function getCard(Request $request)
    {
        try {
            $b_j = new BlackJack();
            $game = BlackJackLog::whereUserId(Auth::id())->whereId($request->game)->first();
            $data = [
                'user_card' => json_decode($game->user_card),
                'deck_last' => json_decode($game->deck_last),
            ];

            $user_cards_value = $b_j->userValueOfCards($data['user_card']);
            if ($user_cards_value > 21) {
                $game->balance_after = Auth::user()->coins;
                $game->win_cash -= $game->bet;
                $game->status = 0;
                $game->save();

                $games = BlackJackLog::whereUserId(Auth::id())->latest()->take(5)->get([
                    'id', 'win_cash', 'user_card'
                ]);
                return response()->json([
                    'status' => 'lose',
                    'games' => $games,
                    'user_card' => $data['user_card'],
                    'user_card_value' => $user_cards_value,
                    'user_balance' => $game->balance_after,
                ]);
            }

            $new_user_card = $b_j->userTakeCard($data['deck_last'], $data['user_card']);
            $data['user_card'] = $new_user_card['cards'];
            $data['deck_last'] = $new_user_card['deck'];
            $user_cards_value = $b_j->userValueOfCards($data['user_card']);

            $game->user_card = json_encode($data['user_card']);
            $game->deck_last = json_encode($data['deck_last']);
            $game->save();

            if ($user_cards_value > 21) {
                $game->balance_after = Auth::user()->coins;
                $game->win_cash -= $game->bet;
                $game->status = 0;
                $game->save();

                $games = BlackJackLog::whereUserId(Auth::id())->latest()->take(5)->get([
                    'id', 'win_cash', 'user_card'
                ]);
                return response()->json([
                    'games' => $games,
                    'status' => 'lose',
                    'user_card' => $data['user_card'],
                    'user_card_value' => $user_cards_value,
                ]);
            } else if ($user_cards_value <= 21) {
                return response()->json([
                    'status' => 'play',
                    'user_card' => $data['user_card'],
                    'user_card_value' => $user_cards_value,
                ]);
            } else {
                $this->stop($request);
            }

        } catch (\Exception $e) {
            $tb = new TBot();
            $tb->sendErrorMes($e);
            return response()->json(['error' => 'Перезапустите пожалуйста страницу'], 422);
        }
    }

    public function stop(Request $request)
    {
        $games = BlackJackLog::whereUserId(Auth::id())->latest()->take(5)->get([
            'id', 'win_cash', 'user_card'
        ]);
        try {
            $b_j = new BlackJack();
            $game = BlackJackLog::whereUserId(Auth::id())->whereId($request->game)->first();
            $data = [
                'user_card' => json_decode($game->user_card),
                'deck_last' => json_decode($game->deck_last),
                'dealer_card' => json_decode($game->dealer_card),
            ];

            $dealer_cards_value = $b_j->dealerValueOfCards($data['dealer_card']);
            $user_card_value = $b_j->userValueOfCards($data['user_card']);

            return $this->dealerStep($game, $data['deck_last'], $dealer_cards_value, $user_card_value, $data['user_card'], $data['dealer_card']);


        } catch (\Exception $e) {
            $tb = new TBot();
            $tb->sendErrorMes($e);
            return response()->json(['error' => 'Перезапустите пожалуйста страницу'], 422);
        }
    }

    public function dealerStep($game, $deck, $dealer_cards_value, $user_card_value, $user_card, $dealer_card)
    {
        $b_j = new BlackJack();
        if ($dealer_cards_value > 16 && $dealer_cards_value <= 21) {
            $res = $b_j->whoWin($user_card, $dealer_card);
            return $this->returnRes($game, $res, $user_card, $user_card_value, $dealer_card, $dealer_cards_value, $deck);
        } elseif ($dealer_cards_value <= 16) {

            $new_dealer_card = $b_j->dealerTakeCard($deck, $dealer_card);
            $res = $b_j->whoWin($user_card, $new_dealer_card['cards']);
            return $this->returnRes($game, $res, $user_card, $user_card_value, $new_dealer_card['cards'], $new_dealer_card['cards_value'], $new_dealer_card['deck']);

        } elseif ($dealer_cards_value > 21) {
            return $this->returnRes($game, 'win', $user_card, $user_card_value, $dealer_card, $dealer_cards_value, $deck);
        }
    }

    public function returnRes($game, $res, $user_card, $user_value, $dealer_cards, $dealer_value, $deck)
    {
        try {
            $data = [
                'status' => 1,
                'user_card' => $user_card,
                'user_card_value' => $user_value,
                'dealer_card' => $dealer_cards,
                'dealer_value' => $dealer_value,
            ];
            if ($res == 'win_x3') {
                $data['rate'] = 3;
            } elseif ($res == 'win') {
                $data['rate'] = 1.5;
            } elseif ($res == 'lose') {
                $data['status'] = 0;
                $data['rate'] = 0;
            } elseif ($res == 'back') {
                $data['rate'] = 1;
            }

            $user = Auth::user();
            $user->coins += $game->bet * $data['rate'];

            $data_return = $data;

            $data['balance_after'] = $user->coins;
            $data['win_cash'] = $game->bet * $data['rate'];
            $data['deck_last'] = json_encode($deck);
            $data['user_card'] = json_encode($user_card);
            $data['dealer_card'] = json_encode($dealer_cards);
            $data['dealer_value'] = json_encode($dealer_value);
            $game->fill($data);

            DB::transaction(function () use ($game, $user) {
                $game->save();
                $user->save();
            });
            $games = BlackJackLog::whereUserId(Auth::id())->latest()->take(5)->get([
                'id', 'win_cash', 'user_card'
            ]);
            $data_return['game'] = 0;
            $data_return['games'] = $games;
            $data_return['balance_user'] = $data['balance_after'];

            return response()->json($data_return);

        } catch (\Exception $e) {
            $tb = new TBot();
            $tb->sendErrorMes($e);
            return response()->json(['error' => 'Перезапустите пожалуйста страницу'], 422);
        }
    }

}
