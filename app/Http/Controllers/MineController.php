<?php

namespace App\Http\Controllers;

use App\Models\Games\MineGameLog;
use App\Models\Games\Mines\BaseMine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MineController extends Controller
{
    public function index()
    {
        $games = MineGameLog::whereUserId(Auth::id())->latest()->take(5)->get([
            'id', 'win_cash'
        ]);

        return view('frontend.games.mines', compact('games'));
    }
}
