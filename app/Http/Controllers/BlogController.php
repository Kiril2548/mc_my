<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->with('comments')->paginate(5);

        return view('frontend.public.blog.index', compact('articles'));
    }

    public function mini()
    {
        $articles = Article::orderBy('created_at', 'desc')->with('comments')->paginate(10);

        return view('frontend.public.blog.mini', compact('articles'));
    }

    public function show(Article $article)
    {
        $article = $article->load('comments');

        $article->views++;
        $article->save();

        $comments = ArticleComment::where('article_id', $article->id)->with('user')->orderBy('created_at', 'desc')->get();

        foreach ($comments as $comment) {
            $comment->views++;
            $comment->save();
        }


        $articles = Article::orderBy('created_at', 'desc')->get();

        return view('frontend.public.blog.show', compact('article', 'articles', 'comments'));

    }

    public function add_comments(Article $article, Request $request)
    {
        if (isset($request->comment)) {
            $article->comments()->create([
                'user_id' => Auth::user()->id,
                'article_id' => $article->id,
                'comment' => $request->comment
            ]);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function update_comments(ArticleComment $comment, Request $request)
    {
        if ($comment->user_id == Auth::user()->id) {
            if (isset($request->comment)) {
                $comment->comment = $request->comment;
                $comment->save();
                return redirect()->back();
            } else {
                return redirect()->back();
            }
        }
    }

    public function delete_comments(ArticleComment $comment)
    {
        if ($comment->user_id == Auth::user()->id) {
            $comment->delete();
        }
        return redirect()->back();
    }

    public function like_comments(ArticleComment $comment)
    {
        if ($comment->user_id != Auth::user()->id) {
            $comment->likes++;
            $comment->save();
            return redirect()->back();
        }
    }
}
