<?php

namespace App\Http\Middleware;

use App\Models\UserBalance;
use Closure;
use Illuminate\Support\Facades\Auth;

class CountUserBalance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $usd = 0;
        $u_b = UserBalance::whereUserId(Auth::id())->with('rates')->get();
        foreach ($u_b as $item) {
            $usd += ($item->amount * $item->rates->rate);
        }
        $user->balance = $usd + ($user->coins / 100);
        $user->save();

        return $next($request);
    }
}
