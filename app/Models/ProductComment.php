<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductComment
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $product_id
 * @property int $user_id
 * @property string $comment
 * @property int $views
 * @property int $likes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductComment whereViews($value)
 */
class ProductComment extends Model
{
    //
}
