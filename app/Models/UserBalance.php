<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'rate_id',
        'amount',
    ];

    public function rates()
    {
        return $this->belongsTo('App\Models\Rate',  'rate_id', 'id');
    }

}
