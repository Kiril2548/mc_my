<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductImg
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $product_id
 * @property string $url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImg whereUrl($value)
 */
class ProductImg extends Model
{
    protected $fillable = [
        'url',
    ];
}
