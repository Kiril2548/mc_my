<?php

namespace App\Models\Games;

use Illuminate\Database\Eloquent\Model;

class BlackJackLog extends Model
{

    protected $fillable = [
        'id',
        'user_id',
        'type',
        'name',
        'mode',
        'status',
        'bet',
        'win_cash',
        'balance_before',
        'balance_after',
        'dealer_card',
        'user_card',
        'deck',
        'deck_last',
    ];

}
