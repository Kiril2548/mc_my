<?php

namespace App\Models\Games\Stones;

class StonePharaoh extends BaseStone
{
    protected $fillable = [
        'id',
        'user_id',
        'type',
        'name',
        'steps',
        'mode',
        'cell_row_select',
        'cell_column_select',
        'quantity_row',
        'quantity_column',
        'stone_value',
        'auto_spin',
        'bet',
        'win_cash',
        'balance_before',
        'balance_after',
        'combines',
        'processing',
    ];

    function saveInDbLog()
    {
        if($this->getMode() == self::MODE_PAY){
            $this->user_id = $this->_user->id;
            $this->type = $this->_type;
            $this->name = $this->_name;
            $this->steps = $this->_step;
            $this->mode = $this->getMode();
            $this->cell_row_select = $this->getCellRowSelect();
            $this->cell_column_select = $this->getCellColumnSelect();
            $this->quantity_row = $this->_quantityRow;
            $this->quantity_column = $this->_quantityColumn;
            $this->stone_value = $this->getStoneValue();
            $this->auto_spin = 0;
            $this->bet = $this->getBet();
            $this->win_cash = $this->_win_cash;
            $this->balance_before = $this->_user->coins;
            $this->balance_after = $this->_user->coins;
            $this->combines = json_encode($this->getCombines());
            $this->processing = $this->getProcessing();
            //Могут быть ошибки при большой нагрузке
        } else {
            $game = new StonePharaohFree();
        }

        $this->save();
    }
}
