<?php

namespace App\Models\Games\Stones;

use App\Models\Games\BaseGame;
use Auth;

abstract class BaseStone extends BaseGame
{
    const MIN_BET  = 1;

    const WIN_START = 'start';
    const WIN_PROCESSING = 'processing';
    const WIN_END = 'end';

    protected $_quantityRow = null;
    protected $_quantityColumn = null;
    protected $_combines = null;
    protected $_step = 0;
    protected $_type = 'stone';
    protected $_openCellNum = null;
    private $_stoneValue = null;
    private $_cellRowSelect = null;
    private $_cellColumnSelect = null;
    protected $_win_cash = 0;
    protected $_processing = null;

    protected $_bet = null;
    protected $_chanceToWin = null;

    protected $_user = null;

    public function __construct(array $attributes = [])
    {
        $this->_name = 'pharaoh';

        parent::__construct($attributes);
    }

    public function __initGame(int $bet = self::MIN_BET, int $cellRowSelect = null, int $cellColumnSelect = null, int $stoneValue = null, int $step = 0, array $combines = null, int $quantityRow = 3, int $quantityColumn = 5) : self
    {
        $this->_bet = $bet;
        if($this->processing != self::WIN_END && (!is_null($this->id) || Auth::user()->coins >= $this->_bet)) {
            $this->_step = $step;
            $this->_quantityRow = $quantityRow;
            $this->_quantityColumn = $quantityColumn;
            $this->_combines = $combines;
            $this->_stoneValue = $stoneValue;
            $this->_cellRowSelect = $cellRowSelect;
            $this->_cellColumnSelect = $cellColumnSelect;
            $this->_user = Auth::user();

            if (is_null($this->_user)) {
                $this->setMode(self::MODE_FREE);
            } else {
                $this->setMode(self::MODE_PAY);
            }
        }else{
            $this->processing = self::WIN_END;
        }

        return $this;
    }

    public function start()
    {
        if($this->processing != self::WIN_END){
            $this->generateStoneValue();
            $this->generateField();
            $this->play();
            $this->payForPlay();
            $this->givePrize();
            $this->saveInDbLog();
        }
        return $this;
    }

    public function generateField($reset = false) : void
    {
        if(is_null($this->_combines) || $reset){
            for ($i = 0; $i < $this->_quantityRow; $i++){
                for ($j = 0; $j < $this->_quantityColumn; $j++){
                    $this->_combines[$i][$j]['itemIsOpen'] = false;
                    if($j == ($this->_quantityColumn - 1)){
                        $prize = ($this->_bet * 0.5);
                    }else if($i == ($this->_quantityRow - 1)) {
                        $prize = ($this->_bet * 0.3);
                    }else{
                        $prize = ($this->_bet * ($i+1) /10);
                    }
                    $this->_combines[$i][$j]['prize'] = $prize;
                }
                shuffle($this->_combines[$i]);
            }
            shuffle($this->_combines);
        }
    }

    public function generateStoneValue($reset = false) : self
    {
        if(is_null($this->getStoneValue()) || $reset){
            $this->setStoneValue(rand(1, 6));
        }

        return $this;
    }

    public function play(): void
    {
        if(is_null($this->id)){
            $this->_processing = self::WIN_START;
        }else{
            $this->_processing = self::WIN_PROCESSING;
        }

        $item = &$this->_combines[$this->_cellRowSelect][$this->_cellColumnSelect]['itemIsOpen'];
        if($item == true){
            $this->_processing = self::WIN_END;
            $this->_chanceToWin = 0;
        }

        $item = true;

        if(!is_null($this->id)){
            $this->_step += 1;
        }

        if($this->_step >= $this->getStoneValue()){
            $this->_processing = self::WIN_END;
        }
    }

    public abstract function saveInDbLog();

    public function getProcessing()
    {
        return $this->_processing;
    }

    public function setProcessing($processing)
    {
        $this->_processing = $processing;
    }

    /**
     * @throws \Exception
     */
    private function payForPlay()
    {
        if(is_null($this->id)){
            $user = Auth::user();

            if($this->getBet() < self::MIN_BET){
                throw new \Exception('Ставка меньше минимальной');
            }

            $user->coins -= $this->getBet();

            $user->save();
        }
    }


    private function givePrize()
    {
        if($this->_processing == self::WIN_END){
            $prize = 0;
            for ($i = 0; $i < $this->_quantityRow; $i++){
                for ($j = 0; $j < $this->_quantityColumn; $j++){
                    if($this->_combines[$i][$j]['itemIsOpen']){
                        $prize += ($this->_combines[$i][$j]['prize']);
                    }
                }
            }
            $this->_win_cash = $prize;

            $user = Auth::user();
            $user->coins += $prize;
            $user->save();
        }
    }

    public function getCellRowSelect()
    {
        return $this->_cellRowSelect;
    }

    public function getCellColumnSelect()
    {
        return $this->_cellColumnSelect;
    }

    public function getResultPlay()
    {
        if(is_null($this->_cellRowSelect) || is_null($this->_cellColumnSelect)){
            return ['id' => $this->id, 'stoneValue' => $this->getStoneValue()];
        }

        return [
            'play' => $this->_combines[$this->_cellRowSelect][$this->_cellColumnSelect],
            'end' => ($this->_processing == self::WIN_END) ? true : false,
            'win_cash' => $this->_win_cash,
            'user_cash' => $this->_user->coins,
        ];
    }

    public function getStoneValue()
    {
        return $this->_stoneValue;
    }

    public function setStoneValue($stoneValue): void
    {
        if($stoneValue >= 1 && $stoneValue <= 6){
            $this->_stoneValue = $stoneValue;
        }else{
            throw new \InvalidArgumentException('Error stoneValue must be between 1 and 6. Now stoneValue is ' . $stoneValue);
        }
    }

    public function getOpenCellNum()
    {
        return $this->_openCellNum;
    }

    public function setOpenCellNum($openCellNum): void
    {
        if($this->_quantityRow * $this->_quantityColumn <= $openCellNum && $openCellNum >= 1){
            $this->_openCellNum = $openCellNum;
        }else{
            throw new \InvalidArgumentException('openCellNum between 1 and i*j ');
        }
    }

    public function getCombines()
    {
        return $this->_combines;
    }

    public function getBet()
    {
        return $this->_bet;
    }

}