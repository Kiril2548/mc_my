<?php

namespace App\Models\Games\Poker;

use Illuminate\Database\Eloquent\Model;

class PokerCombination
{
    public $combination_name = [
        "royal_flush" => [
            'ru' => 'Роял Флеш',
            'eu' => "Royal Flush"
        ],
        "straight_flush" => [
            'ru' => 'Стрит Флеш',
            'eu' => "Straight Flush"
        ],
        "four_of_a_kind" => [
            'ru' => 'Каре',
            'eu' => "Four of a Kind"
        ],
        "full_house" => [
            'ru' => 'Хулл Хаус',
            'eu' => "Full House"
        ],
        "flush" => [
            'ru' => 'Флеш',
            'eu' => "Flush"
        ],
        "straight" => [
            'ru' => 'Стрит',
            'eu' => "Straight"
        ],
        "three_of_a_kind" => [
            'ru' => 'Сет',
            'eu' => "Three of a Kind"
        ],
        "two_pairs" => [
            'ru' => 'Две Пары',
            'eu' => "Two Pairs"
        ],
        "pair" => [
            'ru' => 'Пара',
            'eu' => "Pair"
        ],
        "high_card" => [
            'ru' => 'Старшая карта',
            'eu' => "High Card"
        ],
        "nothing" => [
            'ru' => 'Ничего',
            'eu' => "Nothing"
        ],
    ];

    public $combo_value = [
        "Royal Flush"       =>  10,
        "Straight Flush"    =>  9,
        "Four of a Kind"    =>  8,
        "Full House"        =>  7,
        "Flush"             =>  6,
        "Straight"          =>  5,
        "Three of a Kind"   =>  4,
        "Two Pairs"         =>  3,
        "Pair"              =>  2,
        "High Card"         =>  1,
    ];

    public $card_value = [
        0 => 0,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        'J' => 11,
        'Q' => 12,
        'K' => 13,
        'A' => 14,
    ];


    public $current_user_combo_name = 'nothing';
    public $current_user_combo = [];

    public $current_dealer_combo_name = 'nothing';
    public $current_dealer_combo = [];

    public $winner = '';

    public $min = null;

    public function combination_of_user($user_card, $table)
    {

        $user_cards = array_merge($user_card, $table);

        if ($this->current_user_combo_name == 'nothing') {
            $this->flush($user_cards);
        }
        if ($this->current_user_combo_name == 'nothing') {
            $this->straight($user_cards);
        }
        if ($this->current_user_combo_name == 'nothing') {
            $this->pairs_set_full_house_kare($user_cards);
        }
        if ($this->current_user_combo_name == 'nothing') {
            $this->high_card($user_cards);
        }
    }

    public function combination_of_dealer($dealer_card, $table)
    {
        $dealer_cards = array_merge($dealer_card, $table);

        if ($this->current_dealer_combo_name == 'nothing') {
            $this->flush_d($dealer_cards);
        }
        if ($this->current_dealer_combo_name == 'nothing') {
            $this->straight_d($dealer_cards);
        }
        if ($this->current_dealer_combo_name == 'nothing') {
            $this->pairs_set_full_house_kare_d($dealer_cards);
        }
        if ($this->current_dealer_combo_name == 'nothing') {
            $this->high_card_d($dealer_cards);
        }
    }

    #region user
    public function flush($cards)
    {

        $one_suit = [
            'C' => 0,
            'B' => 0,
            'T' => 0,
            'P' => 0,
        ];

        foreach ($cards as $card) {
            foreach (array_keys($card) as $key) {
                if (substr($key, 0, 1) == 'T') {
                    $one_suit['T']++;
                } elseif (substr($key, 0, 1) == 'B') {
                    $one_suit['B']++;
                } elseif (substr($key, 0, 1) == 'C') {
                    $one_suit['C']++;
                } elseif (substr($key, 0, 1) == 'P') {
                    $one_suit['P']++;
                }
            }
        }

        foreach ($one_suit as $key => $item) {
            if ($item >= 5) {
                $this->current_user_combo_name = $this->combination_name['flush']['eu'];
                foreach ($cards as $card) {
                    foreach (array_keys($card) as $k) {
                        if (substr($k, 0, 1) == $key) {
                            array_push($this->current_user_combo, $card);
                        }
                    }
                }
            }
        }

        while (count($this->current_user_combo) > 5) {
            $del_index = 0;
            $this->min = null;
            foreach ($this->current_user_combo as $index => $item) {
                foreach ($item as $i) {
                    if ($this->min == null) {
                        $this->min = $this->card_value[$i];
                    } else {
                        if ($this->min > $this->card_value[$i]) {
                            $this->min = $this->card_value[$i];
                            $del_index = $index;
                        }
                    }
                }
            }
            unset($this->current_user_combo[$del_index]);
            $this->current_user_combo = array_values($this->current_user_combo);
        }
        $comb = $this->current_user_combo;
        if ($this->straight($this->current_user_combo, true)) {
            foreach ($this->current_user_combo[0] as $item){
                if ($item == 'A'){
                    $this->current_user_combo_name = $this->combination_name['royal_flush']['eu'];
                    return 1;
                }
            }
        }
        $this->current_user_combo = $comb;
    }

    public function straight($cards, $return = false)
    {
        $this->current_user_combo = [];
        $array_of_values_card = [];
        $card_start_from = 0;
        $wait_check_array = [];

        foreach ($cards as $card) {
            foreach ($card as $item) {
                array_push($array_of_values_card, $this->card_value[$item]);
            }
        }

        if ($return) {
            asort($array_of_values_card);
            $array_of_values_card = array_reverse($array_of_values_card);
            for ($i = 0; $i < count($array_of_values_card); $i++) {
                if (
                    ($array_of_values_card[$i]
                        - (
                        $i == count($array_of_values_card) - 1 ? $array_of_values_card[$i] : $array_of_values_card[$i + 1]
                        )
                    ) == 1
                    ||
                    ($array_of_values_card[$i]
                        - (
                        $i == count($array_of_values_card) - 1 ? $array_of_values_card[$i] : $array_of_values_card[$i + 1]
                        )
                    ) == 0
                ) {
                    if ($i == 0) {
                        $card_start_from = $array_of_values_card[$i];
                    }
                    array_push($wait_check_array, 'true');
                }
            }


            if (count($wait_check_array) == 5) {
                $card_start_from = array_search($card_start_from, $this->card_value);
                $this->current_user_combo_name = $this->combination_name['straight_flush']['eu'];
                $i = 0;
                while (count($this->current_user_combo) != 5) {
                    foreach ($cards as $card) {
                        foreach ($card as $item) {
                            if ($item == $card_start_from) {
                                array_push($this->current_user_combo, $card);
                                $card_start_from = array_search($this->card_value[$card_start_from] - 1, $this->card_value);
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        } else {
            asort($array_of_values_card);
            $array_of_values_card = array_reverse($array_of_values_card);
            for ($i = 0; $i < count($array_of_values_card); $i++) {
                if (
                    ($array_of_values_card[$i]
                        - (
                        $i == count($array_of_values_card) - 1 ? $array_of_values_card[$i] : $array_of_values_card[$i + 1]
                        )
                    ) == 1
                ) {
                    if ($i == 0) {
                        $card_start_from = $array_of_values_card[$i];
                    }
                    array_push($wait_check_array, 'true');
                }
            }
            if (count($wait_check_array) >= 5) {
                $card_start_from = array_search($card_start_from, $this->card_value);
                $this->current_user_combo_name = $this->combination_name['straight']['eu'];
                while (count($this->current_user_combo) != 5) {
                    foreach ($cards as $card) {
                        if (count($this->current_user_combo) == 5)
                            break;
                        foreach ($card as $item) {
                            if ($item == $card_start_from) {
                                array_push($this->current_user_combo, $card);
                                $card_start_from = array_search($this->card_value[$card_start_from] - 1, $this->card_value);
                            }
                        }
                    }
                }
            }
        }
    }

    public function pairs_set_full_house_kare($cards)
    {
        $array_of_values_card = [];

        foreach ($cards as $card) {
            foreach ($card as $item) {
                array_push($array_of_values_card, $this->card_value[$item]);
            }
        }

        foreach (array_count_values($array_of_values_card) as $key => $item) {
            if ($item == 2) {
                foreach ($cards as $card) {
                    foreach ($card as $k => $i) {
                        if ($key == $i) {
                            array_push($this->current_user_combo, $card);
                            if (count($this->current_user_combo) == 2) {
                                $this->current_user_combo_name = $this->combination_name['pair']['eu'];
                            } elseif (count($this->current_user_combo) == 4) {
                                $this->current_user_combo_name = $this->combination_name['two_pairs']['eu'];
                            }
                        }
                    }
                }
            }
            if ($item == 3) {
                foreach ($cards as $card) {
                    foreach ($card as $k => $i) {
                        if ($key == $i) {
                            array_push($this->current_user_combo, $card);
                            if (count($this->current_user_combo) == 3) {
                                $this->current_user_combo_name = $this->combination_name['three_of_a_kind']['eu'];
                            }
                        }
                    }
                }
            }
            if ($item == 4) {
                foreach ($cards as $card) {
                    foreach ($card as $k => $i) {
                        if ($key == $i) {
                            array_push($this->current_user_combo, $card);
                            if (count($this->current_user_combo) == 4) {
                                $this->current_user_combo_name = $this->combination_name['four_of_a_kind']['eu'];
                            }
                        }
                    }
                }
            }
            if (count($this->current_user_combo) == 5) {
                $this->current_user_combo_name = $this->combination_name['full_house']['eu'];
            }
        }
    }

    public function high_card($cards)
    {
        foreach ($cards as $card) {
            foreach ($card as $item) {
                if ($this->card_value[$item] > $this->user_high_card_value()) {
                    $this->current_user_combo = $card;
                    $this->current_user_combo_name = $this->combination_name['high_card']['eu'];
                }
            }
        }
    }

    public function user_high_card_value()
    {
        if (count($this->current_user_combo) < 1) {
            return 0;
        } else {
            foreach ($this->current_user_combo as $item) {
                return $this->card_value[$item];
            }
        }
    }
    #endregion

    #region dealer
    public function flush_d($cards)
    {

        $one_suit = [
            'C' => 0,
            'B' => 0,
            'T' => 0,
            'P' => 0,
        ];

        foreach ($cards as $card) {
            foreach (array_keys($card) as $key) {
                if (substr($key, 0, 1) == 'T') {
                    $one_suit['T']++;
                } elseif (substr($key, 0, 1) == 'B') {
                    $one_suit['B']++;
                } elseif (substr($key, 0, 1) == 'C') {
                    $one_suit['C']++;
                } elseif (substr($key, 0, 1) == 'P') {
                    $one_suit['P']++;
                }
            }
        }

        foreach ($one_suit as $key => $item) {
            if ($item >= 5) {
                $this->current_dealer_combo_name = $this->combination_name['flush']['eu'];
                foreach ($cards as $card) {
                    foreach (array_keys($card) as $k) {
                        if (substr($k, 0, 1) == $key) {
                            array_push($this->current_dealer_combo, $card);
                        }
                    }
                }
            }
        }

        while (count($this->current_dealer_combo) > 5) {
            $del_index = 0;
            $this->min = null;
            foreach ($this->current_dealer_combo as $index => $item) {
                foreach ($item as $i) {
                    if ($this->min == null) {
                        $this->min = $this->card_value[$i];
                    } else {
                        if ($this->min > $this->card_value[$i]) {
                            $this->min = $this->card_value[$i];
                            $del_index = $index;
                        }
                    }
                }
            }
            unset($this->current_dealer_combo[$del_index]);
            $this->current_dealer_combo = array_values($this->current_dealer_combo);
        }

        if ($this->straight($this->current_dealer_combo, true)) {
            foreach ($this->current_dealer_combo[0] as $item){
                if ($item == 'A'){
                    $this->current_dealer_combo_name = $this->combination_name['royal_flush']['eu'];
                    return 1;
                }
            }
        }
    }

    public function straight_d($cards, $return = false)
    {
        $this->current_dealer_combo = [];
        $array_of_values_card = [];
        $card_start_from = 0;
        $wait_check_array = [];

        foreach ($cards as $card) {
            foreach ($card as $item) {
                array_push($array_of_values_card, $this->card_value[$item]);
            }
        }

        if ($return) {
            asort($array_of_values_card);
            $array_of_values_card = array_reverse($array_of_values_card);
            for ($i = 0; $i < count($array_of_values_card); $i++) {
                if (
                    ($array_of_values_card[$i]
                        - (
                        $i == count($array_of_values_card) - 1 ? $array_of_values_card[$i] : $array_of_values_card[$i + 1]
                        )
                    ) == 1
                    ||
                    ($array_of_values_card[$i]
                        - (
                        $i == count($array_of_values_card) - 1 ? $array_of_values_card[$i] : $array_of_values_card[$i + 1]
                        )
                    ) == 0
                ) {
                    if ($i == 0) {
                        $card_start_from = $array_of_values_card[$i];
                    }
                    array_push($wait_check_array, 'true');
                }
            }


            if (count($wait_check_array) == 5) {
                $this->current_dealer_combo = [];
                $card_start_from = array_search($card_start_from, $this->card_value);
                $this->current_dealer_combo_name = $this->combination_name['straight_flush']['eu'];
                $i = 0;
                while (count($this->current_dealer_combo) != 5) {
                    foreach ($cards as $card) {
                        foreach ($card as $item) {
                            if ($item == $card_start_from) {
                                array_push($this->current_dealer_combo, $card);
                                $card_start_from = array_search($this->card_value[$card_start_from] - 1, $this->card_value);
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        } else {
            asort($array_of_values_card);
            $array_of_values_card = array_reverse($array_of_values_card);
            for ($i = 0; $i < count($array_of_values_card); $i++) {
                if (
                    ($array_of_values_card[$i]
                        - (
                        $i == count($array_of_values_card) - 1 ? $array_of_values_card[$i] : $array_of_values_card[$i + 1]
                        )
                    ) == 1
                ) {
                    if ($i == 0) {
                        $card_start_from = $array_of_values_card[$i];
                    }
                    array_push($wait_check_array, 'true');
                }
            }

            if (count($wait_check_array) >= 5) {
                $card_start_from = array_search($card_start_from, $this->card_value);
                $this->current_dealer_combo_name = $this->combination_name['straight']['eu'];
                while (count($this->current_dealer_combo) != 5) {
                    foreach ($cards as $card) {
                        if (count($this->current_dealer_combo) == 5)
                            break;
                        foreach ($card as $item) {
                            if ($item == $card_start_from) {
                                array_push($this->current_dealer_combo, $card);
                                $card_start_from = array_search($this->card_value[$card_start_from] - 1, $this->card_value);
                            }
                        }
                    }
                }
            }
        }
    }

    public function pairs_set_full_house_kare_d($cards)
    {
        $array_of_values_card = [];

        foreach ($cards as $card) {
            foreach ($card as $item) {
                array_push($array_of_values_card, $this->card_value[$item]);
            }
        }

        foreach (array_count_values($array_of_values_card) as $key => $item) {
            if ($item == 2) {
                foreach ($cards as $card) {
                    foreach ($card as $k => $i) {
                        if ($key == $i) {
                            array_push($this->current_dealer_combo, $card);
                            if (count($this->current_dealer_combo) == 2) {
                                $this->current_dealer_combo_name = $this->combination_name['pair']['eu'];
                            } elseif (count($this->current_dealer_combo) == 4) {
                                $this->current_dealer_combo_name = $this->combination_name['two_pairs']['eu'];
                            }
                        }
                    }
                }
            }
            if ($item == 3) {
                foreach ($cards as $card) {
                    foreach ($card as $k => $i) {
                        if ($key == $i) {
                            array_push($this->current_dealer_combo, $card);
                            if (count($this->current_dealer_combo) == 3) {
                                $this->current_dealer_combo_name = $this->combination_name['three_of_a_kind']['eu'];
                            }
                        }
                    }
                }
            }
            if ($item == 4) {
                foreach ($cards as $card) {
                    foreach ($card as $k => $i) {
                        if ($key == $i) {
                            array_push($this->current_dealer_combo, $card);
                            if (count($this->current_dealer_combo) == 4) {
                                $this->current_dealer_combo_name = $this->combination_name['four_of_a_kind']['eu'];
                            }
                        }
                    }
                }
            }
            if (count($this->current_dealer_combo) == 5) {
                $this->current_dealer_combo_name = $this->combination_name['full_house']['eu'];
            }
        }
    }

    public function high_card_d($cards)
    {
        foreach ($cards as $card) {
            foreach ($card as $item) {
                if ($this->card_value[$item] > $this->dealer_high_card_value()) {
                    $this->current_dealer_combo = $card;
                    $this->current_dealer_combo_name = $this->combination_name['high_card']['eu'];
                }
            }
        }
    }

    public function dealer_high_card_value()
    {
        if (count($this->current_dealer_combo) < 1) {
            return 0;
        } else {
            foreach ($this->current_dealer_combo as $item) {
                return $this->card_value[$item];
            }
        }
    }
    #endregion

    public function who_win()
    {
        if(
            $this->combo_value[$this->current_user_combo_name]
            >
            $this->combo_value[$this->current_dealer_combo_name]
        ){
            $this->winner = 'user'; // USER WIN
        } elseif (
            $this->combo_value[$this->current_user_combo_name]
            <
            $this->combo_value[$this->current_dealer_combo_name]
        ) {
            $this->winner = 'dealer'; // DEALER WIN
        } else {
            $this->winner = 'i don`t know';
        }
    }

}
