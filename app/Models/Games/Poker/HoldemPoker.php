<?php

namespace App\Models\Games\Poker;

use Illuminate\Database\Eloquent\Model;

class HoldemPoker extends BasePoker
{
    public function start_game()
    {
        return $this->init_game(5);
    }
}
