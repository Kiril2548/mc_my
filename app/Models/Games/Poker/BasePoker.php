<?php

namespace App\Models\Games\Poker;

use Illuminate\Database\Eloquent\Model;

class BasePoker extends PokerCombination
{
    protected $cards = [
        ['P2'   => 2],
        ['P3'   => 3],
        ['P4'   => 4],
        ['P5'   => 5],
        ['P6'   => 6],
        ['P7'   => 7],
        ['P8'   => 8],
        ['P9'   => 9],
        ['P10'  => 10],
        ['PJ'   => 'J'],
        ['PQ'   => 'Q'],
        ['PK'   => 'K'],
        ['PA'   => 'A'],
        ['T2'   => 2],
        ['T3'   => 3],
        ['T4'   => 4],
        ['T5'   => 5],
        ['T6'   => 6],
        ['T7'   => 7],
        ['T8'   => 8],
        ['T9'   => 9],
        ['T10'  => 10],
        ['TJ'   => 'J'],
        ['TQ'   => 'Q'],
        ['TK'   => 'K'],
        ['TA'   => 'A'],
        ['B2'   => 2],
        ['B3'   => 3],
        ['B4'   => 4],
        ['B5'   => 5],
        ['B6'   => 6],
        ['B7'   => 7],
        ['B8'   => 8],
        ['B9'   => 9],
        ['B10'  => 10],
        ['BJ'   => 'J'],
        ['BQ'   => 'Q'],
        ['BK'   => 'K'],
        ['BA'   => 'A'],
        ['C2'   => 2],
        ['C3'   => 3],
        ['C4'   => 4],
        ['C5'   => 5],
        ['C6'   => 6],
        ['C7'   => 7],
        ['C8'   => 8],
        ['C9'   => 9],
        ['C10'  => 10],
        ['CJ'   => 'J'],
        ['CQ'   => 'Q'],
        ['CK'   => 'K'],
        ['CA'   => 'A'],
    ];
    protected $card_in_deck = 56;
    protected $deck_count = 1;
    protected $cards_count = 1;
    protected $cards_types_count = 13;

    public $_bank = null;

    public $deck_cards = [];
    public $table_cards = [];
    public $dealer_cards = [];
    public $user_cards = [];

    protected $_type = 'Poker';
    protected $_name = 'Holdem';

    public function init_game($bank)
    {
        $this->setBet($bank);

        $deck = $this->createDeck();
        $this->setDeckCards($deck);

        $this->firstTwoUserCard();
        $this->firstTwoDealerCard();
        $this->tableCards();

        return [
            'deck_of_game' => $deck,
            'user_cards' => $this->user_cards,
            'dealer_cards' => $this->dealer_cards,
            'deck' => $this->getDeckCards(),
            'table' => $this->getTableCards(),
        ];
    }

    public function createDeck()
    {
        $deck = [];
        for ($i = 0; $i < $this->deck_count; $i++) {
            for ($j = 0; $j < $this->cards_count; $j++) {
                foreach ($this->cards as $key => $card) {
                    array_push($deck, $key);
                }
            }
        }
        shuffle($deck);
        $deck = $this->printDeck($deck);
        return $deck;
    }

    public function printDeck($deck)
    {
        $deck_view = [];
        foreach ($deck as $item) {
            array_push($deck_view, $this->cards[$item]);
        }
        return $deck_view;
    }

    public function firstTwoUserCard()
    {
        $deck = $this->getDeckCards();
        $first = $this->getRandCard($deck);
        $first_card = $deck[$first];
        unset($deck[$first]);
        $deck = array_values($deck);
        array_push($this->user_cards, $first_card);
        $this->setDeckCards($deck);

        $second = $this->getRandCard($deck);
        $second_card = $deck[$second];
        unset($deck[$second]);
        $deck = array_values($deck);
        array_push($this->user_cards, $second_card);
        $this->setDeckCards($deck);
    }

    public function firstTwoDealerCard()
    {
        $deck = $this->getDeckCards();

        $first = $this->getRandCard($deck);
        $first_card = $deck[$first];
        unset($deck[$first]);
        $deck = array_values($deck);
        array_push($this->dealer_cards, $first_card);
        $this->setDeckCards($deck);

        $second = $this->getRandCard($deck);
        $second_card = $deck[$second];
        unset($deck[$second]);
        $deck = array_values($deck);
        array_push($this->dealer_cards, $second_card);
        $this->setDeckCards($deck);
    }

    public function tableCards()
    {
        $deck = $this->getDeckCards();
        for ($i = 0; $i < 5; $i++){
            $card = $this->getRandCard($deck);
            $card_get = $deck[$card];
            unset($deck[$card]);
            $deck = array_values($deck);
            array_push($this->table_cards, $card_get);
            $this->setDeckCards($deck);
        }
    }

    public function getRandCard($deck)
    {
        return rand(0, (count($deck) - 1));
    }


    public
    function getBet()
    {
        return $this->_bet;
    }

    public
    function setBet($bank): void
    {
        $this->_bet = $bank;
    }

    public
    function getUserCards()
    {
        return $this->user_cards;
    }

    public
    function setUserCards($user_cards): void
    {
        $this->user_cards = $user_cards;
    }

    public
    function getTableCards()
    {
        return $this->table_cards;
    }

    public
    function setTableCards($table_cards): void
    {
        $this->table_cards = $table_cards;
    }

    public
    function getDeckCards()
    {
        return $this->deck_cards;
    }

    public
    function setDeckCards($deck_cards): void
    {
        $this->deck_cards = $deck_cards;
    }

    public
    function getDealerCards()
    {
        return $this->dealer_cards;
    }

    public
    function setDealerCards($dealer_cards): void
    {
        $this->dealer_cards = $dealer_cards;
    }
}
