<?php

namespace App\Models\Games;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Games\SlotGameLog
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $game
 * @property int $steps
 * @property string $mode
 * @property int|null $win
 * @property int $auto_spin
 * @property float $price
 * @property float $range
 * @property float $win_cash
 * @property float $balance_before
 * @property float $balance_after
 * @property mixed $combine
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereAutoSpin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereBalanceAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereBalanceBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereCombine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereGame($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereSteps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereWin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\SlotGameLog whereWinCash($value)
 * @mixin \Eloquent
 */
class SlotGameLog extends Model
{
    protected $fillable = [
        'user_id',
        'type',
        'game',
        'steps',
        'mode',
        'win',
        'auto_spin',
        'win_cash',
        'balance_before',
        'balance_after',
        'combine',
        'price',
        'range',
    ];
}