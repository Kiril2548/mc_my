<?php

namespace App\Models\Games\Mines;

use App\Models\Games\BaseGame;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Games\Mines\BaseMine
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Mines\BaseMine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Mines\BaseMine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Mines\BaseMine query()
 * @mixin \Eloquent
 */
class BaseMine extends BaseGame
{
    public $mult_cof = [
        '1' => 0.1,
        '2' => 0.30,
        '3' => 1,
        '4' => 1.25,
        '5' => 1.50,
        '6' => 1.86,
        '7' => 2,
        '8' => 2.48,
        '9' => 2.90,
        '10' => 3.56,
        '11' => 3.79,
        '12' => 4.1,
        '13' => 4.4,
        '14' => 4.9,
        '15' => 5.4,
        '16' => 5.7,
        '17' => 6.1,
        '18' => 6.54,
        '19' => 7,
        '20' => 8,
        '21' => 8.54,
        '22' => 9,
        '23' => 10,
        '24' => 12,
    ];

    public $_rows = null;
    public $_price = null;
    public $_cof = null;
    public $_mines = null;
    public $_steps = null;

    protected $_type = 'mines';

    public function __construct($rows = 5, $mines = 5, $cof = 0.05, $price = 1, array $attributes = [])
    {
        $this->setRows($rows);
        $this->setMines($mines);
        $this->setCof($cof);
        $this->setPrice($price);

        parent::__construct($attributes);
    }

    public function createCell($cell = [])
    {
        for ($i = 0; $i < ($this->_rows * $this->_rows); $i++) {
            array_push($cell, 1);
        }
        $cell = $this->putMines($cell);
        return $cell;
    }

    public function putMines($cell)
    {
        for ($i = 0; $i < $this->_mines; $i++) {
            $rand = $this->getRand();
            while (($cell[$rand] == 0)) {
                $rand = $this->getRand();
            }
            $cell[$rand] = 0;
        }
        return $cell;
    }

    public function getPrizeOnThisStep($rows, $mines, $price, $steps)
    {
        if ($steps > 0) {
            $cell_size = $rows * $rows;
            $prize = $price;

            for ($i = 0; $i < $steps; $i++) {
                $prize *= ($cell_size - $i) / ($cell_size - $i - $mines);
            }
            return $prize;
        } else {
            return 0;
        }
    }

    public function getPrizeOnNextStep($rows, $mines, $price, $steps)
    {
        if ($steps > 0) {
            $cell_size = $rows * $rows;
            $prize = $price;

            for ($i = 0; $i < ($steps + 1); $i++) {
                $prize *= ($cell_size - $i) / ($cell_size - $i - $mines);
            }
            return $prize;
        } else {
            return 0;
        }
    }

    public function lose_or_win($chance)
    {
        $chance = $chance * 100; //150
        $rand = rand(0, 100); //50
        if ($rand > $chance) { // 50 > 150
            return 0; // lose
        }
        return 1;
    }

    public function lose_cell($cell, $item_sel, $set)
    {
        if ($set == 1) {
            return $cell;
        } else {
            foreach ($cell as $key => $item) {
                $cell[$key] = 1;
            }
            $cell[$item_sel] = $set;
            $cell = $this->createLoseCell($cell);
            return $cell;
        }
    }

    public function createLoseCell($cell)
    {
        for ($i = 0; $i < $this->_mines-1; $i++) {
            $rand = $this->getRand();
            while (($cell[$rand] == 0)) {
                $rand = $this->getRand();
            }
            $cell[$rand] = 0;
        }
        return $cell;
    }

    public function countChance($steps, $rows, $user_chance)
    {
        $tiles_cnt = $rows * $rows;
        $mines = $this->getMines();

        $chance = 1;

        for ($i = 0; $i < $steps; $i++) {
            $chance *= ($tiles_cnt - $i - $mines) / ($tiles_cnt - $i);
        }

        return round(($chance + $user_chance) / 2, 2);
    }

    public function getRand()
    {
        return rand(0, ($this->_rows * $this->_rows) - 1);
    }

    public function getRows()
    {
        return $this->_rows;
    }

    public function setRows($rows): void
    {
        $this->_rows = $rows;
    }

    public function getPrice()
    {
        return $this->_price;
    }

    public function setPrice($price): void
    {
        $this->_price = $price;
    }

    public function getCof()
    {
        return $this->_cof;
    }

    public function setCof($cof): void
    {
        $this->_cof = $cof;
    }

    public function getMines()
    {
        return $this->_mines;
    }

    public function setMines($mines): void
    {
        $this->_mines = $mines;
    }
}
