<?php

namespace App\Models\Games;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Games\BaseGame
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\BaseGame newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\BaseGame newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\BaseGame query()
 * @mixin \Eloquent
 */
class BaseGame extends Model
{
    CONST MODE_PAY  = 'pay';
    CONST MODE_FREE = 'free'; //free spins

    private $_mode = null;

    protected $_name = null;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function setMode($mode) : void
    {
        if($mode == self::MODE_PAY || $mode == self::MODE_FREE){
            $this->_mode = $mode;
        }else{
            throw new \InvalidArgumentException('Error mode is not : ' . self::MODE_PAY .' or ' . self::MODE_FREE);
        }
    }

    public function getMode()
    {
        return $this->_mode;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setName($name): void
    {
        $this->_name = $name;
    }
}
