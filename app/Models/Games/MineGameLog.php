<?php

namespace App\Models\Games;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Games\MineGameLog
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $game
 * @property int $steps
 * @property int $mines
 * @property int $rows
 * @property string $mode
 * @property int|null $win
 * @property int $auto_spin
 * @property float $price
 * @property float $range
 * @property float $win_cash
 * @property float $balance_before
 * @property float $balance_after
 * @property mixed $game_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereAutoSpin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereBalanceAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereBalanceBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereGame($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereGameData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereMines($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereRows($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereSteps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereWin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\MineGameLog whereWinCash($value)
 * @mixin \Eloquent
 */
class MineGameLog extends Model
{

    protected $fillable = [
        'id',
        'user_id',
        'type',
        'game',
        'steps',
        'mines',
        'rows',
        'mode',
        'win',
        'auto_spin',
        'win_cash',
        'balance_before',
        'balance_after',
        'game_data',
        'price',
        'range',
    ];

}
