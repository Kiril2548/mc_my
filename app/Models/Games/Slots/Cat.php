<?php

namespace App\Models\Games\Slots;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Games\Slots\Cat
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Slots\Cat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Slots\Cat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Slots\Cat query()
 * @mixin \Eloquent
 */
class Cat extends BaseSlot
{
    const MIN_BET_USD  = 0.01;
    protected $bet_coin = 1;

    public function __construct($rows = 5, $columns = 3, array $attributes = [])
    {
        $this->setRows($rows);
        $this->setColumns($columns);

        $this->setName('cats');

        $this->_payouts = [
            0 => [
                    1 => 2, // -> one zero = * at 2
                    2 => 10, // -> two zero = * at 10
                    3 => 50 // -> three zero = * at 50
                ],
            1 => [1 => 0, 2 => 5, 3 => 15],
            2 => [1 => 0, 2 => 0, 3 => 10]
        ];

        $this->_items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        parent::__construct($attributes);
    }

    public function start()
    {
        $this->_combinations = $this->createCombinations($this->_items, $this->_payouts);
        $this->spin();
    }

    public function payForSpin()
    {
        $user = Auth::user();

        $user->coins -= $this->getBetCoin();

        $user->save();
    }

    public function getPrize()
    {
        $user = Auth::user();

        if($this->_prize){
            $user->coins += ($this->_prize * $this->getBetCoin());
        }

        $user->save();
    }

    public function getBetCoin(): int
    {
        return $this->bet_coin;
    }

    public function setBetCoin(int $bet_coin): void
    {
        $this->bet_coin = $bet_coin;
    }

    public function winCombineInEnd()
    {

    }

}
