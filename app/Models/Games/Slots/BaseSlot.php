<?php

namespace App\Models\Games\Slots;

use App\Models\Games\BaseGame;

/**
 * App\Models\Games\Slots\BaseSlot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Slots\BaseSlot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Slots\BaseSlot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Slots\BaseSlot query()
 * @mixin \Eloquent
 */
class BaseSlot extends BaseGame
{
    public $_rows    = null;
    public $_columns = null;
    public $_prize   = null;
    public $_payouts = null;
    public $_cn_tk_prz = null;
    public $_chance_to_win = null; // in %
    public $_id_won = null;
    public $_combinations; // unique combinations
    public $_combinations_enum = []; // unique combinations
    public $_rand_combination  = [];

    protected $_type = 'slots';

    public function __construct(array $attributes = [])
    {
        $this->_chance_to_win = 100;

        parent::__construct($attributes);
    }

    function spin(){

        $this->_rand_combination = $this->getRandArr();

        $this->cstmRand();

        if($this->userWon() && !$this->cnTkPrze()){
            $this->_rand_combination = $this->getRandArr();
        }

        $this->userWon();
    }

    function cnTkPrze(){
        return $this->_cn_tk_prz;
    }

    function userWon(){
        $comb = $this->getCombByComArr($this->getRand());
        $user_won = (bool)$comb['payout'];
        $this->_prize = $comb['payout'];
        return $user_won;
    }

    public function getRand()
    {
        return $this->_rand_combination;
    }

    function getCombByComArr($comb_arr){
        $comb = false;
        $comb_id = $this->getCombIdByCombStr($comb_arr);
        if($comb_id !== false){
            $comb = $this->_combinations[$comb_id];
        }
        return $comb;
    }

    function getCombIdByCombStr($comb_arr){

        $result = false;
        foreach ($this->_combinations as $id => $comb){
            if(implode('-', $comb_arr) == implode('-', $comb['comb_arr'])){
                $this->_id_won = $result = $id;
                break;
            }
        }
        return $result;
    }

    function cstmRand(){
        $rand = rand(0, 100);
        $this->_cn_tk_prz = ($this->getChanceToWin() >= $rand);
    }

    function getChanceToWin(){
        return $this->_chance_to_win;
    }

    protected function getRandArr(){
        $r1 = rand(0, count($this->_items) - 1);
        $r2 = rand(0, count($this->_items) - 1);
        $r3 = rand(0, count($this->_items) - 1);

        $randArr = [$this->_items[$r1], $this->_items[$r2], $this->_items[$r3]];

        while (!$this->checkIs($randArr)){
            $r1 = rand(0, count($this->_items) - 1);
            $r2 = rand(0, count($this->_items) - 1);
            $r3 = rand(0, count($this->_items) - 1);

            $randArr = [$this->_items[$r1], $this->_items[$r2], $this->_items[$r3]];
        }

        return $randArr;
    }

    public function checkIs($randArr)
    {
        $result = false;
        foreach ($this->_combinations as $id => $comb){
            if(implode('-', $randArr) == implode('-', $comb['comb_arr'])){
                $result = true;
                break;
            }
        }
        return $result;
    }

    function createCombinations($slots, $set_payouts)
    {
        $slots_cnt = count($slots);

        $outcome = [];

        for($i = 0; $i < $slots_cnt; $i++){
            for($k = 0; $k < $slots_cnt; $k++){
                for($j = 0; $j < $slots_cnt; $j++){
                    $comb_arr = [$slots[$i], $slots[$k], $slots[$j]];
                    $this->_combinations_enum[] = $comb_arr;
                    sort($comb_arr);
                    $comb_str = implode('-', $comb_arr);

                    if(isset($outcome[$comb_str])){

                        $outcome[$comb_str]['cnt'] += 1;
                    }else{
                        $outcome[$comb_str]['cnt'] = 1;
                    }
                    $outcome[$comb_str]['comb_str'] = $comb_str;
                    $outcome[$comb_str]['comb_arr'] = $comb_arr;
                }
            }
        }

        $total_combs = 0;
        foreach ($outcome as $comb => $o){
            $total_combs += $o['cnt'];
        }

        $total_chance = [];
        $total_chance['total'] = null;

        foreach ($outcome as $comb => $o){
            $outcome[$comb]['chance'] = $o['cnt'] / $total_combs * 100;
            $outcome[$comb]['payout'] = 100 / $outcome[$comb]['chance'];
            $total_chance['total'] += $outcome[$comb]['chance'];
        }

//        usort($outcome, function($a, $b){
//            return $a['chance'] > $b['chance'];
//        });

        $this->total_combinations = $total_combs;

        foreach ($outcome as $comb_key => $o){
            $payout = 0;

            foreach ($set_payouts as $element => $val){
                foreach ($val as $times => $summ){

                    $count_val = array_count_values($outcome[$comb_key]['comb_arr']);

                    if(isset($count_val[$element]) && $count_val[$element] == $times){
                        $payout += $summ;
                    }
                }
            }
            $outcome[$comb_key]['payout'] = $payout;
        }

        usort($outcome, function($a, $b){
            return $a['chance'] > $b['chance'];
        });

        return $outcome;

    }

    public function getRows()
    {
        return $this->_rows;
    }

    public function setRows($rows): void
    {
        $this->_rows = $rows;
    }

    public function getColumns()
    {
        return $this->_columns;
    }

    public function setColumns($columns): void
    {
        $this->_columns = $columns;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setType($type): void
    {
        $this->_type = $type;
    }


}
