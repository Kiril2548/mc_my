<?php

namespace App\Models\Games;

use Illuminate\Database\Eloquent\Model;

class BlackJack extends BaseGame
{
    protected $cards = [
        ['P2' => 2],
        ['P3' => 3],
        ['P4' => 4],
        ['P5' => 5],
        ['P6' => 6],
        ['P7' => 7],
        ['P8' => 8],
        ['P9' => 9],
        ['P10' => 10],
        ['PJ' => 10],
        ['PQ' => 10],
        ['PK' => 10],
        ['PA' => 11],
        ['T2' => 2],
        ['T3' => 3],
        ['T4' => 4],
        ['T5' => 5],
        ['T6' => 6],
        ['T7' => 7],
        ['T8' => 8],
        ['T9' => 9],
        ['T10' => 10],
        ['TJ' => 10],
        ['TQ' => 10],
        ['TK' => 10],
        ['TA' => 11],
        ['B2' => 2],
        ['B3' => 3],
        ['B4' => 4],
        ['B5' => 5],
        ['B6' => 6],
        ['B7' => 7],
        ['B8' => 8],
        ['B9' => 9],
        ['B10' => 10],
        ['BJ' => 10],
        ['BQ' => 10],
        ['BK' => 10],
        ['BA' => 11],
        ['C2' => 2],
        ['C3' => 3],
        ['C4' => 4],
        ['C5' => 5],
        ['C6' => 6],
        ['C7' => 7],
        ['C8' => 8],
        ['C9' => 9],
        ['C10' => 10],
        ['CJ' => 10],
        ['CQ' => 10],
        ['CK' => 10],
        ['CA' => 11],
    ];
    protected $card_in_deck = 56;
    protected $deck_count = 1;
    protected $cards_count = 1;
    protected $cards_types_count = 13;

    public $_bet = null;

    public $deck_cards = [];
    public $dealer_cards = [];
    public $user_cards = [];

    protected $_type = 'black_jack';
    protected $_name = 'black_jack';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }


    public function init_game($bet)
    {
        $this->setBet($bet);

        $deck = $this->createDeck();
        $this->setDeckCards($deck);

        $this->firstTwoUserCard();
        $this->firstTwoDealerCard();

        return [
            'deck_of_game' => $deck,
            'user_cards' => $this->user_cards,
            'user_cards_value' => $this->userValueOfCards($this->getUserCards()),
            'dealer_cards' => $this->dealer_cards,
            'dealer_cards_value' => $this->dealerValueOfCards($this->getDealerCards()),
            'deck' => $this->getDeckCards(),
        ];
    }

    public function createDeck()
    {
        $deck = [];
        for ($i = 0; $i < $this->deck_count; $i++) {
            for ($j = 0; $j < $this->cards_count; $j++) {
                foreach ($this->cards as $key => $card) {
                    array_push($deck, $key);
                }
            }
        }
        shuffle($deck);
        $deck = $this->printDeck($deck);
        return $deck;
    }

    public function printDeck($deck)
    {
        $deck_view = [];
        foreach ($deck as $item) {
            array_push($deck_view, $this->cards[$item]);
        }
        return $deck_view;
    }

    public function firstTwoUserCard()
    {
        $deck = $this->getDeckCards();

        $first = $this->getRandCard($deck);
        $first_card = $deck[$first];
        unset($deck[$first]);
        $deck = array_values($deck);
        array_push($this->user_cards, $first_card);
        $this->setDeckCards($deck);

        $second = $this->getRandCard($deck);
        $second_card = $deck[$second];
        unset($deck[$second]);
        $deck = array_values($deck);
        array_push($this->user_cards, $second_card);
        $this->setDeckCards($deck);
    }

    public function firstTwoDealerCard()
    {
        $deck = $this->getDeckCards();

        $first = $this->getRandCard($deck);
        $first_card = $deck[$first];
        unset($deck[$first]);
        $deck = array_values($deck);
        array_push($this->dealer_cards, $first_card);
        $this->setDeckCards($deck);

        $second = $this->getRandCard($deck);
        $second_card = $deck[$second];
        unset($deck[$second]);
        $deck = array_values($deck);
        array_push($this->dealer_cards, $second_card);
        $this->setDeckCards($deck);
    }

    public function getRandCard($deck)
    {
        return rand(0, (count($deck) - 1));
    }

    public function userTakeCard($deck, $cards)
    {
        $card = $this->getRandCard($deck);
        $teken_card = $deck[$card];
        unset($deck[$card]);
        $deck = array_values($deck);
        array_push($cards, $teken_card);
        return [
            'deck' => $deck,
            'cards' => $cards,
        ];
    }

    public function dealerTakeCard($deck, $cards)
    {
        while ($this->dealerValueOfCards($cards) < 17) {
            $card = $this->getRandCard($deck);
            $teken_card = $deck[$card];
            unset($deck[$card]);
            $deck = array_values($deck);
            array_push($cards, $teken_card);
        }
        return [
            'deck' => $deck,
            'cards' => $cards,
            'cards_value' => $this->dealerValueOfCards($cards)
        ];
    }

    public function userValueOfCards($cards)
    {
        $values = 0;
        foreach ($cards as $card) {
            foreach ($card as $key => $item) {
                $values += $item;
            }
        }
        return $values;
    }

    public function whoWin($user_cards, $dealer_cards)
    {
        $user_value = $this->userValueOfCards($user_cards);
        $dealer_value = $this->dealerValueOfCards($dealer_cards);
        if ($user_value < 22 && $dealer_value < 22) {
            if ($user_value == $dealer_value) {
                return 'back';
            } elseif ($user_value == 21) {
                return 'win_x3';
            } elseif ($user_value > $dealer_value) {
                return 'win';
            } elseif ($user_value < $dealer_value) {
                return 'lose';
            }
        } elseif ($user_value > 21 && $dealer_value > 21) {
            return 'back';
        } elseif ($user_value < 22 && $dealer_value > 21) {
            return 'win';
        } elseif ($user_value > 21 && $dealer_value < 22) {
            return 'lose';
        } else {
            return 'lose';
        }
    }

    public
    function dealerValueOfCards($cards)
    {
        $values = 0;
        foreach ($cards as $card) {
            foreach ($card as $key => $item) {
                $values += $item;
            }
        }
        return $values;
    }

    public
    function getBet()
    {
        return $this->_bet;
    }

    public
    function setBet($bet): void
    {
        $this->_bet = $bet;
    }

    public
    function getUserCards()
    {
        return $this->user_cards;
    }

    public
    function setUserCards($user_cards): void
    {
        $this->user_cards = $user_cards;
    }

    public
    function getDealerCards()
    {
        return $this->dealer_cards;
    }

    public
    function setDealerCards($dealer_cards): void
    {
        $this->dealer_cards = $dealer_cards;
    }

    public
    function getDeckCards()
    {
        return $this->deck_cards;
    }

    public
    function setDeckCards($deck_cards): void
    {
        $this->deck_cards = $deck_cards;
    }

}
