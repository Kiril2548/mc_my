<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArticleCategory
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article[] $articles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read \App\Models\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $children
 * @property-write mixed $slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory query()
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $category_id
 * @property string $title
 * @property int $place
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory wherePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleCategory whereUpdatedAt($value)
 */
class ArticleCategory extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'place',
        'category_id',
        'status'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'category_id', 'id');
    }

}
