<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * App\Models\TBot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TBot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TBot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TBot query()
 * @mixin \Eloquent
 */
class TBot extends Model
{
    const TOKEN = '779876557:AAE3OSjlfoPjIHZoaTGT6bLJ5vOrsvaAnX4';
    const DEV_CHAT_ID = '-331726784';
    const LIVE_CHAT_ID = '-360238397';

    public function sendInfoMes($text)
    {
        if(App::environment('local')){
            return file_get_contents("https://api.telegram.org/bot". self::TOKEN ."/sendMessage?chat_id=". self::DEV_CHAT_ID ."&text=Info from: " . config('app.url', '127.0.0.1') ."%0A{$text}");
        } else {
            return file_get_contents("https://api.telegram.org/bot". self::TOKEN ."/sendMessage?chat_id=". self::LIVE_CHAT_ID ."&text=Info from: " . config('app.url', '127.0.0.1') . "%0A{$text}");
        }
    }

    public function sendErrorMes($text)
    {
//        $text = json_encode($text);
        if(App::environment('local')) {
            return file_get_contents("https://api.telegram.org/bot". self::TOKEN ."/sendMessage?chat_id=". self::DEV_CHAT_ID ."&text=🔥Error🔥: " . config('app.url', '127.0.0.1') . "%0A{$text}");
        } else {
            return file_get_contents("https://api.telegram.org/bot". self::TOKEN ."/sendMessage?chat_id=". self::LIVE_CHAT_ID ."&text=🔥Error🔥: " . config('app.url', '127.0.0.1') . "%0A{$text}");
        }
    }

}
