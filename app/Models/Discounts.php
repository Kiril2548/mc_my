<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Discounts
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Discounts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Discounts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Discounts query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Discounts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Discounts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Discounts whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Discounts whereValue($value)
 */
class Discounts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
    ];
}
