<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Orders
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders query()
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $order_id
 * @property int|null $category_id
 * @property int|null $user_id
 * @property string $who
 * @property int|null $product_id
 * @property string|null $count
 * @property string $status
 * @property float $amount
 * @property string|null $error_des
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereErrorDes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWho($value)
 */
class Orders extends Model
{
    //
}
