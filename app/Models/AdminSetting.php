<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminSetting
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property int $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminSetting whereValue($value)
 */
class AdminSetting extends Model
{
    //
}
