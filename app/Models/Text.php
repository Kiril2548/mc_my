<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Text
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $page
 * @property string $key
 * @property string $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text wherePage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Text whereUpdatedAt($value)
 */
class Text extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page',
        'key',
        'text',
    ];
}
