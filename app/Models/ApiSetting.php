<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ApiSetting
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property string $value
 * @property string $more
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting whereMore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiSetting whereValue($value)
 */
class ApiSetting extends Model
{
    //
}
