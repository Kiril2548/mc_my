<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Bank
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $from
 * @property float $take
 * @property string $to
 * @property float $get
 * @property float $rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereGet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereTake($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank whereUserId($value)
 */
class Bank extends Model
{
    protected $fillable = [
        'user_id',
        'from',
        'take',
        'to',
        'get',
        'rate',
    ];

    public function exchangeCurrency($user, $amount, $from, $to = 'COINS')
    {
        try {
            $user_balances = UserBalance::whereUserId($user->id)->whereRateId($from)->with('rates')->first();
            if ($user_balances->amount >= $amount){

                $user_coins = ($amount * $user_balances->rates->rate)*100;
                $user->coins += $user_coins;
                $user_balances->amount = $user_balances->amount - $amount;

                $data = [
                    'user_id'   => $user->id,
                    'from'   => $user_balances->rates->crypto_symbol,
                    'take'   => $amount,
                    'to'   => $to,
                    'get'   => $user_coins,
                    'rate'   => $user_balances->rates->rate,
                ];
                $exchange = new Bank();
                $exchange->fill($data);
                DB::transaction(function () use ($exchange, $user, $user_balances) {
                    $exchange->save();
                    $user->save();
                    $user_balances->save();
                });
            }else{
                return 'Amount of bigger the balance of currency '.$user_balances->rates->crypto_symbol;
            }
        } catch (\Exception $e) {
            $tb = (new TBot())->sendErrorMes($e);
            return $user->coins;
        }

        return $user->coins;
    }
}
