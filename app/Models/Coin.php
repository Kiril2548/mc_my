<?php

namespace App\Models\Games;

use App\Models\Bank;
use App\Models\TBot;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

/**
 * App\Models\Games\Coin
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Coin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Coin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Games\Coin query()
 * @mixin \Eloquent
 */
class Coin extends Model
{
    protected $costOfOneCoin = 0.1;

    protected $from = 'USD';
    protected $to  = 'COIN';

    public $_user = null;
    public $_bank = null;

    public function __construct(array $attributes = [])
    {
        $this->_user = Auth::user();
        $this->_bank = new Bank();

        $this->_bank->user_id = $this->_user['id'];
        $this->_bank->from = $this->from;
        $this->_bank->to   = $this->to;
        $this->_bank->rate = $this->costOfOneCoin;

        parent::__construct($attributes);
    }

    public function buyCoins($quantity) : bool
    {
        $res = false;

        $costAll = ($quantity * $this->costOfOneCoin);

        if($costAll <= $this->_user->balance){
            $this->_user->balance -= $costAll;
            $this->_user->coins   += $quantity;

            $this->_bank->take = $costAll;
            $this->_bank->get  = $quantity;

            try {
                DB::transaction(function () use (&$res){
                    $this->_bank->save();
                    $this->_user->save();

                    $res = true;
                });
            } catch (\Exception $e){
                $tBot = new TBot();
                $tBot->sendErrorMes($e);
                $res = false;
            }
        }

        return $res;
    }
}
