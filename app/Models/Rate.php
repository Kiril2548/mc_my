<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Rate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $currency
 * @property string $to_usd
 * @property float $rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereToUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereUpdatedAt($value)
 */
class Rate extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'currency',
        'crypto_tag',
        'crypto_symbol',
        'rate'
    ];

}
