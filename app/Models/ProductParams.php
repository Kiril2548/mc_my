<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductParams
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $product_id
 * @property string $key
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductParams whereValue($value)
 */
class ProductParams extends Model
{
    protected $fillable = [
        'key',
        'value',
    ];
}
