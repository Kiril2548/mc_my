<?php

namespace App\Console\Commands;

use App\Models\Rate;
use Illuminate\Console\Command;

class GetCrypto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:crypto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse list of crypto currency to rate tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cryptos = file_get_contents('https://api.coinmarketcap.com/v1/ticker/');
        $cryptos = json_decode($cryptos);

        foreach ($cryptos as $crypto){
            $rate = Rate::where('currency', $crypto->name)->first();
            if (is_null($rate)){
                Rate::create([
                    'currency' => $crypto->name,
                    'crypto_tag' => $crypto->id,
                    'crypto_symbol' => $crypto->symbol,
                    'rate' => $crypto->price_usd,
                    'status' => 0
                ]);
            } else {
                Rate::where('currency', $crypto->name)->update([
                    'rate' => $crypto->price_usd
                ]);
            }
        }

    }
}
