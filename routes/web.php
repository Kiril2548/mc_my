<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'MainController@index')->name('main');

Route::get('shop', 'ShopController@shop')->name('shop');
Route::get('shop_', 'ShopController@shop_')->name('shop_');
Route::get('shop__', 'ShopController@shop__')->name('shop__');

Route::resource('pages', 'PageController')->only('show');

Route::get('articles/mini', 'BlogController@mini')->name('articles.mini');
Route::resource('articles', 'BlogController')->only('index', 'show');

/*auth*/
Route::middleware(['auth', 'count.balance'])->group(function () {

    Route::post('/buy/coins', 'BuyCoinsController@addBalance')->name('buy.coins');

    Route::get('/bank', 'BankController@bank')->name('bank');
    Route::post('/bank/exchange', 'BankController@exchange')->name('bank.exchange');

    Route::get('mines', 'MineController@index')->name('mines');
    Route::get('/blackjack', 'BlackJackController@blackjack')->name('blackjack');
    Route::get('/poker/holdem', 'PokerController@start')->name('poker.holdem');
    Route::get('stone/pharaoh', 'Games\Stones\PharaohController@index')->name('stone.pharaoh');

    Route::get('/home', 'UserController@show')->name('home');

    Route::resource('user', 'UserController')->only('show','edit', 'update');

    Route::prefix('games')->name('games.')->namespace('Games')->group(function () {

        Route::prefix('slots')->name('slots.')->namespace('Slots')->group(function () {
            Route::resource('cats', 'CatsController')->only('show', 'store');
            Route::get('cats', 'CatsController@store');
        });

        Route::prefix('/stone')->name('stones.')->namespace('Stones')->group(function () {
            Route::post('pharaoh/game', 'PharaohController@game')->name('pharaoh.game');
            Route::get('pharaoh/game', 'PharaohController@game')->name('pharaoh.game');
        });

        Route::prefix('/mines')->name('mines.')->namespace('Mines')->group(function () {
            Route::post('/', 'MineController@mine')->name('check');
            Route::post('get', 'MineController@getCash')->name('get');
        });

        Route::prefix('/blackjack')->name('blackjack.')->namespace('BlackJack')->group(function () {
            Route::post('/getCard', 'BlackJackController@getCard')->name('get_card');
            Route::post('/start', 'BlackJackController@start')->name('start');
            Route::post('/stop', 'BlackJackController@stop')->name('stop');
        });

    });

    Route::post('comments/store/{article}', 'BlogController@add_comments')->name('comments.store');
    Route::post('comments/like/{comment}', 'BlogController@like_comments')->name('comments.like');
    Route::post('comments/update/{comment}', 'BlogController@update_comments')->name('comments.update');
    Route::post('comments/delete/{comment}', 'BlogController@delete_comments')->name('comments.delete');
});


#region Admin
Route::group(['prefix' => 'admin', 'middleware' => [
    'auth',
    'role:moderator'
], 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    /*default*/
    Route::get('/', 'MainController@index')->name('main');
    Route::resource('users', 'UserController')->only('index', 'edit'); //*
    Route::resource('sessions', 'SessionController')->only('index'); //*


    /*E-commerce*/
    Route::get('categories/place', 'CategoryController@place')->name('categories.place');
    Route::resource('categories', 'CategoryController')->only('index', 'edit');

    Route::resource('products', 'ProductController')->only('index', 'edit', 'show');
    Route::resource('rates', 'RateController')->only('index', 'edit', 'show');
    Route::resource('orders', 'OrderController')->only('index', 'edit');
    Route::resource('payments', 'PaymentController')->only('index', 'edit');
    Route::resource('carts', 'CartController')->only('index', 'edit');


    /*Content*/
    Route::resource('pages', 'PageController')->only('index', 'edit', 'show');
    Route::resource('menus', 'MenuController')->only('index', 'edit');
    Route::resource('news', 'NewController')->only('index', 'edit', 'show');

    Route::resource('article-categories', 'ArticleCategoryController')->only('index', 'edit'); //*
    Route::get('article-categories/place', 'ArticleCategoryController@place')->name('article-categories.place'); //*

    Route::resource('articles', 'ArticleController')->only('index', 'edit', 'show');
    Route::resource('texts', 'TextController')->only('index', 'edit', 'show');


    /*Settings*/
    Route::resource('settings', 'SettingController')->middleware('role:admin')->only('index', 'edit', 'show');
    Route::resource('api_settings', 'SettingController')->middleware('role:admin')->only('index', 'edit', 'show');
    Route::resource('roles', 'SettingController')->middleware('role:admin')->only('index', 'edit', 'show');
    Route::resource('permissions', 'SettingController')->middleware('role:admin')->only('index', 'edit', 'show');


    /*misc*/
    Route::get('/documentation', 'DocController@index')->name('documentation');

    Route::resource('admin_settings', 'AdminSettingController')->middleware('role:owner')->only('index');

});
#endregion

