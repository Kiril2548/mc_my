<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Routes for Admin*/
Route::group(['prefix' => 'admin','middleware'  =>  [
    'auth',
    'role:moderator'
] , 'namespace' => 'Api\Admin', 'as' => 'admin.' ], function() {

    Route::post('users/search', 'UserController@search')->name('users.search');
    Route::resource('users', 'UserController')->only('update', 'destroy');

    Route::post('categories/search', 'CategoryController@search')->name('categories.search');
    Route::resource('categories', 'CategoryController')->only('update', 'store', 'destroy');

    Route::post('rates/search', 'RateController@search')->name('rates.search');
    Route::resource('rates', 'RateController')->only('update', 'store', 'destroy');

    Route::post('article-categories/search', 'ArticleCategoryController@search')->name('article-categories.search');
    Route::resource('article-categories', 'ArticleCategoryController')->only('update', 'store', 'destroy');

    Route::post('pages/search', 'PageController@search')->name('pages.search');
    Route::resource('pages', 'PageController')->only('update', 'store', 'destroy');

    Route::post('news/search', 'NewController@search')->name('news.search');
    Route::resource('news', 'NewController')->only('update', 'store', 'destroy');

    Route::post('articles/search', 'ArticleController@search')->name('articles.search');
    Route::resource('articles', 'ArticleController')->only('update', 'store', 'destroy');

    Route::post('texts/search', 'TextController@search')->name('texts.search');
    Route::resource('texts', 'TextController')->only('update', 'store', 'destroy');

    Route::post('products/search', 'ProductController@search')->name('products.search');
    Route::resource('products', 'ProductController')->only('update', 'store', 'destroy');

    Route::post('settings/update', 'SettingController@update')->name('settings.update');
    Route::resource('settings', 'SettingController')->only('store', 'destroy');


    Route::put('admin_settings/update', 'AdminSettingController@update')->name('admin_settings.update');

});
